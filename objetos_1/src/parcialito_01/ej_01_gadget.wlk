
import dispositivos.*
import ej_02_cerbatana.*

object gadget {
	
	var dispositivos = #{}
	var peso
	
	
	method incorporar(dispositivo) {
		dispositivos.add(dispositivo)
	}
	
	method perder(dispositivo) {
		dispositivos.remove(dispositivo)
	}
	
	method nacionalidades() {
		return dispositivos.map({
			dispositivo => dispositivo.nacionalidad()
		}).asSet()
	}
	
	method peso(kg) {
		peso = kg * 1000	// en gramos
	}
	
	method pesoTotal() {
		return peso + dispositivos.map({
			dispositivo => dispositivo.peso()
		}).sum()
	}
	
	method preparadoContraGarra() {
		return dispositivos.size() >= 3
			and dispositivos.all({
				dispositivo => dispositivo.despliegue() < 1500
			}) 
	}
	
	method nacionalidadDelDispositivoMasRapido() {
		return dispositivos.min({
			dispositivo => dispositivo.despliegue()
		}).nacionalidad()
	}
	
	method sobrecarga(ms) {
		return dispositivos.filter({
			dispositivo => dispositivo.despliegue() > ms
		}).map({
			dispositivo => dispositivo.peso()
		}).sum()
	}
	
	method esDeseado(dispositivo) {
		return not dispositivos.contains(dispositivo)
			and dispositivo.despliegue() < 1700  
	}
	
	method dispositivosDeseados(_dispositivos) {
		return _dispositivos.filter({
			dispositivo => self.esDeseado(dispositivo)
		}) 
	}
	
}

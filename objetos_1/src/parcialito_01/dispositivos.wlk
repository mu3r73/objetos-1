
object helicoptero {
	
	const peso = 5000
	const despliegue = 1200
	const nacionalidad = "argentina"
	
	
	method peso() {
		return peso
	}
	
	method despliegue() {
		return despliegue
	}
	
	method nacionalidad() {
		return nacionalidad
	}
	
}

object llaveInglesa {
	
	const peso = 1500
	const despliegue = 500
	const nacionalidad = "francesa"
	
	
	method peso() {
		return peso
	}
	
	method despliegue() {
		return despliegue
	}
	
	method nacionalidad() {
		return nacionalidad
	}
	
}

object paraguas {
	
	const peso = 500
	const despliegue = 700
	const nacionalidad = "italiana"
	
	
	method peso() {
		return peso
	}
	
	method despliegue() {
		return despliegue
	}
	
	method nacionalidad() {
		return nacionalidad
	}
	
}


object infladorDeTraje {
	
	const peso = 200
	const despliegue = 2100
	const nacionalidad = "argentina"
	
	
	method peso() {
		return peso
	}
	
	method despliegue() {
		return despliegue
	}
	
	method nacionalidad() {
		return nacionalidad
	}
	
}

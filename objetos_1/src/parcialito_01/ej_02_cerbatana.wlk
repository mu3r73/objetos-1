
object cerbatana {
	
	const peso = 200
	const despliegue0 = 100
	const nacionalidad = "francesa"
	var mira = miraTelescopica
	var cartucho = papelEnsalivado
	
	
	method mira(_mira) {
		mira = _mira
	}
	
	method cartucho(_cartucho) {
		cartucho = _cartucho
	}
	
	method peso() {
		return peso + mira.peso() + cartucho.peso() 
	}
	
	method despliegue() {
		return despliegue0 + mira.despliegue().max(cartucho.despliegue())
	}
	
	method nacionalidad() {
		return nacionalidad
	}
	
}

object miraTelescopica {
	
	const peso = 200
	const despliegue = 500
	
	
	method peso() {
		return peso
	}
	
	method despliegue() {
		return despliegue
	}
	
}


object miraLaser {
	
	const peso = 150
	const despliegue = 600
	
	
	method peso() {
		return peso
	}
	
	method despliegue() {
		return despliegue
	}
	
}

object dardos {
	
	const peso = 100
	const despliegue = 700
	
	
	method peso() {
		return peso
	}
	
	method despliegue() {
		return despliegue
	}
	
}

object perdigones {
	
	const peso = 250
	const despliegue = 400
	
	
	method peso() {
		return peso
	}
	
	method despliegue() {
		return despliegue
	}
	
}

object papelEnsalivado {
	
	const peso = 10
	const despliegue = 1600
	
	
	method peso() {
		return peso
	}
	
	method despliegue() {
		return despliegue
	}
	
}


object pepita {
	
	var energia = 0
	
	
	method comer(gramos) {
		energia += gramos * 4
	}
	
	method volar(kilometros) {
		energia -= 10 + kilometros
	}
	
	method energia() {
		return energia
	}
	
	method estaDebil() {
		return (energia < 50)
	}
	
	method estaFeliz() {
		return ((energia >= 500) and (energia <= 1000))
	}
	
	method cuantoQuiereVolar() {
		var cuanto = energia / 5
		if ((energia >= 300) and (energia <= 400)) {
			cuanto += 10
		}
		if (energia % 20 == 0) {
			cuanto += 15
		}
		return cuanto
	}
	
}

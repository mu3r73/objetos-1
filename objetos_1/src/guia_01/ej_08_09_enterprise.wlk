
object enterprise {
	
	var potencia = 50 	// entre 0 y 100
	var coraza = 5 		// entre 0 y 20


	method potencia() {
		return potencia
	}
	
	method coraza() {
		return coraza
	}
	
	method encontrarPilaAtomica() {
		potencia += 25
		if (potencia > 100) {
			potencia = 100
		}
	}
	
	method encontrarEscudo() {
		coraza += 10
		if (coraza > 20) {
			coraza = 20
		}
	}
	
	method recibirAtaque(puntos) {
		coraza -= puntos
		if (coraza < 0) {
			potencia += coraza
			coraza = 0
		}
		if (potencia < 0) {
			potencia = 0
		}
	}
	
	method fortalezaDefensiva() {
		return coraza + potencia
	}

	method necesitaFortalecerse() {
		return (coraza == 0) and (potencia < 20)
	}
	
	method fortalezaOfensiva() {
		if (potencia < 20) {
			return 0
		} else {
			return (potencia - 20) / 2
		}
	}
	
}

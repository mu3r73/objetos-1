
object modeloDeMotor {

	var cambio = 0		// de 0 a 5
	var rpm = 0			// de 0 a 5000
	
	
	method arrancar() {
		cambio = 1
		rpm = 500
	}
	
	method subirCambio() {
		cambio += 1
		if (cambio > 5) {
			cambio = 5
		}
	}
	
	method bajarCambio() {
		cambio -= 1
		if (cambio < 0) {
			cambio = 0
		}
	}
	
	method subirRPM(cuantos) {
		rpm += cuantos
		if (rpm > 5000) {
			rpm = 5000
		}
	}
	
	method bajarRPM(cuantos) {
		rpm -= cuantos
		if (rpm < 0) {
			rpm = 0
		}
	}
	
	method cambio() {
		return cambio
	}
	
	method rpm() {
		return rpm
	}
	
	method velocidad() {
		return (rpm / 100) * (0.5 + (cambio / 2.0))
	}
	
	method consumoActualPorKm() {
		var consumo = 0.05
		if (rpm > 3000) {
			consumo *= (rpm - 2500) / 500
		}
		if (cambio == 1) {
			consumo *= 3
		} else if (cambio == 2) {
			consumo *= 2
		}
		return consumo
	}
	
}

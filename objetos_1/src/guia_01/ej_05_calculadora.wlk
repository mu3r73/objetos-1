
object calculadora {
	
	var res = 0


	method cargar(numero) {
		res = numero
	}
	
	method sumar(numero) {
		res += numero	
	}
	
	method restar(numero) {
		res -= numero
	}
	
	method multiplicar(numero) {
		res *= numero
	}
	
	method valorActual() {
		return res
	}
	
}

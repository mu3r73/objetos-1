
object tecladoEspingarda {

	var numero = ""
	var llamadas = 0

	
	method agregarDigito(digito) {
		numero = numero + digito
	}
	
	method llamar() {
		llamadas++
		numero = ""
	}
	
	method numeroIngresado() {
		return numero
	}
	
	method esNumeroValido() {
		return (numero.length() == 5)
			or ((numero.length() == 7) and numero.startsWith("15")) 
	}
 
 	method borrarUltimoDigito() {
 		numero = numero.substring(0, numero.size() - 1)
 	}
 	
 	method cantLlamadas() {
 		return llamadas
 	}
 	
}


// carpas

class Carpa {
	
	const marca
	const limiteDeGente
	var tieneBanda = false
	var personas = #{}
	
	constructor(_marca, _limiteDeGente) {
		marca = _marca
		limiteDeGente = _limiteDeGente
	}
	
	method marca() {
		return marca
	}
	
	method tieneBanda(bool) {
		tieneBanda = bool
	}
	
	method tieneBanda() {
		return tieneBanda
	}
	
	method permiteEntrarA(persona) {
		return (personas.size() < limiteDeGente)
			and not persona.estaEbria()
	}
	
	method admitir(persona) {
		if (not persona.puedeEntrarEn(self)) {
			throw new CervezaException("la persona no puede entrar en la carpa")
		}
		personas.add(persona)
	}
	
	method ebriosEmpedernidos() {
		return personas.filter({
			persona => persona.esEbrioEmpedernido()
		})
	}
	
	method cantEbriosEmpedernidos() {
		return self.ebriosEmpedernidos().size()
	}
	
}

// jarras

class Jarra {
	
	const carpa
	const capacidad // en l
	
	constructor(_carpa, _capacidad) {
		carpa = _carpa
		capacidad = _capacidad
	}
	
	method capacidad() {
		return capacidad
	}
	
	method marca() {
		return carpa.marca()
	}
	
	method alcoholQueAporta() {
		return capacidad * self.marca().graduacion()
	}
	
}

// personas

class Persona {
	// abstracta
	
	var peso
	var aguante
	var leGustaLaMusicaTradicional
	var jarras = #{}
	
	constructor(_peso, _aguante, _leGustaLaMusicaTradicional) {
		self.peso(_peso)
		self.aguante(_aguante)
		self.leGustaLaMusicaTradicional(_leGustaLaMusicaTradicional)
	}
	
	method peso(_peso) {
		peso = _peso
	}
	
	method aguante(_aguante) {
		aguante = _aguante
	}
	
	method leGustaLaMusicaTradicional(bool) {
		leGustaLaMusicaTradicional = bool
	}
	
	method comprar(jarra) {
		jarras.add(jarra)
	}
	
	method leGusta(marca)
	// abstracto 
	
	method estaEbria() {
		return self.alcoholIngerido() * peso > aguante
	}
	
	method alcoholIngerido() {
		return jarras.map({
			jarra => jarra.alcoholQueAporta()
		}).sum()
	}
	
	method quiereEntrarEn(carpa) {
		return self.leGusta(carpa.marca())
			and leGustaLaMusicaTradicional == carpa.tieneBanda()
	}
	
	method puedeEntrarEn(carpa) {
		return self.quiereEntrarEn(carpa)
			and carpa.permiteEntrarA(self)
	}
	
	method entrarEn(carpa) {
		carpa.admitir(self)
	}
	
	method esEbrioEmpedernido() {
		return self.estaEbria()
			and jarras.all({
				jarra => jarra.capacidad() >= 1
			})
	}
	
}

class Belga inherits Persona {
	
	constructor(_peso,_aguante,_leGustaLaMusicaTradicional) = super(_peso,_aguante,_leGustaLaMusicaTradicional)
	
	override method leGusta(marca) {
		return marca.lupulo() > 4
	}
	
}

class Checo inherits Persona {
	
	constructor(_peso,_aguante,_leGustaLaMusicaTradicional) = super(_peso,_aguante,_leGustaLaMusicaTradicional)
	
	override method leGusta(marca) {
		return marca.graduacion() > 80 // %
	}
	
}

class Aleman inherits Persona {
	
	constructor(_peso,_aguante,_leGustaLaMusicaTradicional) = super(_peso,_aguante,_leGustaLaMusicaTradicional)
	
	override method leGusta(marca) {
		return true
	}
	
}

// marcas

class Marca {
	// abstracta
	
	const lupulo // en g/l
	
	constructor(_lupulo) {
		lupulo = _lupulo
	}
	
	method lupulo() {
		return lupulo
	}
	
	method graduacion()
	// abstracto
	
}

class Rubia inherits Marca {
	
	const graduacion
	
	constructor(_lupulo, _graduacion) = super(_lupulo) {
		graduacion = _graduacion
	}
	
	override method graduacion() {
		return graduacion
	}
	
}

class Negra inherits Marca {
	
	constructor(_lupulo) = super(_lupulo)
	
	override method graduacion() {
		return indicadores.graduacionReglamentaria().min(lupulo * 2)
	}
	
}

class Roja inherits Negra {
	
	constructor(_lupulo) = super(_lupulo)
	
	override method graduacion() {
		return super() + 25
	}
	
}


// objetos bien conocidos

object indicadores {
	
	var graduacionReglamentaria = 5 // %
	
	method graduacionReglamentaria(porcentaje) {
		graduacionReglamentaria = porcentaje
	}
	
	method graduacionReglamentaria() {
		return graduacionReglamentaria
	}
	
}

// excepciones

class CervezaException inherits Exception {
	
	constructor(_message) = super(_message)
	
}

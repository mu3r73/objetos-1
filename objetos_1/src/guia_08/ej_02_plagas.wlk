// elementos

class Elemento {
	// abstracta
	
	method esBueno()
	// abstracto
	
	method recibirAtaqueDe(plaga)
	// abstracto
	
}

class Hogar inherits Elemento {
	
	var mugre
	var confort
	
	constructor(_mugre, _confort) {
		self.mugre(_mugre)
		self.confort(_confort)
	}
	
	method mugre(nivel) {
		mugre = nivel
	}
	
	method confort(nivel) {
		confort = nivel
	}
	
	override method esBueno() {
		return mugre <  2 * confort
	}
	
	override method recibirAtaqueDe(plaga) {
		mugre += plaga.danioQueCausa()
	}
	
}

class Huerta inherits Elemento {
	// requiere el objeto bien conocido indicadores
	
	var produccion
	
	constructor(_produccion) {
		self.produccion(_produccion)
	}
	
	method produccion(nivel) {
		produccion = nivel
	}
	
	override method esBueno() {
		return produccion > indicadores.produccionStandard()
	}
	
	override method recibirAtaqueDe(plaga) {
		produccion -= plaga.danioQueCausa()
		if (plaga.transmiteEnfermedades()) {
			produccion -= 10
		}
	}
	
}

class Mascota inherits Elemento {
	
	var salud
	
	constructor(_salud) {
		self.salud(_salud)
	}
	
	method salud(_salud) {
		salud = _salud
	}
	
	override method esBueno() {
		return salud > 15
	}
	
	override method recibirAtaqueDe(plaga) {
		if (plaga.transmiteEnfermedades()) {
			salud -= plaga.danioQueCausa()
		}
	}
	
}

// plagas

class Plaga {
	// abstracta
	
	var poblacion
	
	constructor(_poblacion) {
		self.poblacion(_poblacion)
	}
	
	method poblacion(_poblacion) {
		poblacion = _poblacion
	}
	
	method danioQueCausa()
	// abstracto
	
	method transmiteEnfermedades()
	// abstracto
	
	method atacar(elemento) {
		poblacion += poblacion * 0.1
		elemento.recibirAtaqueDe(self)
	}
	
}

class Cucarachas inherits Plaga {
	
	constructor(_poblacion) = super(_poblacion)
	
	override method danioQueCausa() {
		return poblacion / 2
	}
	
	override method transmiteEnfermedades() {
		return poblacion > 500
	}
	
}

class Pulgas inherits Plaga {
	
	constructor(_poblacion) = super(_poblacion)
	
	override method danioQueCausa() {
		return 2 * poblacion
	}
	
	override method transmiteEnfermedades() {
		return true
	}
	
}

class Garrapatas inherits Pulgas {
	
	constructor(_poblacion) = super(_poblacion)
	
	override method atacar(elemento) {
		poblacion += poblacion * 0.2
		elemento.recibirAtaqueDe(self)
	}
	
}

class Mosquitos inherits Plaga {
	
	var infectadaCon
	
	constructor(_poblacion) = super(_poblacion)
	
	method infectadaCon(enfermedad) {
		infectadaCon = enfermedad
	}
	
	override method danioQueCausa() {
		return poblacion
	}
	
	override method transmiteEnfermedades() {
		return infectadaCon != null
	}
	
}

// objetos bien conocidos

object indicadores {
	
	var produccionStandard
	
	method produccionStandard(nivel) {
		produccionStandard = nivel
	}
	
	method produccionStandard() {
		return produccionStandard
	}
	
}

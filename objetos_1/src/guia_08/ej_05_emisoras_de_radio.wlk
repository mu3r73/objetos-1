
// emisoras

class Emisora {
	
	var programacion = #{}
	
	
	method estilosMusicales() {
		return programacion.map({
			programa => programa.estilosMusicales()
		}).flatten()
	}
	
	method puedeIncorporar(programa) {
		return not self.tieneAlgunProgramaQueSeSuperpongaCon(programa)
			and self.tieneAlgunEstiloIncluidoEn(programa)
	}
	
	method tieneAlgunProgramaQueSeSuperpongaCon(_programa) {
		return programacion.any({
			programa => programa.seSuperponeCon(_programa)
		})
	}
	
	method tieneAlgunEstiloIncluidoEn(programa) {
		return self.estilosMusicales().any({
			estilo => programa.incluyeEstilo(estilo)
		})
	}
	
	method agregarPrograma(programa) {
		if (not self.puedeIncorporar(programa)) {
			throw new RadioException("no se puede agregar el programa a la emisora")
		}
		programacion.add(programa)
	}
	
}

// programas

class Programa {
	// abstracta
	
	var horaInicio
	var horaFin
	var temas = #{}
	
	constructor(_horaInicio, _horaFin) {
		self.horaInicio(_horaInicio)
		self.horaFin(_horaFin)
	}
	
	method horaInicio(_horaInicio) {
		horaInicio = _horaInicio
	}
	
	method horaInicio() {
		return horaInicio
	}
	
	method horaFin(_horaFin) {
		horaFin = _horaFin
	}
	
	method horaFin() {
		return horaFin
	}
	
	method estilosMusicales()
	// abstracto
	
	method incluyeEstilo(estilo) {
		return self.estilosMusicales().contains(estilo)
	}
	
	method seSuperponeCon(programa) {
		// de Román:
		return horaFin > programa.horaInicio()
			and horaInicio < programa.horaFin()
//		return programa.horaInicio().between(horaInicio, horaFin)
//			or programa.horaFin().between(horaInicio, horaFin)
//			or (programa.horaInicio() < horaInicio
//				and programa.horaFin() > horaFin)
//			or (programa.horaInicio() > horaInicio
//				and programa.horaFin() < horaFin)
	}
	
	method puedeIncorporar(tema) {
		return self.duracionTotalDeTemas() + tema.duracion() <= self.duracionTotal()
	}
	
	method duracionTotal() {
		return horaFin - horaInicio
	}
	
	method duracionTotalDeTemas() {
		return temas.map({
			tema => tema.duracion()
		}).sum()
	}
	
	method incorporarTema(tema) {
		if (not self.puedeIncorporar(tema)) {
			throw new RadioException("no se puede incorporar el tema al programa")
		}
		temas.add(tema)
	}
	
	method estaCargado() {
		return self.duracionTotalDeTemas() > self.duracionTotal() * 0.8
	}
	
}

class ProgramaAutonomo inherits Programa {
	
	var estilosMusicales = #{}
	
	constructor(_horaInicio, _horaFin) = super(_horaInicio, _horaFin)
	
	method agregarEstiloMusical(estilo) {
		estilosMusicales.add(estilo)
	}
	
	method quitarEstiloMusical(estilo) {
		estilosMusicales.remove(estilo)
	}
	
	override method estilosMusicales() {
		return estilosMusicales
	}

	override method puedeIncorporar(tema) {
		return super(tema)
			and self.incluyeEstilo(tema.banda().estiloMusical())
	}
	
}

class ProgramaFinanciado inherits Programa {
	
	var empresaFinanciante
	
	constructor(_horaInicio, _horaFin, _empresaFinanciante) = super(_horaInicio, _horaFin) {
		self.empresaFinanciante(_empresaFinanciante)
	}
	
	method empresaFinanciante(_empresaFinanciante) {
		empresaFinanciante = _empresaFinanciante
	}
	
	override method estilosMusicales() {
		return empresaFinanciante.estilosMusicales()
	}
	
	override method puedeIncorporar(tema) {
		return super(tema)
			and empresaFinanciante.bandasPreferidas().includes(tema.banda())
	}
	
}

// empresas

class Empresa {
	
	var bandasPreferidas = #{}
	
	method agregarBandaPreferida(banda) {
		bandasPreferidas.add(banda)
	}
	
	method quitarBandaPreferida(banda) {
		bandasPreferidas.remove(banda)
	}
	
	method bandasPreferidas() {
		return bandasPreferidas
	}
	
	method estilosMusicales() {
		bandasPreferidas.map({
			banda => banda.estiloMusical()
		})
	}
		
}

// bandas

class Banda {
	
	var estiloMusical
	
	constructor(_estiloMusical) {
		self.estiloMusical(_estiloMusical)
	}
	
	method estiloMusical(_estiloMusical) {
		estiloMusical = _estiloMusical
	}
	
	method estiloMusical() {
		return estiloMusical
	}
	
}

// temas musicales

class Tema {
	
	const banda
	const duracion
	
	constructor(_banda, _duracion) {
		banda = _banda
		duracion = _duracion
	}
	
	method banda() {
		return banda
	}
	
	method duracion() {
		return duracion
	}
	
}

// excepciones

class RadioException inherits Exception {
	
	constructor(_message) = super(_message)
	
}

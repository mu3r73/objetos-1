
// equipos

class Equipo {
	
	var puntosJuego
	var rival
	var jugadores = #{}	// 1 guardian, 2 golpeadores, 3 cazadores, 1 buscador
	
	method agregarJugador(jugador) {
		jugadores.add(jugador)
		jugador.equipo(self)
	}
	
	method habilidadPromedio() {
		return jugadores.map({
			jugador => jugador.habilidad()
		}).sum() / jugadores.size()
	}
	
	method tieneJugadorEstrellaVs(equipoRival) {
		return jugadores.any({
			jugador => equipoRival.todosSonMenosHabilesQue(jugador)
		})
	}
	
	method todosSonMenosHabilesQue(jugadorRival) {
		return jugadores.all({
			jugador => jugadorRival.lePasaElTrapoA(jugador)
		})
	}
	
	method iniciarJuegoVs(equipoRival) {
		rival = equipoRival
		puntosJuego = 0
		jugadores.each({
			jugador => jugador.iniciarJuego()
		})
	}
	
	method jugarTurno() {
		jugadores.each({
			jugador => jugador.jugarTurnoVs(rival)
		})
		
	}
	
	method blancosUtiles() {
		return jugadores.filter({
			jugador => jugador.esBlancoUtil()
		})
	}
	
	method tieneQuaffle() {
		return jugadores.any({
			jugador => jugador.tieneQuaffle()
		})
	}
	
	method atraparQuaffle() {
		self.cazadorMasRapido().obtenerQuaffle()
	}
	
	method cazadorMasRapido() {
		return self.cazadores().max({
			jugador => jugador.velocidad()
		})
	}
	
	method cazadores() {
		return jugadores.filter({
			jugador => jugador.esCazador()
		})
	}
	
	method quienPuedeBloquearA(jugadorRival) {
		return jugadores.anyOne({
			jugador => jugador.puedeBloquearA(jugadorRival)
		})
	}
	
	method ganarPuntos(puntos) {
		puntosJuego += puntos
	}
	
	method ganarJuego() {
		rival.perderJuego()
		// etc.
	}
	
	method perderJuego() {
		// etc.
	}
	
	method terminarJuego() {
		// etc.
	}
	
}

// jugadores

class Jugador {
	// abstracta
	
	var peso
	var reflejos
	var escoba
	var equipo
	var skills
	var turnosAturdido = 0
	
	constructor(_peso, _reflejos, _escoba) {
		self.peso(_peso)
		self.reflejos(_reflejos)
		self.escoba(_escoba)
		self.skills(0)
	}
	
	method peso(_peso) {
		peso = _peso
	}
	
	method reflejos(_reflejos) {
		reflejos = _reflejos
	}
	
	method reflejos() {
		return reflejos
	}
	
	method escoba(_escoba) {
		escoba = _escoba
	}
	
	method equipo(_equipo) {
		equipo = _equipo
	}
	
	method skills(_skills) {
		skills = _skills
	}
	
	method skills() {
		return skills
	}
	
	method manejoDeEscoba() {
		return self.skills() / peso
	}
	
	method velocidad() {
		return escoba.velocidad() * self.manejoDeEscoba()
	}
	
	method habilidad()
	// abstracto
	
	method lePasaElTrapoA(jugadorRival) {
		return self.habilidad() >= jugadorRival.habilidad()
	}
	
	method esGrosso() {
		return self.habilidad() > equipo.habilidadPromedio()
			and self.velocidad() > indicadores.velocidadStandard()
	}
	
	method iniciarJuego()
	// abstracto
	
	method jugarTurnoVs(equipoRival) {
		if (self.estaAturdido()) {
			turnosAturdido -= 1
		}
	}
	
	method estaAturdido() {
		return turnosAturdido > 0
	}
	
	method puedeBloquearA(jugadorRival)
	// abstracto
	
	method esBlancoUtil()
	// abstracto
	
	method recibirGolpeDeBludger() {
		self.skills((skills - 2).max(0))
		escoba.recibirGolpe()
		if (self.esGrosso()) {
			turnosAturdido = 1
		}
	}
	
	method esCazador() {
		return false
	}
	
}

class Guardian inherits Jugador {
	
	var fuerza
	
	constructor(_peso, _reflejos, _escoba, _fuerza) = super(_peso, _reflejos, _escoba) {
		self.fuerza(_fuerza)
	}
	
	method fuerza(_fuerza) {
		fuerza = _fuerza
	}
	
	override method habilidad() {
		return self.velocidad() + self.skills() + reflejos + fuerza
	}
	
	override method iniciarJuego() {
		// no pasa nada
	}
	
	override method jugarTurnoVs(equipoRival) {
		// no pasa nada
		super(equipoRival)
	}
	
	override method puedeBloquearA(jugadorRival) {
		return 1.randomUpTo(3) == 3
	}
	
	override method esBlancoUtil() {
		return not equipo.tieneQuaffle()
	}
	
}

class Cazador inherits Jugador {
	
	var punteria
	var fuerza
	var tieneQuaffle
	
	constructor(_peso, _reflejos, _escoba, _punteria, _fuerza) = super(_peso, _reflejos, _escoba) {
		self.punteria(_punteria)
		self.fuerza(_fuerza)
	}
	
	method punteria(_punteria) {
		punteria = _punteria
	}
	
	method fuerza(_fuerza) {
		fuerza = _fuerza
	}
	
	override method esCazador() {
		return true
	}

	method obtenerQuaffle() {
		tieneQuaffle = true
	}
	
	method perderQuaffle() {
		tieneQuaffle = false
	}
	
	override method habilidad() {
		return self.velocidad() + self.skills() + punteria * fuerza
	}
	
	override method iniciarJuego() {
		// no pasa nada
	}
	
	override method jugarTurnoVs(equipoRival) {
		if (tieneQuaffle) {
			self.intentarHacerGolA(equipoRival)
		}
		super(equipoRival)
	}
	
	method intentarHacerGolA(equipoRival) {
		var bloqueador = equipoRival.quienPuedeBloquearA(self)
		if (bloqueador == null) {
			self.hacerGol()
		} else {
			self.recibirBloqueo(bloqueador, equipoRival)
		}
	}
	
	method hacerGol() {
		equipo.ganarPuntos(10)
		self.skills(skills + 5)
	}
	
	method recibirBloqueo(bloqueador, equipoRival) {
		self.skills((skills - 2).max(0))
		bloqueador.skills(bloqueador.skills() + 10)
		self.perderQuaffle()
		equipoRival.atraparQuaffle()
	}
	
	override method puedeBloquearA(jugadorRival) {
		return self.lePasaElTrapoA(jugadorRival)
	}
	
	override method esBlancoUtil() {
		return tieneQuaffle
	}
	
	method tieneQuaffle() {
		return tieneQuaffle
	}
	
	override method recibirGolpeDeBludger() {
		super()
		self.perderQuaffle()
	}
	
}

class Golpeador inherits Jugador {
	
	var punteria
	var fuerza
	
	constructor(_peso, _reflejos, _escoba, _punteria, _fuerza) = super(_peso, _reflejos, _escoba) {
		self.punteria(_punteria)
		self.fuerza(_fuerza)
	}
	
	method punteria(_punteria) {
		punteria = _punteria
	}
	
	method fuerza(_fuerza) {
		fuerza = _fuerza
	}
	
	override method habilidad() {
		return self.velocidad() + self.skills() + punteria + fuerza
	}
	
	override method iniciarJuego() {
		// no pasa nada
	}
	
	override method jugarTurnoVs(equipoRival) {
		if (not self.estaAturdido()) {
			self.intentarGolpearA(equipoRival)
		}
		super(equipoRival)
	}
	
	method intentarGolpearA(equipoRival) {
		var blanco = self.elegirBlancoUtil(equipoRival)
		if (self.puedeGolpearA(blanco)) {
			blanco.recibirGolpeDeBludger()
			self.skills(skills + 1)
		}		
	}
	
	method elegirBlancoUtil(equipoRival) {
		return equipoRival.blancosUtiles().anyOne()
	}
	
	method puedeGolpearA(jugadorRival) {
		return punteria > jugadorRival.reflejos()
	}
	
	override method puedeBloquearA(jugadorRival) {
		return self.esGrosso()
	}
	
	override method esBlancoUtil() {
		return false
	}
	
} 

class Buscador inherits Jugador {
	
	var vision
	var estaBuscando	// si es true, esta buscando el snitch
						// si es false, esta persiguiendo el snitch
	var turnosBuscando
	var kmRecorridos
	
	constructor(_peso, _reflejos, _escoba, _vision) = super(_peso, _reflejos, _escoba) {
		self.vision(_vision)
	}
	
	method vision(_vision) {
		vision = _vision
	}
	
	override method habilidad() {
		return self.velocidad() + self.skills() + reflejos * vision
	}
	
	override method iniciarJuego() {
		self.iniciarBusqueda()
	}
	
	method iniciarBusqueda() {
		estaBuscando = true
		turnosBuscando = 0
	}
	
	override method jugarTurnoVs(equipoRival) {
		if (not self.estaAturdido()) {
			self.buscarOPerseguirSnitch()
		}
		super(equipoRival)
	}
	
	method buscarOPerseguirSnitch() {
		if (estaBuscando) {
			self.jugarTurnoDeBusqueda()
		} else {
			self.jugarTurnoDePersecucion()
		}
	}
	
	method jugarTurnoDeBusqueda() {
		turnosBuscando += 1
		if ((1.randomUpTo(1000) < self.habilidad() + turnosBuscando)) {
			self.iniciarPersecucion()
		} 
	}
	
	method iniciarPersecucion() {
		estaBuscando = false
		turnosBuscando = 0
		kmRecorridos = 0		
	}
	
	method jugarTurnoDePersecucion() {
		kmRecorridos += self.velocidad() / 1.6
		if (kmRecorridos >= 5000) {
			self.atraparSnitch()
		}
	}
	
	method atraparSnitch() {
		self.skills(skills + 10)
		equipo.ganarPuntos(150)
		equipo.ganarJuego()
	}
	
	override method puedeBloquearA(jugadorRival) {
		return false
	}
	
	override method esBlancoUtil() {
		return not estaBuscando
			and 5000 - kmRecorridos < 1000
	}
	
	override method recibirGolpeDeBludger() {
		self.iniciarBusqueda()
	}
	
}

// escobas

class Escoba {
	// abstracta
	
	method velocidad()
	// abstracto
	
	method recibirGolpe()
	// abstracto
	
}

class Nimbus inherits Escoba {
	
	const anioDeFabricacion
	var salud // porcentaje
	
	constructor(_anioDeFabricacion) {
		anioDeFabricacion = _anioDeFabricacion
		self.salud(100)
	}
	
	method salud(porcentaje) {
		salud = porcentaje
	}
	
	override method velocidad() {
		return (80 - self.edad()) * salud
	}
	
	method edad() {
		return indicadores.anioActual() - anioDeFabricacion
	}
	
	override method recibirGolpe() {
		salud -= 10
	}
	
}

class Firebolt inherits Escoba {
	
	override method velocidad() {
		return 100
	}
	
	override method recibirGolpe() {
		// no pasa nada
	}
	
}

// objetos bien conocidos

object indicadores {
	
	var anioActual = 2016
	var velocidadStandard = 90
	
	method anioActual(anio) {
		anioActual = anio
	}
	
	method anioActual() {
		return anioActual
	}
	
	method velocidadStandard(velocidad) {
		velocidadStandard = velocidad
	}
	
	method velocidadStandard() {
		return velocidadStandard
	}
	
}


class Duenio {
	
	var amorPropio
	var edad
	var diversionBase
	var mascotas = #{}
	
	
	constructor(amor, anios, diversion) {
		amorPropio = amor
		self.edad(anios)
		diversionBase = diversion
	}
	
	method edad(anios) {
		edad = anios
	}
	
	method agregarMascota(mascota) {
		mascotas.add(mascota)
	}
	
	method quitarMascota(mascota) {
		mascotas.remove(mascota)
	}
	
	method amorPorMascota() {
		return amorPropio / mascotas.size()
	}
	
	method amorDeMascotas() {
		return mascotas.map({
			mascota => mascota.amor(self.amorPorMascota())
		}).sum()
	}
	
	method seguridadDeMascotas() {
		return mascotas.map({
			mascota => mascota.seguridad(edad)
		}).sum()
	}
	
	method diversionDeMascotas() {
		return mascotas.map({
			mascota => mascota.diversion(edad)
		}).sum()
	}
	
	method nivelDeAmor() {
		return self.amorDeMascotas()
	}
	
	method nivelDeSeguridad() {
		return edad.min(50) + self.seguridadDeMascotas()
	}
	
	method nivelDeDiversion() {
		return diversionBase + self.diversionDeMascotas()
	}
	
	method seSienteSeguro() {
		return self.nivelDeSeguridad() > 50
	}
	
	method esFeliz() {
		return self.seSienteSeguro()
			and (self.nivelDeAmor() + self.nivelDeDiversion() >= 2 * self.nivelDeSeguridad()) 
	}
	
	method puntosDeEntrenamiento() {
		return self.amorPorMascota()
	}
	
	method mascotasBuenas() {
		return mascotas.filter({
			mascota => mascota.esBuena(self.amorPorMascota(), edad)
		})
	}
	
	method entrenadoresRecomendados() {
		return self.mascotasBuenas().map({
			mascota => mascota.entrenador()
		})
	}
	
	method mejorEntrenador() {
		return self.entrenadoresRecomendados().max({
			entrenador => entrenador.puntosDeEntrenamiento()
		})
	}
	
}

class Entrenador {
	
	var experiencia
	
	constructor(_experiencia) {
		experiencia = _experiencia
	}
	
	method puntosDeEntrenamiento() {
		return experiencia
	}
	
}

class Mascota {
	// abstracta
	
	var entrenador
	
	method entrenador(_entrenador) {
		entrenador = _entrenador
	}
	
	method entrenador() {
		return entrenador
	}
	
	method amor(amorRecibido)
	// abstracto
	
	method seguridad(edadDelDuenio)
	// abstracto
	
	method diversion(edadDelDuenio)
	// abstracto
	
	method entrenar()
	// abstracto
	
	method esBuena(amorRecibido, edadDelDuenio) {
		return self.amor(amorRecibido) + self.seguridad(edadDelDuenio) + self.diversion(edadDelDuenio) > 150
	}
	
}

class Canario inherits Mascota {
	
	override method amor(amorRecibido) {
		return 0
	}
	
	override method seguridad(edadDelDuenio) {
		return 0
	}
	
	override method diversion(edadDelDuenio) {
		return 5
	}
	
	override method entrenar() {
		// nada sucede
	}
	
}

class Gato inherits Mascota {
	// requiere el objeto bien conocido: amorDevueltoPorGatos
	
	const seguridad
	var diversion
	
	constructor(_seguridad, _diversion) {
		seguridad = _seguridad
		diversion = _diversion
	}
	
	override method amor(amorRecibido) {
		return amorRecibido * amorDevueltoPorGatos.porcentaje() / 100
	}
	
	override method seguridad(edadDelDuenio) {
		return seguridad
	}
	
	override method diversion(edadDelDuenio) {
		return diversion
	}
	
	override method entrenar() {
		diversion += 15
	}
	
}

class Perro inherits Mascota {
	
	var nivelDeGuardian
	
	constructor(_nivelDeGuardian) {
		nivelDeGuardian = _nivelDeGuardian
	}
	
	method nivelDeGuardian() {
		return nivelDeGuardian
	}
	
	override method amor(amorRecibido) {
		return amorRecibido * 2		 	
	}
	
	override method seguridad(edadDelDuenio) {
		if (edadDelDuenio < 18) {
			return 30 + nivelDeGuardian + 20 
		} else {
			return 30 + nivelDeGuardian
		}
	}
	
	override method diversion(edadDelDuenio) {
		return 125 - edadDelDuenio
	}
	
	override method entrenar() {
		nivelDeGuardian += (entrenador.puntosDeEntrenamiento() * 0.1)
	}
	
}

class PerroLabrador inherits Perro {
	
	constructor(_nivelDeGuardian) = super(_nivelDeGuardian)
	
	override method amor(amorRecibido) {
	 	return super(amorRecibido) + 5
	}
	
	override method diversion(edadDelDuenio) {
		return 130
	}
	
}

// objetos bien conocidos

object amorDevueltoPorGatos {
	
	var porcentaje = 25
	
	
	method porcentaje(_porcentaje) {
		porcentaje = _porcentaje
	}
	
	method porcentaje() {
		return porcentaje
	}
	
}


// arboles

class Arbol {
	
	var vejez
	var altura
	const ramaje
	var objetos = #{}
	
	constructor(_vejez, _altura, _ramaje) {
		self.vejez(_vejez)
		self.altura(_altura)
		ramaje = _ramaje
	}
	
	method vejez(anios) {
		vejez = anios
	}
	
	method vejez() {
		return vejez
	}
	
	method altura(metros) {
		altura = metros
	}
	
	method altura() {
		return altura
	}
	
	method capacidadTotal() {
		return ramaje.capacidadTotal()
	}
	
	method capacidadDisponible() {
		return self.capacidadTotal() - self.espacioOcupado()
	}
	
	method espacioOcupado() {
		return objetos.map({
			objeto => objeto.espacioQueOcupa()
		}).sum()
	}
	
	method cantObjetosDestinadosA(alguien) {
		return self.objetosDestinadosA(alguien).size()
	}
	
	method objetosDestinadosA(alguien) {
		return objetos.filter({
			objeto => objeto.estaDestinadoA(alguien)
		})
	}
	
	method agregarAdorno(adorno) {
		if (not self.hayEspacioPara(adorno)) {
			throw new ArbolException("no hay espacio en el árbol para agregar el adorno")
		}
		objetos.add(adorno)
	}
	
	method incorporar(objeto) {
		if (not self.sePuedeIncorporar(objeto)) {
			throw new ArbolException("no hay espacio en el árbol para agregar el objeto")
		}
		objetos.add(objeto)
	}
	
	method sePuedeIncorporar(objeto) {
		return self.hayEspacioPara(objeto)
			and self.cantObjetosDestinadosA(objeto.destinatario()) >= 2
	}
	
	method hayEspacioPara(objeto) {
		return self.capacidadDisponible() >= objeto.espacioQueOcupa()
	}
	
	method costoTotal() {
		return objetos.map({
			objeto => objeto.precio()
		}).sum()
	}
	
}

// ramajes

class Ramaje {
	// abstracta
	
	const arbol
	
	constructor(_arbol) {
		arbol = _arbol
	}
	
	method capacidadTotal()
	// abstracto
	
}

class Natural inherits Ramaje {
	
	constructor(_arbol) = super(_arbol)
	
	override method capacidadTotal() {
		return arbol.vejez() * arbol.altura()
	}
	
}

class Frondoso inherits Natural {
	
	var varas
	
	constructor(_arbol, _varas) = super(_arbol) {
		self.varas(_varas)
	}
	
	method varas(_varas) {
		varas = _varas
	}
	
	override method capacidadTotal() {
		return super() + varas / 2
	}
	
}

class Artificial inherits Ramaje {
	
	var varas
	
	constructor(_arbol, _varas) = super(_arbol) {
		self.varas(_varas)
	}
	
	method varas(_varas) {
		varas = _varas
	}
		
	override method capacidadTotal() {
		return varas + arbol.altura()
	}
	
}

// objetos

class Objeto {
	// abstracta
	
	method precio()
	// abstracto
	
	method espacioQueOcupa()
	// abstracto
	
	method estaDestinadoA(alguien)
	// abstracto
	
}

class Regalo inherits Objeto {
	
	var precio
	var destinatario
	
	constructor(_precio, _destinatario) {
		precio = _precio
		destinatario = _destinatario
	}
	
	override method precio() {
		return precio
	}
	
	method destinatario() {
		return destinatario
	}
	
	override method espacioQueOcupa() {
		return 1
	}
	
	override method estaDestinadoA(alguien) {
		return alguien == destinatario
	}
	
}

class Tarjeta inherits Objeto {
	
	var destinatarios
	
	constructor(_destinatarios) {
		destinatarios = _destinatarios
	}
	
	method destinatarios() {
		return destinatarios
	}
	
	override method precio() {
		return 2
	}
	
	override method espacioQueOcupa() {
		return 0
	}
	
	override method estaDestinadoA(alguien) {
		return destinatarios.contains(alguien)
	}
	
}

class Adorno inherits Objeto {
	
	var peso
	var superioridad
	
	constructor(_peso) {
		peso = _peso
	}
	
	method superioridad(_superioridad) {
		superioridad = _superioridad
	}
	
	override method precio() {
		return peso * superioridad
	}
	
	override method espacioQueOcupa() {
		return 1
	}
	
	override method estaDestinadoA(alguien) {
		return false
	}
	
}

class TiraDeLuces inherits Adorno {
	
	var lamparitas
	
	constructor(_peso, _lamparitas) = super(_peso) {
		lamparitas = _lamparitas
	}
	
	method luminosidad() {
		return lamparitas
	}
	
	method superioridad() {
		return self.luminosidad() / 10
	}
	
}

class FigurasElaboradas inherits Adorno {
	
	constructor(_peso) = super(_peso)
	
	method superioridad() {
		return indicadores.superioridadDeFigurasElaboradas()
	}
	
}

// objetos bien conocidos

object indicadores {
	
	var superioridadDeFigurasElaboradas = 5
	
	method superioridadDeFigurasElaboradas(coeficiente) {
		superioridadDeFigurasElaboradas = coeficiente
	}
	
	method superioridadDeFigurasElaboradas() {
		return superioridadDeFigurasElaboradas
	}
	
}

// excepciones

class ArbolException inherits Exception {
	
	constructor(_message) = super(_message)
	
}

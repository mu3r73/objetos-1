
// usuarios

class Usuario {
	
	var publicaciones = #{}
	var amigos = #{}
	
	
	method agregarAmigo(_usuario) {
		// la amistad no necesita ser recíproca
		// (funciona como el 'seguir' de twitter)
		amigos.add(_usuario)
	}
	
	method publicarPublico(publicacion) {
		self.publicar(publicacion, new PermisoPublico())
	}
	
	method publicarSoloAmigos(publicacion) {
		self.publicar(publicacion, new PermisoSoloAmigos(self))
	}
	
	method publicarPrivadoConListaBlanca(publicacion, listaBlanca) {
		self.publicar(publicacion, new PermisoPrivadoConListaBlanca(listaBlanca))
	}
	
	method publicarPrivadoConListaNegra(publicacion, listaNegra) {
		self.publicar(publicacion, new PermisoPrivadoConListaNegra(listaNegra))
	}
	
	method publicar(publicacion, permiso) {
		if (self.haPublicado(publicacion)) {
			throw new PublicacionException("la publicación ya ha sido publicada por el usuario")
		}
		publicacion.permiso(permiso)
		publicaciones.add(publicacion)
	}
	
	method haPublicado(publicacion) {
		return publicaciones.contains(publicacion)
	}
	
	method tieneAmigo(_usuario) {
		return amigos.contains(_usuario)
	}
	
	method espacioTotalOcupadoPorPublicaciones() {
		return publicaciones.map({
			publicacion => publicacion.espacioQueOcupa()
		}).sum()
	}
	
	method meGusta(publicacion) {
		publicacion.recibirMeGusta(self)
	}
	
	method puedeVer(publicacion) {
		return publicacion.permiteAccesoA(self)
	}
	
	method cantDeAmigos() {
		return amigos.size()
	}
	
	method esMasAmistosoQue(_usuario) {
		return self.cantDeAmigos() > _usuario.cantDeAmigos()
	}
	
	method permiteVerTodasLasPublicacionesA(_usuario) {
		return publicaciones.all({
			publicacion => _usuario.puedeVer(publicacion)
		})
	}
	
	method mejoresAmigos() {
		return amigos.filter({
			amigo => self.permiteVerTodasLasPublicacionesA(amigo)
		})
	}
	
	method totalDeMeGustasRecibidos() {
		return publicaciones.map({
			publicacion => publicacion.cantDeMeGustasRecibidos()
		}).sum()
	}
	
	method amigoMasPopular() {
		return amigos.max({
			amigo => amigo.totalDeMeGustasRecibidos()
		})
	}
	
}

// publicaciones

class Publicacion {
	// abstracta
	
	var permiso
	var usuariosAQuienesLesGusta = #{}
	
	
	method espacioQueOcupa()
	// abstracto
	
	method permiso(_permiso) {
		permiso = _permiso
	}
	
	method perteneceA(usuario) {
		return usuario.haPublicado(self)
	}
	
	method permiteAccesoA(usuario) {
		return self.perteneceA(usuario)
			or permiso.estaAutorizado(usuario)
	}
	
	method recibirMeGusta(usuario) {
		// el dueño de la publicación también puede indicar 'me gusta'
		usuariosAQuienesLesGusta.add(usuario)
	}
	
	method cantDeMeGustasRecibidos() {
		return usuariosAQuienesLesGusta.size()
	}
	
	method usuariosAQuienesLesGusta() {
		return usuariosAQuienesLesGusta
	}
	
}

class Foto inherits Publicacion {
	// requiere el objeto bien conocido entornoFaceless
	
	var alto
	var ancho
	
	constructor(_alto, _ancho) {
		self.alto(_alto)
		self.ancho(_ancho)
	}
	
	method alto(_alto) {
		alto = _alto
	}
	
	method ancho(_ancho) {
		ancho = _ancho
	}
	
	override method espacioQueOcupa() {
		return alto * ancho * entornoFaceless.factorDeCompresionDeFotos()
	}
	
}

class Texto inherits Publicacion {
	
	var cantDeCaracteres
	
	constructor(_cantDeCaracteres) {
		self.cantDeCaracteres(_cantDeCaracteres)
	}
	
	method cantDeCaracteres(_cantDeCaracteres) {
		cantDeCaracteres = _cantDeCaracteres
	}
	
	override method espacioQueOcupa() {
		return cantDeCaracteres
	}
	
}

class Video inherits Publicacion {
	
	var duracion
	
	constructor(_duracion) {
		self.duracion(_duracion)
	}
	
	method duracion(_duracion) {
		duracion = _duracion
	}
	
}

class VideoNormal inherits Video {
	
	constructor(_duracion) = super(_duracion)
	
	override method espacioQueOcupa() {
		return duracion
	}
	
}

class VideoHD inherits Video {
	
	constructor(_duracion) = super(_duracion)
	
	override method espacioQueOcupa() {
		return duracion * 3
	}
	
}

// permisos

class Permiso {
	// abstracta
	
	method estaAutorizado(_usuario)
	// abstracto
		
}

class PermisoPublico inherits Permiso {
	
	override method estaAutorizado(_usuario) {
		return true
	}
	
}

class PermisoSoloAmigos inherits Permiso {
	
	const duenio
	
	constructor(_duenio) {
		duenio = _duenio
	}
	
	override method estaAutorizado(_usuario) {
		return duenio.tieneAmigo(_usuario)
	}
	
}

class PermisoPrivadoConListaBlanca inherits Permiso {
	
	const listaBlanca
	
	constructor(_listaBlanca) {
		listaBlanca = _listaBlanca 
	}
	
	override method estaAutorizado(_usuario) {
		return listaBlanca.contains(_usuario)
	}
	
}

class PermisoPrivadoConListaNegra inherits Permiso {
	
	const listaNegra
	
	constructor(_listaNegra) {
		listaNegra = _listaNegra
	}
	
	override method estaAutorizado(_usuario) {
		return not listaNegra.contains(_usuario)
	}
	
}

// objetos bien conocidos

object entornoFaceless {
	
	var factorDeCompresionDeFotos = 0.7
	
	
	method factorDeCompresionDeFotos(factor) {
		factorDeCompresionDeFotos = factor
	}
	
	method factorDeCompresionDeFotos() {
		return factorDeCompresionDeFotos
	}
	
}

// excepciones

class PublicacionException inherits Exception {
	
	constructor(msg) = super(msg)
	
}

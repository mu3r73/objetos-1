// Cupidos

class Cupido {
	
	var seducibles = #{}
	
	
	method agregarSeducible(seducible) {
		seducibles.add(seducible)
	}
	
	method quitarSeducible(seducible) {
		seducibles.remove(seducible)
	}
	
	method flechados() {
		return seducibles.map({
			seducible => seducible.pareja()
		})
	}
	
	method artilugiosOstentososDeFlechados() {
		return self.flechados().map({
			flechado => flechado.ostentar()
		})
	}
	
}

// seducibles

class Seducible {
	
	var generosPreferidos
	var pareja
	
	constructor(_generosPreferidos) {
		self.generosPreferidos(_generosPreferidos)
	}
	
	method generosPreferidos(_generosPreferidos) {
		generosPreferidos = _generosPreferidos
	}
	
	method pareja(seductor) {
		pareja = seductor
	}
	
	method pareja() {
		return pareja
	}
	
	method aceptaCitaCon(seductor) {
		return pareja == seductor
			or (pareja == null
				and generosPreferidos.contains(seductor.genero())
			)
	}
	
	method tenerCitaCon(seductor)
	// abstracto
	
	method formarParejaCon(seductor) {
		pareja = seductor
	}
	
}

class Cazafortunas inherits Seducible {
	
	constructor(_generosPreferidos) = super(_generosPreferidos)
	
	override method aceptaCitaCon(seductor) {
		return super(seductor)
			and seductor.esMillonario()
	} 
	
	override method tenerCitaCon(seductor) {
		seductor.artilugios().each({
			artilugio => artilugio.consecuenciaEconomica()
		})
	}
	
}

class Militante inherits Seducible {
	
	var nivelIntelectualPreferido
	
	constructor(_generosPreferidos, _nivelIntelectual) = super(_generosPreferidos) {
		self.nivelIntelectualPreferido(_nivelIntelectual)
	}
	
	method nivelIntelectualPreferido(_nivelIntelectual) {
		nivelIntelectualPreferido = _nivelIntelectual
	}
	
	override method aceptaCitaCon(seductor) {
		return super(seductor)
			and seductor.nivelIntelectualConArtilugios() > nivelIntelectualPreferido
			and (seductor.esMillonario()
				or seductor.aspectoPersonalConArtilugios() > 2 * seductor.nivelIntelectualConArtilugios()
			)
	} 
	
	override method tenerCitaCon(seductor) {
		// no pasa nada
	}
	
}

class Envidioso inherits Seducible {
	
	var rival
	
	constructor(_generosPreferidos, _rival) = super(_generosPreferidos) {
		self.rival(_rival)
	}
	
	method rival(_rival) {
		rival = _rival
	}
	
	override method aceptaCitaCon(seductor) {
		return super(seductor)
			and rival.aceptaCitaCon(seductor)
			// loop infinito si:
			// envidioso1 rival de envidioso2
			// y envidioso2 rival de envidioso1 
	} 
	
	override method tenerCitaCon(seductor) {
		seductor.artilugios().each({
			artilugio =>
				artilugio.consecuenciaEconomica()
				artilugio.consecuenciaMoral()
		})
	}
	
}

class Libre inherits Seducible {
	
	constructor(_generosPreferidos) = super(_generosPreferidos)
	
	override method tenerCitaCon(seductor) {
		seductor.artilugios().each({
			artilugio => artilugio.consecuenciaMoral()
		})
	}
	
}

// seductores

class Seductor {
	// requiere el objeto bien conocido indicadores
	
	var genero
	var nivelEconomico
	var nivelIntelectual
	var aspectoPersonal
	var artilugios = #{}
	var parejas = #{}
	
	constructor(_genero, _nivelEconomico, _nivelIntelectual, _aspectoPersonal) {
		self.genero(_genero)
		self.nivelEconomico(_nivelEconomico)
		self.nivelIntelectual(_nivelIntelectual)
		self.aspectoPersonal(_aspectoPersonal)
	}
	
	method multiplicador() {
		return 1
	}
	
	method genero(_genero) {
		genero = _genero
	}
	
	method genero() {
		return genero
	}
	
	method nivelEconomico(nivel) {
		nivelEconomico = nivel
	}
	
	method nivelEconomico() {
		return nivelEconomico
	}
	
	method nivelEconomicoConArtilugios() {
		return nivelEconomico
			+ self.nivelEconomicoDeArtilugios()
	}
	
	method esMillonario() {
		return self.nivelEconomicoConArtilugios() > indicadores.nivelDeMillonario()
	}
	
	method nivelIntelectual(nivel) {
		nivelIntelectual = nivel
	}
	
	method nivelIntelectual() {
		return nivelIntelectual
	}
	
	method nivelIntelectualConArtilugios() {
		return nivelIntelectual
			+ self.nivelIntelectualDeArtilugios()
	}
	
	method aspectoPersonal(nivel) {
		aspectoPersonal = nivel
	}
	
	method aspectoPersonal() {
		return aspectoPersonal
	}
	
	method aspectoPersonalConArtilugios() {
		return aspectoPersonal
			+ self.aspectoPersonalDeArtilugios()
	}
	
	method agregarArtilugio(artilugio) {
		artilugios.add(artilugio)
	}
	
	method quitarArtilugio(artilugio) {
		artilugios.remove(artilugio)
	}
	
	method artilugios() {
		return artilugios
	}
	
	method nivelEconomicoDeArtilugios() {
		return artilugios.map({
			artilugio => artilugio.modificadorNivelEconomico(nivelEconomico, self.multiplicador())
		}).sum()
	}
	
	method nivelIntelectualDeArtilugios() {
		return artilugios.map({
			artilugio => artilugio.modificadorNivelIntelectual(nivelIntelectual, self.multiplicador())
		}).sum()
	}
	
	method aspectoPersonalDeArtilugios() {
		return artilugios.map({
			artilugio => artilugio.modificadorAspectoPersonal(aspectoPersonal, self.multiplicador())
		}).sum()
	}
	
	method seducirA(seducible) {
		if (seducible.aceptaCitaCon(self)) {
			seducible.tenerCitaCon(self)
		}
		if (seducible.aceptaCitaCon(self)) {
			seducible.formarParejaCon(self)
			self.formarParejaCon(seducible)
		}
	}
	
	method formarParejaCon(seducible) {
		parejas.add(seducible)
	}
	
	method romperParejaCon(seducible) {
		parejas.remove(seducible)
	}
	
	method artilugiosEconomicosCapos() {
		return artilugios.filter({
			artilugio => artilugio.esCapo() 
		})
	}
	
	method ostentar() {
		return self.artilugiosEconomicosCapos().anyOne()
	}
	
}

class SeductorEstrella inherits Seductor {
	
	constructor(_genero,_nivelEconomico,_nivelIntelectual,_aspectoPersonal) = super(_genero,_nivelEconomico,_nivelIntelectual,_aspectoPersonal)
	
	override method multiplicador() {
		return 3
	}
	
}

// artilugios

class Artilugio {
	// abstracta
	
	var duenio
	
	constructor(_duenio) {
		self.duenio(_duenio)
	}
	
	method duenio(_duenio) {
		duenio = _duenio
	}
	
	method modificadorNivelEconomico()
	// abstracto
	
	method modificadorNivelIntelectual()
	// abstracto
	
	method modificadorAspectoPersonal()
	// abstracto
	
	method consecuenciaEconomica()
	// abstracto
	
	method consecuenciaMoral()
	// abstracto
	
	method esCapo() {
		return self.modificadorNivelEconomico() > duenio.nivelEconomico() * 0.5
	}
	
}

class Billetera inherits Artilugio {
	
	var pesos
	
	constructor(_duenio, _pesos) = super(_duenio) {
		self.pesos(_pesos)
	}
	
	method pesos(_pesos) {
		pesos = _pesos
	}
	
	override method modificadorNivelEconomico() {
		return pesos * duenio.multiplicador()
	}
	
	override method modificadorNivelIntelectual() {
		if (duenio.esMillonario()) {
			return -(duenio.nivelIntelectual() * 0.2 * duenio.multiplicador())
		} else {
			return 0
		}
	}
	
	override method modificadorAspectoPersonal() {
		return duenio.aspectoPersonal() * 0.1 * duenio.multiplicador()
	}
	
	override method consecuenciaEconomica() {
		pesos -= pesos * 0.1
	}
	
	override method consecuenciaMoral() {
		// no pasa nada
	}
	
}

class Auto inherits Artilugio {
	
	var precio
	
	constructor(_duenio, _precio) = super(_duenio) {
		self.precio(_precio)
	}
	
	method precio(_precio) {
		precio = _precio
	}
	
	override method modificadorNivelEconomico() {
		return -(precio * 0.1 * duenio.multiplicador())
	}
	
	override method modificadorNivelIntelectual() {
		return 0
	}
	
	override method modificadorAspectoPersonal() {
		return precio * 2 * duenio.multiplicador()
	}

	override method consecuenciaEconomica() {
		precio -= precio * 0.05
	}
	
	override method consecuenciaMoral() {
		precio -= precio * 0.05
	}
	
}

class Reputacion inherits Artilugio {
	
	var nivelEconomico
	var nivelIntelectual
	var aspectoPersonal
	
	constructor(_duenio, _nivelEconomico, _nivelIntelectual, _aspectoPersonal) = super(_duenio) {
		self.nivelEconomico(_nivelEconomico)
		self.nivelIntelectual(_nivelIntelectual)
		self.aspectoPersonal(_aspectoPersonal)
	}
	
	method nivelEconomico(_nivelEconomico) {
		nivelEconomico = _nivelEconomico
	}
	
	method nivelIntelectual(_nivelIntelectual) {
		nivelIntelectual = _nivelIntelectual
	}
	
	method aspectoPersonal(_aspectoPersonal) {
		aspectoPersonal = _aspectoPersonal
	}
	
	override method modificadorNivelEconomico() {
		return nivelEconomico * duenio.multiplicador()
	}
	
	override method modificadorNivelIntelectual() {
		return nivelIntelectual * duenio.multiplicador()
	}
	
	override method modificadorAspectoPersonal() {
		return aspectoPersonal * duenio.multiplicador()
	}

	override method consecuenciaEconomica() {
		nivelEconomico -= 10
	}
	
	override method consecuenciaMoral() {
		nivelIntelectual -= 5
		aspectoPersonal -= 5
	}
	
}

class TituloTecnicoProgramador inherits Artilugio {
	
	constructor(_duenio) = super(_duenio)
	
	override method modificadorNivelEconomico() {
		return duenio.nivelEconomico() * 0.8 * duenio.multiplicador()
	}
	
	override method modificadorNivelIntelectual() {
		return duenio.nivelIntelectual() * duenio.multiplicador()
	}
	
	override method modificadorAspectoPersonal() {
		return 0
	}
	
	override method consecuenciaEconomica() {
		// no pasa nada
	}
	
	override method consecuenciaMoral() {
		// no pasa nada
	}
	
}

// objetos bien conocidos

object indicadores {
	
	var nivelDeMillonario = 1000000
	
	method nivelDeMillonario(pesos) {
		nivelDeMillonario  = pesos
	}
	
	method nivelDeMillonario() {
		return nivelDeMillonario 
	}
	
}


import ej_09_faceless.*

// escenarios pedidos en el ejercicio

test "Usuario publica un contenido dado, con un permiso dado" {
	
	const u = new Usuario()
	const u1 = new Usuario()
	const u2 = new Usuario()
	u.agregarAmigo(u2)
	
	const p = new Texto(100)
	
	u.publicarPrivadoConListaNegra(p, #{u, u2})
	
	// usuario se incluyó a sí mismo en la lista negra 
	// ... pero como es el dueño, igual puede ver la publicación 
	assert.that(u.puedeVer(p))
	
	// usuario u1 no está en la lista negra => puede ver la publicación
	assert.that(u1.puedeVer(p))
	
	// usuario u2 está en la lista negra => no puede ver la publicación
	// (aunque es amigo de quien la publicó)
	assert.notThat(u2.puedeVer(p))
	
}

test "Publicar un video que sea accesible sólo a los amigos de un usuario" {
	
	const u = new Usuario()
	const u1 = new Usuario()
	const u2 = new Usuario()
	const u3 = new Usuario()
	u.agregarAmigo(u1)
	u.agregarAmigo(u2)
	
	const p = new VideoNormal(180)
	
	u.publicarSoloAmigos(p)
	
	// puede ver su propia publicación
	assert.that(u.puedeVer(p))
	// los amigos pueden ver la publicación
	assert.that(u1.puedeVer(p) and u2.puedeVer(p))
	// los que no son amigos no pueden ver la publicación
	assert.notThat(u3.puedeVer(p))
	
}

// otros tests varios

// usuarios

test "Usuario: amigoMasPopular (amigos con distinta cantidad de 'me gusta's)" {
	
	const u = new Usuario()
	const u1 = new Usuario()
	const u2 =  new Usuario()
	u.agregarAmigo(u1)
	u.agregarAmigo(u2)
	
	const p1 = new Texto(100)
	const p2 = new Texto(100)
	const p3 = new Texto(100)
	
	u1.publicarPublico(p1)
	u2.publicarPublico(p2)
	u2.publicarPublico(p3)
	
	u.meGusta(p1)
	u.meGusta(p2)
	u.meGusta(p3)
	
	assert.equals(u2, u.amigoMasPopular())
	
}

test "Usuario: amigoMasPopular (amigos con igual cantidad de 'me gusta's)" {
	
	const u = new Usuario()
	const u1 = new Usuario()
	const u2 =  new Usuario()
	u.agregarAmigo(u1)
	u.agregarAmigo(u2)
	
	const p1 = new Texto(100)
	const p2 = new Texto(100)
	
	u1.publicarPublico(p1)
	u2.publicarPublico(p2)
	
	u.meGusta(p1)
	u.meGusta(p2)
	
	assert.that(#{u1, u2}.contains(u.amigoMasPopular()))
	
}

test "Usuario: amigoMasPopular (sin amigos)" {
	
	const u = new Usuario()
	
	assert.throwsException({
		u.amigoMasPopular()
	})
	
}

test "Usuario: totalDeMeGustasRecibidos (con publicaciones)" {
	
	const ud = new Usuario()
	const u1 = new Usuario()
	const u2 = new Usuario()
	const u3 = new Usuario()
	
	const p1 = new Texto(100)
	const p2 = new Texto(100)
	
	ud.publicarPublico(p1)
	ud.publicarPublico(p2)
	
	u1.meGusta(p1)
	u2.meGusta(p1)
	u2.meGusta(p2)
	u3.meGusta(p2)
	
	assert.equals(4, ud.totalDeMeGustasRecibidos())
	
}

test "Usuario: totalDeMeGustasRecibidos (sin publicaciones)" {
	
	const u = new Usuario()
	
	assert.equals(0, u.totalDeMeGustasRecibidos())
	
}

test "Usuario: mejoresAmigos (con publicaciones) == sólo los amigos que pueden ver todo" {
	
	const ud = new Usuario()
	const u1 = new Usuario()
	const u2 = new Usuario()
	ud.agregarAmigo(u1)
	ud.agregarAmigo(u2)
	
	const p1 = new Texto(100)
	const p2 = new Texto(100)
	
	ud.publicarSoloAmigos(p1)
	ud.publicarPrivadoConListaBlanca(p2, #{u2})
	
	assert.equals(#{u2}, ud.mejoresAmigos())
	
}

test "Usuario: mejoresAmigos (sin publicaciones) == todos los amigos" {
	
	const ud = new Usuario()
	const u1 = new Usuario()
	const u2 = new Usuario()
	ud.agregarAmigo(u1)
	ud.agregarAmigo(u2)
	
	assert.equals(#{u1, u2}, ud.mejoresAmigos())
	
}

test "Usuario: permiteVerTodasLasPublicacionesA(usr_al_que_le_permite_ver_todo) == true" {
	
	const ud  = new Usuario()
	const u = new Usuario()
	ud.agregarAmigo(u)
	
	const p1 = new Texto(100)
	const p2 = new Texto(100)
	
	ud.publicarPrivadoConListaBlanca(p1, #{u})
	ud.publicarSoloAmigos(p2)
	
	assert.that(ud.permiteVerTodasLasPublicacionesA(u))
	
}

test "Usuario: permiteVerTodasLasPublicacionesA(usr_al_que_NO_le_permite_ver_alguna_pub) == false" {
	
	const ud  = new Usuario()
	const u = new Usuario()
	
	const p1 = new Texto(100)
	const p2 = new Texto(100)
	
	ud.publicarPublico(p1)
	ud.publicarSoloAmigos(p2)
	
	assert.notThat(ud.permiteVerTodasLasPublicacionesA(u))
	
}

test "Usuario: permiteVerTodasLasPublicacionesA (sin publicaciones) == true" {
	
	const u1  = new Usuario()
	const u2 = new Usuario()
	
	assert.that(u1.permiteVerTodasLasPublicacionesA(u2))
	
}

// Usuario: puedeVer -> ver Publicacion: permiteAccesoA

test "Usuario: esMasAmistosoQue(cant amigos de u1 > cant. amigos de u2) == true" {
	
	const u1 = new Usuario()
	const u2 = new Usuario()
	
	u1.agregarAmigo(u2)
	
	assert.that(u1.esMasAmistosoQue(u2))
	
}

test "Usuario: esMasAmistosoQue(cant amigos de u1 == cant. amigos de u2) == false" {
	
	const u1 = new Usuario()
	const u2 = new Usuario()
	
	assert.notThat(u1.esMasAmistosoQue(u2))
	
}

test "Usuario: esMasAmistosoQue(cant amigos de u1 < cant. amigos de u2) == false" {
	
	const u1 = new Usuario()
	const u2 = new Usuario()
	
	u2.agregarAmigo(u1)
	
	assert.notThat(u1.esMasAmistosoQue(u2))
	
}

// Usuario: meGusta -> ver Publicacion: recibirMeGusta

test "Usuario: espacioTotalOcupadoPorPublicaciones (con publicaciones)" {
	
	const u = new Usuario()
	
	const p1 = new Foto(160, 90)
	const p2 = new Texto(100)
	const p3 = new VideoNormal(180)
	
	u.publicarPublico(p1)
	u.publicarPublico(p2)
	u.publicarPublico(p3)
	
	assert.equals(p1.espacioQueOcupa() + p2.espacioQueOcupa() + p3.espacioQueOcupa(), u.espacioTotalOcupadoPorPublicaciones())
	
}

test "Usuario: espacioTotalOcupadoPorPublicaciones (sin publicaciones)" {
	
	const u = new Usuario()
	
	assert.equals(0, u.espacioTotalOcupadoPorPublicaciones())
	
}

test "Usuario: tieneAmigo(amigo) == true" {
	
	const ud = new Usuario()
	const ua = new Usuario()
	
	ud.agregarAmigo(ua)
	
	assert.that(ud.tieneAmigo(ua))
	
}

test "Usuario: tieneAmigo(NO_amigo) == false" {
	
	const ud = new Usuario()
	const u = new Usuario()
	
	assert.notThat(ud.tieneAmigo(u))
	
}

test "Usuario: haPublicado(publicacion_propia) == true" {
	
	const ud = new Usuario()
	const p = new Texto(100)
	
	ud.publicarPublico(p)
	
	assert.that(ud.haPublicado(p))
	
}

test "Usuario: haPublicado(publicacion_ajena) == false" {
	
	const ud = new Usuario()
	const u = new Usuario()
	const p = new Texto(100)
	
	ud.publicarPublico(p)
	
	assert.notThat(u.haPublicado(p))
	
}

test "Usuario: publicar(publicacion_ya_publicada)" {
	
	const u = new Usuario()
	const p = new Texto(100)
	
	u.publicarPublico(p)
	
	assert.throwsExceptionWithType(new PublicacionException(""), {
		=> u.publicarPublico(p)
	})
	
}

// publicaciones

test "Publicacion: cantDeMeGustasRecibidos (le gusta a alguien)" {
	
	const ud = new Usuario()
	const p = new Texto(100)
	
	ud.publicarSoloAmigos(p)

	p.recibirMeGusta(ud)
	
	assert.equals(1, p.cantDeMeGustasRecibidos())
	
}

test "Publicacion: usuariosAQuienesLesGusta (le gusta a alguien)" {
	
	const ud = new Usuario()
	const p = new Texto(100)
	
	ud.publicarSoloAmigos(p)

	p.recibirMeGusta(ud)
	
	assert.equals(#{ud}, p.usuariosAQuienesLesGusta())
	
}

test "Publicacion: cantDeMeGustasRecibidos (no le gusta a nadie)" {
	
	const p = new Texto(100)
	
	assert.equals(0, p.cantDeMeGustasRecibidos())
	
}

test "Publicacion: usuariosAQuienesLesGusta (no le gusta a nadie)" {
	
	const p = new Texto(100)
	
	assert.equals(#{}, p.usuariosAQuienesLesGusta())
	
}

test "Publicacion: permiteAccesoA(duenio) == true" {
	
	const ud = new Usuario()
	const p = new Texto(100)
	
	ud.publicarSoloAmigos(p)
	
	assert.that(p.permiteAccesoA(ud))
	
}

test "Publicacion: permiteAccesoA(usuario_autorizado) == true" {
	
	const ud = new Usuario()
	const ua = new Usuario()
	ud.agregarAmigo(ua)
	
	const p = new Texto(100)
	
	ud.publicarSoloAmigos(p)
	
	assert.that(p.permiteAccesoA(ua))
	
}

test "Publicacion: permiteAccesoA(usuario_NO_autorizado) == false" {
	
	const ud = new Usuario()
	const u = new Usuario()
	
	const p = new Texto(100)
	
	ud.publicarSoloAmigos(p)
	
	assert.notThat(p.permiteAccesoA(u))
	
}

// Publicacion: perteneceA -> ver Usuario: haPublicado

// permisos

test "PermisoPrivadoConListaNegra: estaAutorizado(usuario_NO_en_lista) == true" {
	
	const u = new Usuario()
	const una = new Usuario()
	
	const p = new PermisoPrivadoConListaNegra(#{una})
	
	assert.that(p.estaAutorizado(u))
	
}

test "PermisoPrivadoConListaNegra: estaAutorizado(usuario_en_lista) == false" {
	
	const una = new Usuario()
	
	const p = new PermisoPrivadoConListaNegra(#{una})
	
	assert.notThat(p.estaAutorizado(una))
	
}

test "PermisoPrivadoConListaBlanca: estaAutorizado(usuario_en_lista) == true" {
	
	const ua = new Usuario()
	
	const p = new PermisoPrivadoConListaBlanca(#{ua})
	
	assert.that(p.estaAutorizado(ua))
	
}

test "PermisoPrivadoConListaBlanca: estaAutorizado(usuario_NO_en_lista) == false" {
	
	const ua = new Usuario()
	const u = new Usuario()
	
	const p = new PermisoPrivadoConListaBlanca(#{ua})
	
	assert.notThat(p.estaAutorizado(u))
	
}

test "PermisoSoloAmigos: estaAutorizado(amigo) == true" {
	
	const u = new Usuario()
	const ua = new Usuario()
	u.agregarAmigo(ua)
	
	const p = new PermisoSoloAmigos(u)
	
	assert.that(p.estaAutorizado(ua))
	
}

test "PermisoSoloAmigos: estaAutorizado(NO_amigo) == false" {
	
	const u = new Usuario()
	const una = new Usuario()
	
	const p = new PermisoSoloAmigos(u)
	
	assert.notThat(p.estaAutorizado(una))
	
}

test "PermisoPublico: estaAutorizado" {
	
	const p = new PermisoPublico()
	
	assert.that(p.estaAutorizado(new Usuario()))
	
}

// publicaciones

test "VideoNormal: espacioQueOcupa" {
	
	const vn = new VideoNormal(444)
	assert.equals(444, vn.espacioQueOcupa())
	
}

test "VideoHD: espacioQueOcupa" {
	
	const vhd = new VideoHD(444)
	assert.equals(444 * 3, vhd.espacioQueOcupa())
	
}

test "Texto: espacioQueOcupa" {
	
	const t = new Texto(555)
	assert.equals(555, t.espacioQueOcupa())
	
}

test "Foto: espacioQueOcupa" {
	
	const f = new Foto(848, 480)
	assert.equals(848 * 480 * entornoFaceless.factorDeCompresionDeFotos(), f.espacioQueOcupa())
	
}

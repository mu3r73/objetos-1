
// ejercicio corregido y ampliado según implementación de Román

class Cuenta {
	// abstracta
	
	method depositar(suma) {
		if (suma < 0) {
			throw new CuentaException("No se puede depositar una suma negativa: " + suma)
		}
	}
	
	method extraer(suma) {
		if (suma < 0) {
			throw new CuentaException("No se puede extraer una suma negativa: " + suma)
		}
		if (suma > self.saldo()) {
			throw new CuentaException("Saldo " + self.saldo() + " insuficiente para extraer: " + suma)
		}
	}
	
	method saldo()
	// abstracto
	
	method restaurarSaldo()
	// abstracto
	
}

class CuentaBasica inherits Cuenta {
	// abstracta
	
	var saldo = 0.0
	var saldoAnterior = 0.0
	
	override method depositar(suma) {
		super(suma)
		saldo += self.aDepositar(suma)
	}
	
	override method extraer(suma) {
		super(suma)
		saldo -= self.aExtraer(suma)
	}
	
	method aDepositar(suma)
	// abstracto
	
	method aExtraer(suma)
	// abstracto
	
	override method saldo() {
		return saldo
	}
	
	override method restaurarSaldo() {
		saldo = saldoAnterior
	}
	
}


class CuentaNormal inherits CuentaBasica {
	
	override method aDepositar(suma) {
		return suma
	}
	
	override method aExtraer(suma) {
		return suma
	}
	
}

class CuentaEmbargada inherits CuentaBasica {
	
	override method aDepositar(suma) {
		return suma * 0.80
	}
	
	override method aExtraer(suma) {
		if (self.saldo() - suma > 5) {
			return suma + 5
		} else {
			return suma
		}
	}
	
}

class CuentaDolarizada inherits CuentaBasica {
	// usa el objeto bien conocido cotizacionDelDolar
	
	override method aDepositar(pesos) {
		return pesos / cotizacionDelDolar.venta()
	}
	
	override method aExtraer(pesos) {
		return pesos / cotizacionDelDolar.compra()
	}
	
	override method saldo() {
		return self.saldo() * cotizacionDelDolar.compra()
	}
	
}

class CuentaCombinada inherits Cuenta {
	
	const cuentaPrimaria
	const cuentaSecundaria
	
	constructor(cPrimaria, cSecundaria) {
		cuentaPrimaria = cPrimaria
		cuentaSecundaria = cSecundaria
	}
	
	override method saldo() {
		return cuentaPrimaria.saldo() + cuentaSecundaria.saldo()
	}
	
	override method depositar(suma) {
		super(suma)
		if (cuentaSecundaria.saldo() < 1000) {
			cuentaSecundaria.depositar(suma)
		} else {
			cuentaPrimaria.depositar(suma)
		}
	}
	
	override method extraer(suma) {
		super(suma)
		if (suma <= cuentaPrimaria.saldo()) {
			cuentaPrimaria.extraer(suma)
		} else {
			cuentaSecundaria.extraer(suma - cuentaPrimaria.saldo())
			cuentaPrimaria.extraer(cuentaPrimaria.saldo())
		}
	}
	
	override method restaurarSaldo() {
		cuentaPrimaria.restaurarSaldo()
		cuentaSecundaria.restaurarSaldo()
	} 
	
}

class CuentaException inherits Exception {
	
	constructor(msg) = super(msg)
	
}

object cotizacionDelDolar {
	
	var compra = 15.40
	var venta = 15.85
	
	
	method compra(pesos) {
		compra = pesos
	}
	
	method venta(pesos) {
		venta = pesos
	}
	
	method compra() {
		return compra
	}
	
	method venta() {
		return venta
	}
	
}


// familias

class Familia {
	
	var integrantes = #{}
	
	method agregarIntegrante(integrante) {
		integrantes.add(integrante)
	}
	
	method quitarIntegrante(integrante) {
		integrantes.remove(integrante)
	}
	
	method integrantesCapos() {
		integrantes.filter({
			integrante => integrante.esCapo()
		})
	}
	
	method honorPerCapita() {
		return self.honorDeIntegrantes() / integrantes.size()
	}
	
	method honorDeIntegrantes() {
		return integrantes.map({
			integrante => integrante.honor()
		}).sum()
	}
	
	method esEjemplar() {
		return integrantes.all({
			integrante => integrante.honor() > 60
		})
	}
	
}

// integrantes

class Integrante {
	// abstracta
	
	var honor
	
	constructor(_honor) {
		self.honor(_honor)
	}
	
	method honor(_honor) {
		honor = _honor
	}
	
	method honor()
	// abstracto
	
	method esCapo()
	// abstracto
	
}

class Criminal inherits Integrante {
	
	var armas = #{}
	
	constructor(_honor) = super(_honor)
	
	override method honor() {
		return honor + self.honorDeArmas()
	}
	
	method agregarArma(arma) {
		armas.add(arma)
	}
	
	method quitarArma(arma) {
		armas.remove(arma)
	}
	
	method honorDeArmas() {
		return armas.map({
			arma => arma.honor()
		}).sum()
	}
	
	override method esCapo() {
		return self.honor() > 100
			and self.tieneArmaHeavy()
	}
	
	method tieneArmaHeavy() {
		return armas.any({
			arma => arma.esHeavy()
		})
	}
	
}

class Respetable inherits Integrante {
	
	var cantDeTitulos = 0
	var tieneCargoPolitico
	
	constructor(_honor) = super(_honor)
	
	method agregarTitulo() {
		cantDeTitulos += 1
	}
	
	method tieneCargoPolitico(bool) {
		tieneCargoPolitico = bool
	}
	
	override method honor() {
		return honor + self.honorDeTitulos()
	}
	
	method honorDeTitulos() {
		return cantDeTitulos * 10
	}
	
	override method esCapo() {
		return self.honor() > 100
			and tieneCargoPolitico
	}
	
}

// armas

class Arma {
	// abstracta
	
	method honor()
	// abstracto
	
	method potencia()
	// abstracto
	
	method esHeavy() {
		return self.potencia() > 200
			or self.honor() > 10
	}
	
}

class Cuchillo inherits Arma {
	
	const honor
	
	constructor(_honor) {
		honor = _honor
	}
	
	override method honor() {
		return honor
	}
	
	override method potencia() {
		return 1
	} 
	
}

class Ametralladora inherits Arma {
	
	const potencia
	
	constructor(_potencia) {
		potencia = _potencia
	}
	
	override method honor() {
		return indicadores.honorDeAmetralladoras()
	}
	
	override method potencia() {
		return potencia
	}
	
}

class Bomba inherits Arma {
	
	override method honor() {
		return 0
	}
	
	override method potencia() {
		return 1000
	}
	
}

// objetos bien conocidos

object indicadores {
	
	var honorDeAmetralladoras = 5
	
	method honorDeAmetralladoras(honor) {
		honorDeAmetralladoras = honor
	}
	
	method honorDeAmetralladoras() {
		return honorDeAmetralladoras
	}
	
}

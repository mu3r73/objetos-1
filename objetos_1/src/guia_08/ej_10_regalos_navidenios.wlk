
// arboles

class Arbol {
	// abstracta
	
	var vejez
	var altura
	var varas
	var objetos = #{}
	
	constructor(_vejez, _altura, _varas) {
		vejez = _vejez
		altura = _altura
		varas = _varas
	}
	
	method capacidadTotal()
	// abstracto
	
	method capacidadDisponible() {
		return self.capacidadTotal() - self.espacioOcupado()
	}
	
	method espacioOcupado() {
		return objetos.map({
			objeto => objeto.espacioQueOcupa()
		}).sum()
	}
	
	method cantObjetosDestinadosA(alguien) {
		return self.objetosDestinadosA(alguien).size()
	}
	
	method objetosDestinadosA(alguien) {
		return objetos.filter({
			objeto => objeto.estaDestinadoA(alguien)
		})
	}
	
	method agregarAdorno(adorno) {
		if (not self.hayEspacioPara(adorno)) {
			throw new ArbolException("no hay espacio en el árbol para agregar el adorno")
		}
		objetos.add(adorno)
	}
	
	method incorporar(objeto) {
		if (not self.sePuedeIncorporar(objeto)) {
			throw new ArbolException("no hay espacio en el árbol para agregar el objeto")
		}
		objetos.add(objeto)
	}
	
	method sePuedeIncorporar(objeto) {
		return self.hayEspacioPara(objeto)
			and self.cantObjetosDestinadosA(objeto.destinatario()) >= 2
	}
	
	method hayEspacioPara(objeto) {
		return self.capacidadDisponible() >= objeto.espacioQueOcupa()
	}
	
	method costoTotal() {
		return objetos.map({
			objeto => objeto.precio()
		}).sum()
	}
	
}

class Natural inherits Arbol {
	
	constructor(_vejez, _altura, _varas) = super(_vejez, _altura, _varas)
	
	override method capacidadTotal() {
		return vejez * altura
	}
	
}

class Frondoso inherits Natural {
	
	constructor(_vejez, _altura, _varas) = super(_vejez, _altura, _varas)
	
	override method capacidadTotal() {
		return super() + varas / 2
	}
	
}

class Artificial inherits Arbol {
	
	constructor(_vejez, _altura, _varas) = super(_vejez, _altura, _varas)
	
	override method capacidadTotal() {
		return varas + altura
	}
	
}

// objetos

class Objeto {
	// abstracta
	
	method precio()
	// abstracto
	
	method espacioQueOcupa()
	// abstracto
	
	method estaDestinadoA(alguien)
	// abstracto
	
}

class Regalo inherits Objeto {
	
	var precio
	var destinatario
	
	constructor(_precio, _destinatario) {
		precio = _precio
		destinatario = _destinatario
	}
	
	override method precio() {
		return precio
	}
	
	method destinatario() {
		return destinatario
	}
	
	override method espacioQueOcupa() {
		return 1
	}
	
	override method estaDestinadoA(alguien) {
		return alguien == destinatario
	}
	
}

class Tarjeta inherits Objeto {
	
	var destinatarios
	
	constructor(_destinatarios) {
		destinatarios = _destinatarios
	}
	
	method destinatarios() {
		return destinatarios
	}
	
	override method precio() {
		return 2
	}
	
	override method espacioQueOcupa() {
		return 0
	}
	
	override method estaDestinadoA(alguien) {
		return destinatarios.contains(alguien)
	}
	
}

class Adorno inherits Objeto {
	
	var peso
	var superioridad
	
	constructor(_peso) {
		peso = _peso
	}
	
	method superioridad(_superioridad) {
		superioridad = _superioridad
	}
	
	override method precio() {
		return peso * superioridad
	}
	
	override method espacioQueOcupa() {
		return 1
	}
	
	override method estaDestinadoA(alguien) {
		return false
	}
	
}

class TiraDeLuces inherits Adorno {
	
	var lamparitas
	
	constructor(_peso, _lamparitas) = super(_peso) {
		lamparitas = _lamparitas
	}
	
	method luminosidad() {
		return lamparitas
	}
	
	method superioridad() {
		return self.luminosidad() / 10
	}
	
}

class FigurasElaboradas inherits Adorno {
	
	constructor(_peso) = super(_peso)
	
	method superioridad() {
		return indicadores.superioridadDeFigurasElaboradas()
	}
	
}

// objetos bien conocidos

object indicadores {
	
	var superioridadDeFigurasElaboradas = 5
	
	method superioridadDeFigurasElaboradas(coeficiente) {
		superioridadDeFigurasElaboradas = coeficiente
	}
	
	method superioridadDeFigurasElaboradas() {
		return superioridadDeFigurasElaboradas
	}
	
}

// excepciones

class ArbolException inherits Exception {
	
	constructor(_message) = super(_message)
	
}

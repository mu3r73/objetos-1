
import parcial_16_11_24.*

// advertencia: los tests no cubren ni remotamente todas las posibilidades

test "1.a) créditos que otorga una materia, régimen semanal" {
	
	const mate = new MateriaSemanal("Matemática 1", 6)
	
	assert.equals(6 * 2, mate.creditos())
	
}

test "1.b) créditos que otorga una materia, seminario" {
	
	const sem = new Seminario("Seminario 1")
	
	sem.agregarActividad(new Actividad("actividad 1", 1))
	sem.agregarActividad(new Actividad("actividad 2", 2))
	sem.agregarActividad(new Actividad("actividad 3", 3))
	
	assert.equals(1 + 2 + 3, sem.creditos())
	
}

test "1.c) créditos que otorga una materia, trabajo final" {
	
	const tf = new TrabajoFinal("Trabajo Final")
	
	assert.equals(10, tf.creditos())
	
}

test "2.a) alumno normal puede inscribirse en materia" {
	
	var pepe = new Alumno()
	
	const mate = new MateriaSemanal("Matemática 2", 8)
	
	assert.that(pepe.puedeInscribirseEn(mate))
	
	mate.agregarPrevia(new MateriaSemanal("Matemática 1", 6))
	
	assert.notThat(pepe.puedeInscribirseEn(mate))
	
}

test "2.b) alumno tutelado puede inscribirse en materia" {
	
	var pepe = new AlumnoTutelado()
	
	const mate = new MateriaSemanal("Matemática 2", 8)
	
	assert.notThat(pepe.puedeInscribirseEn(mate))
	
	pepe.agregarMateriaRecomendada(mate)
	
	assert.that(pepe.puedeInscribirseEn(mate))
	
	mate.agregarPrevia(new MateriaSemanal("Matemática 1", 6))
	
	assert.notThat(pepe.puedeInscribirseEn(mate))
	
}

test "2.c) alumno de intercambio puede inscribirse en materia" {
	
	var pepe = new AlumnoDeIntercambio()
	
	const mate = new MateriaSemanal("Matemática 2", 8)
	
	assert.notThat(pepe.puedeInscribirseEn(mate))
	
	pepe.agregarMateriaAConvenio(mate)
	
	assert.that(pepe.puedeInscribirseEn(mate))
	
	mate.agregarPrevia(new MateriaSemanal("Matemática 1", 6))
	
	assert.that(pepe.puedeInscribirseEn(mate))
	
}

test "3. alumno: aprobar materia + materia está aprobada" {
	
	var pepe = new Alumno()
	
	const mate = new MateriaSemanal("Matemática 2", 8)
	
	assert.notThat(pepe.tieneAprobada(mate))
	
	pepe.aprobar(mate)
	
	assert.that(pepe.tieneAprobada(mate))
	
}

test "4. alumno: ¿intención de inscripcion es válida?" {
	
	const mate1 = new MateriaSemanal("Intro", 8) 
	const mate2 = new MateriaSemanal("Matemática 1", 6)
	const mate3 = new MateriaSemanal("Inglés", 5)
	const mate4 = new MateriaSemanal("Matemática 2", 8)
	mate4.agregarPrevia(mate1)
	
	var intencion = new IntencionDeInscripcion()
	intencion.agregarMateria(mate1)
	intencion.agregarMateria(mate2)
	intencion.agregarMateria(mate3)
	
	var pepe = new Alumno()
	
	assert.that(pepe.esValida(intencion))
	
	intencion.agregarMateria(mate4)
	
	assert.notThat(pepe.esValida(intencion))
	
}

test "5. alumno: efectivizar intención de inscripcion" {
	
	const mate1 = new MateriaSemanal("Intro", 8) 
	const mate2 = new MateriaSemanal("Matemática 1", 6)
	const mate3 = new MateriaSemanal("Inglés", 5)
	const mate4 = new MateriaSemanal("Matemática 2", 8)
	mate4.agregarPrevia(mate1)
	
	var intencion = new IntencionDeInscripcion()
	intencion.agregarMateria(mate1)
	intencion.agregarMateria(mate2)
	intencion.agregarMateria(mate3)
	
	var pepe = new Alumno()
	
	// este caso debería funcionar ok
	pepe.hacerEfectiva(intencion)
	
	// agregando una materia que requiere una previa no aprobada, debería producirse un error
	intencion.agregarMateria(mate4)
	
	assert.throwsExceptionWithType(new InscripcionException(""), {
		pepe.hacerEfectiva(intencion)
	})
	
}

test "6. alumno: inscripciones" {
	
	var pepe = new Alumno()
	
	const mate1 = new MateriaSemanal("Intro", 8) 
	const mate2 = new MateriaSemanal("Matemática 1", 6)
	const mate3 = new MateriaSemanal("Inglés", 5)
	
	var intencion = new IntencionDeInscripcion()
	intencion.agregarMateria(mate1)
	intencion.agregarMateria(mate2)
	intencion.agregarMateria(mate3)
	
	pepe.hacerEfectiva(intencion)
	
	assert.equals(#{intencion}, pepe.inscripciones())
	
}

test "7. alumno: cantidad de veces que se inscribio en una materia" {
	
	var pepe = new Alumno()
	
	const mate1 = new MateriaSemanal("Intro", 8) 
	const mate2 = new MateriaSemanal("Matemática 1", 6)
	const mate3 = new MateriaSemanal("Inglés", 5)
	
	var intencion = new IntencionDeInscripcion()
	intencion.agregarMateria(mate1)
	intencion.agregarMateria(mate2)
	intencion.agregarMateria(mate3)
	
	pepe.hacerEfectiva(intencion)
	
	assert.equals(1, pepe.cuantasVecesSeInscribioEn(mate1))
	
}

test "8. alumno: materia aprobada más jodida" {
	
	var pepe = new Alumno()
	
	const mate1 = new MateriaSemanal("Intro", 8) 
	const mate2 = new MateriaSemanal("Matemática 1", 6)
	const mate3 = new MateriaSemanal("Inglés", 5)
	
	var intencion1 = new IntencionDeInscripcion()
	intencion1.agregarMateria(mate1)
	intencion1.agregarMateria(mate2)
	
	var intencion2 = new IntencionDeInscripcion()
	intencion2.agregarMateria(mate1)
	intencion2.agregarMateria(mate3)
	
	pepe.hacerEfectiva(intencion1)
	pepe.hacerEfectiva(intencion2)
	
	pepe.aprobar(mate1)
	pepe.aprobar(mate2)
	pepe.aprobar(mate3)
	
	assert.equals(mate1, pepe.materiaAprobadaMasJodida())
	
}

test "9. alumno: materias aprobadas simples" {
	
	var pepe = new Alumno()
	
	const mate1 = new MateriaSemanal("Intro", 8) 
	const mate2 = new MateriaSemanal("Matemática 1", 6)
	const mate3 = new MateriaSemanal("Inglés", 5)
	
	var intencion1 = new IntencionDeInscripcion()
	intencion1.agregarMateria(mate1)
	intencion1.agregarMateria(mate2)
	
	var intencion2 = new IntencionDeInscripcion()
	intencion2.agregarMateria(mate1)
	intencion2.agregarMateria(mate3)
	
	pepe.hacerEfectiva(intencion1)
	pepe.hacerEfectiva(intencion2)
	
	pepe.aprobar(mate1)
	pepe.aprobar(mate2)
	pepe.aprobar(mate3)
	
	assert.equals(#{mate2, mate3}, pepe.materiasAprobadasSimples())
	
}

test "10. materia es de interés para conjunto de alumnos" {
	
	const mate = new MateriaSemanal("Matemática 1", 6)
	
	var pepe = new Alumno()
	var tete = new Alumno()
	var toto = new Alumno()
	
	assert.that(mate.esDeInteresPara(#{pepe, tete, toto}))
	
}

test "11. alumno: tiene talentos complementarios con otro alumno" {
	
	var pepe = new Alumno()
	var toto = new Alumno()
	
	const mate1 = new MateriaSemanal("Intro", 8) 
	const mate2 = new MateriaSemanal("Matemática 1", 6)
	const mate3 = new MateriaSemanal("Inglés", 5)
	
	var intencion1 = new IntencionDeInscripcion()
	intencion1.agregarMateria(mate1)
	intencion1.agregarMateria(mate2)
	
	var intencion2 = new IntencionDeInscripcion()
	intencion2.agregarMateria(mate1)
	intencion2.agregarMateria(mate3)
	
	var intencion2b = new IntencionDeInscripcion()
	intencion2b.agregarMateria(mate1)
	intencion2b.agregarMateria(mate3)
	
	var intencion3 = new IntencionDeInscripcion()
	intencion3.agregarMateria(mate2)
	intencion3.agregarMateria(mate3)
	
	pepe.hacerEfectiva(intencion1)
	pepe.hacerEfectiva(intencion2)
	pepe.aprobar(mate1)
	pepe.aprobar(mate2)
	pepe.aprobar(mate3)
	// simples: 2 y 3; jodida: 1
	
	toto.hacerEfectiva(intencion2b)
	toto.hacerEfectiva(intencion3)
	toto.aprobar(mate1)
	toto.aprobar(mate2)
	toto.aprobar(mate3)
	// simples: 1 y 2, jodida: 3
	
	assert.that(pepe.tieneTalentosComplementariosCon(toto))
	
}

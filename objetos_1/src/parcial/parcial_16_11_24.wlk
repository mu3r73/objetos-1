
// alumnos

class Alumno {
	
	var materiasAprobadas = #{}
	var inscripciones = #{}
	
	method esValida(intencionDeInscripcion) {
		return not intencionDeInscripcion.sumaTotalDeCreditosSuperaElMaximo()
			and self.puedeEscribirseEnTodasLasMateriasDe(intencionDeInscripcion)
	}
	
	method puedeEscribirseEnTodasLasMateriasDe(intencionDeInscripcion) {
		return intencionDeInscripcion.materias().all({
			materia => self.puedeInscribirseEn(materia)
		})
	}
	
	method puedeInscribirseEn(materia) {
		return not self.tieneAprobada(materia)
			and self.tieneAprobadasLasPrevias(materia)
	}
	
	method tieneAprobada(materia) {
		return materiasAprobadas.contains(materia)
	}
	
	method tieneAprobadasLasPrevias(materia) {
		return materia.previas().all({
			previa => self.tieneAprobada(previa)
		})
	}
	
	method hacerEfectiva(intencionDeInscripcion) {
		if (not self.esValida(intencionDeInscripcion)) {
			throw new InscripcionException("No se puede efectivizar la inscripción")
		}
		inscripciones.add(intencionDeInscripcion)
	}
	
	method aprobar(materia) {
		materiasAprobadas.add(materia)
	}
	
	method inscripciones() {
		return inscripciones
	}
	
	method cuantasVecesSeInscribioEn(materia) {
		return inscripciones.count({
			inscripcion => inscripcion.incluyeMateria(materia)
		})
	}
	
	method materiaAprobadaMasJodida() {
		return materiasAprobadas.max({
			materia => self.cuantasVecesSeInscribioEn(materia)
		})
	}
	
	method materiasAprobadasSimples() {
		return materiasAprobadas.filter({
			materia => self.leResultoSimple(materia)
		})
	}
	
	method leResultoSimple(materia) {
		return self.cuantasVecesSeInscribioEn(materia) == 1
	}
	
	method tieneTalentosComplementariosCon(alumno) {
		return alumno.leResultoSimple(self.materiaAprobadaMasJodida())
			or self.leResultoSimple(alumno.materiaAprobadaMasJodida())
	}
	
}

class AlumnoTutelado inherits Alumno {
	
	var materiasRecomendadas = #{}
	
	method agregarMateriaRecomendada(materia) {
		materiasRecomendadas.add(materia)
	}
	
	override method puedeInscribirseEn(materia) {
		return super(materia)
			and self.esRecomendada(materia)
	}
	
	method esRecomendada(materia) {
		return materiasRecomendadas.contains(materia)
	}
	
}

class AlumnoDeIntercambio inherits Alumno {
	
	var materiasEnConvenio = #{}
	
	method agregarMateriaAConvenio(materia) {
		materiasEnConvenio.add(materia)
	}
	
	override method puedeInscribirseEn(materia) {
		return self.estaEnConvenio(materia)
	}
	
	method estaEnConvenio(materia) {
		return materiasEnConvenio.contains(materia)
	}
	
}

// intenciones de inscripción

class IntencionDeInscripcion {
	
	var materias = #{}
	
	method agregarMateria(materia) {
		materias.add(materia)
	}
	
	method materias() {
		return materias
	}
	
	method sumaTotalDeCreditosSuperaElMaximo() {
		return self.sumaTotalDeCreditos() > indicadores.creditosMaximos()
	}
	
	method sumaTotalDeCreditos() {
		return materias.sum({
			materia => materia.creditos()
		})
	}
	
	method incluyeMateria(materia) {
		return materias.contains(materia)
	}
	
}

// materias

class Materia {
	// abstracta
	
	const nombre
	var previas = #{}	// materias previas requeridas para poder cursar ésta
	
	constructor(_nombre) {
		nombre = _nombre
	}
	
	method nombre() {
		return nombre
	}
	
	method agregarPrevia(materia) {
		previas.add(materia)
	}
	
	method previas() {
		return previas
	}
	
	method creditos()
	// abstracto
	
	method esDeInteresPara(alumnos) {
		return alumnos.all({
			alumno => alumno.puedeInscribirseEn(self)
		})
	}
	
}

class MateriaSemanal inherits Materia {
	
	const cantHoras
	
	constructor(_nombre, _cantHoras) = super(_nombre) {
		cantHoras = _cantHoras
	}
	
	override method creditos() {
		return cantHoras * 2
	}
	
}

class Seminario inherits Materia {
	
	var actividades = #{}
	
	constructor(_nombre) = super(_nombre)
	
	method agregarActividad(actividad) {
		actividades.add(actividad)
	}
	
	override method creditos() {
		return actividades.sum({
			actividad => actividad.creditos()
		})
	}
	
}

class TrabajoFinal inherits Materia {
	
	constructor(_nombre) = super(_nombre)
	
	override method creditos() {
		return 10
	}
	
}

// actividades

class Actividad {
	
	const nombre
	const creditos
	
	constructor(_nombre, _creditos) {
		nombre = _nombre
		creditos = _creditos
	}
	
	method creditos() {
		return creditos
	}
	
}

// excepciones

class InscripcionException inherits Exception {
	
	constructor(mensaje) = super(mensaje)
	
}

// objetos bien conocidos

object indicadores {
	
	var creditosMaximos = 100 	// valor predeterminado
	
	method creditosMaximos(numero) {
		creditosMaximos = numero
	}
	
	method creditosMaximos() {
		return creditosMaximos
	}
	
}

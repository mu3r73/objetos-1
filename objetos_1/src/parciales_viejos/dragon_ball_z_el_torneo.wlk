
// torneos

class Torneo {
	// abstracta
	
	var inscriptos = #{}
	
	method agregarInscripto(guerrero) {
		inscriptos.add(guerrero)
	}
	
	method seleccionarParticipantes()
	// abstracto
	
}

class PowerIsBest inherits Torneo {
	
	override method seleccionarParticipantes() {
		return self.inscriptosOrdenadosPorPotencia().take(16)
	}
	
	method inscriptosOrdenadosPorPotencia() {
		return inscriptos.asList().sortBy({
			i1, i2 => i1.potOfensivo() > i2.potOfensivo()
		})
	}
	
}

class Funny inherits Torneo {
	
	override method seleccionarParticipantes() {
		return self.inscriptosOrdenadosPorCantPiezasEnTraje().take(16)
	}
	
	method inscriptosOrdenadosPorCantPiezasEnTraje() {
		return inscriptos.asList().sortBy({
			i1, i2 => i1.traje().cantPiezas() > i2.traje().cantPiezas()
		})
	}
	
}

class Surprise inherits Torneo {
	
	override method seleccionarParticipantes() {
		return self.inscriptosOrdenadosAlAzar().take(16)
	}
	
	method inscriptosOrdenadosAlAzar() {
		return inscriptos.asList().sortBy({
			i1, i2 => 0.randomUpTo(100) < 50
		})
	}
	
}

// guerreros

class Guerrero {
	
	var potOfensivo
	var experiencia
	var energia = 100	// porcentaje
	var traje
	
	constructor(_potOfensivo, _experiencia) {
		potOfensivo = _potOfensivo
		experiencia = _experiencia
	}
	
	method potOfensivo() {
		return potOfensivo
	}
	
	method traje(_traje) {
		traje = _traje
	}
	
	method traje() {
		return traje
	}
	
	method estaMuerto() {
		return energia == 0
	}
	
	method atacar(oponente) {
		oponente.recibirAtaqueCon(self.potOfensivo())
	}
	
	method recibirAtaqueCon(potOfensAtacante) {
		traje.absorberAtaque()
		self.recibirGolpe(self.danioReducido(potOfensAtacante * 0.1))
		experiencia = traje.experienciaModificada(experiencia + 1)
	}
	
	method danioReducido(danio) {
		return danio - self.danioResistido(danio)
	}
	
	method danioResistido(danio) {
		return danio
	}
	
	method recibirGolpe(danio) {
		energia -= traje.danioModificado(danio)
	}
	
	method comerSemillaErmitanio() {
		energia = 100
	}
	
	method puedeConvertirse() {
		return false
	}
	
}

class Saiyan inherits Guerrero {
	
	var potOfensivoOriginal
	var nivel = 0
	
	constructor(_potOfensivo, _experiencia) = super(_potOfensivo, _experiencia) {
		potOfensivoOriginal = _potOfensivo
	}
	
	override method puedeConvertirse() {
		return true
	}
	
	method convertirse() {
		nivel = (nivel + 1).min(3)
	}
	
	override method potOfensivo() {
		if (self.estaConvertido()) {
			return potOfensivo * 1.5
		} else {
			return potOfensivo
		}
	}
	
	method estaConvertido() {
		return nivel > 0
	}
	
	override method danioResistido(danio) {
		if (nivel == 0) {
			return super(danio)
		} else if (nivel == 1) {
			return danio * 0.05
		} else if (nivel == 2) {
			return danio * 0.07
		} else {	// (nivel == 3)
			return danio * 0.15
		}
	}
	
	override method comerSemillaErmitanio() {
		super()
		potOfensivo += potOfensivoOriginal * 0.05
	}
	
	override method recibirAtaqueCon(potOfensAtacante) {
		super(potOfensAtacante)
		if (energia < 0.01) {
			self.volverAEstadoOriginal()
		}
	}
	
	method volverAEstadoOriginal() {
		nivel = 0
	}
	
}

// trajes

class Traje {
	// abstracta
	
	var desgaste = 0
	
	method danioModificado(danio)
	// abstracto
	
	method experienciaModificada(experiencia)
	// abstracto
	
	method absorberAtaque() {
		desgaste += 5
	}
	
	method estaGastado() {
		return desgaste == 100
	}
	
	method cantPiezas() {
		return 1
	}
	
}

class TrajeComun inherits Traje {
	
	const absorcion	// porcentaje, entre 0 y 100
	
	constructor(_absorcion) {
		absorcion = _absorcion
	}
	
	override method danioModificado(danio) {
		if (self.estaGastado()) {
			return danio
		} else {
			return danio * absorcion / 100
		}
	}
	
	override method experienciaModificada(experiencia) {
		return experiencia
	}
	
}

class TrajeDeEntrenamiento inherits Traje {
	
	override method danioModificado(danio) {
		return danio
	}
	
	override method experienciaModificada(experiencia) {
		if (self.estaGastado()) {
			return experiencia
		} else {
			return experiencia * indicadores.multExperienciaTrajesDeEntrenamiento()
		}
	}
	
}

class TrajeModularizado inherits Traje {
	
	var piezas = #{}
	
	method agregarPieza(pieza) {
		piezas.add(pieza)
	}
	
	method quitarPieza(pieza) {
		piezas.remove(pieza)
	}
	
	override method danioModificado(danio) {
		return self.piezasNoGastadas().map({
			pieza => pieza.resistencia()
		}).sum()
	}
	
	override method experienciaModificada(experiencia) {
		return experiencia * (1 + self.porcentajeDePiezasNoGastadas())
	}
	
	override method estaGastado() {
		return self.piezasNoGastadas().isEmpty()
	}
	
	method piezasNoGastadas() {
		return piezas.filter({
			pieza => not pieza.estaGastada()
		})
	}
	
	method cantPiezasNoGastadas() {
		return self.piezasNoGastadas().size()
	}
	
	method porcentajeDePiezasNoGastadas() {
		return self.cantPiezasNoGastadas() / self.cantPiezas()
	}
	
	override method absorberAtaque() {
		piezas.forEach({
			pieza => pieza.absorberAtaque(5 / self.cantPiezas())
		})
	}
	
	override method cantPiezas() {
		return piezas.size()
	}
	
}

class Pieza {
	
	var resistencia	// porcentaje, entre 0 y 100
	var desgaste = 0
	
	constructor(_resistencia) {
		resistencia = _resistencia
	}
	
	method resistencia() {
		return resistencia
	}
	
	method estaGastada() {
		return desgaste >= 20
	}
	
	method absorberAtaque(_desgaste) {
		desgaste += _desgaste
	}
	
}

// objetos bien conocidos

object indicadores {
	
	var multExperienciaTrajesDeEntrenamiento = 2
	
	method multExperienciaTrajesDeEntrenamiento(numero) {
		multExperienciaTrajesDeEntrenamiento = numero
	}
	
	method multExperienciaTrajesDeEntrenamiento() {
		return multExperienciaTrajesDeEntrenamiento
	}
	
}

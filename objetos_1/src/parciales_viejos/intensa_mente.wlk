
object riley {
	
	var felicidad = 1000
	var edad = 11
	var emDominante = alegria
	var recuerdos = []
	var memoria = #{}
	var pensamientoActual
	
	method emDominante(_emDominante) {
		emDominante = _emDominante
	}
	
	method felicidad(nivel) {
		felicidad = nivel
		if (felicidad < 1) {
			error.throwWithMessage("la felicidad no puede ser inferior a 1")
		}
	}
	
	method felicidad() {
		return felicidad
	}
	
	method vivirEvento(descripcion) {
		recuerdos.add(emDominante.crearRecuerdo(self, descripcion, new Date()))
	}
	
	method asentarRecuerdo(recuerdo) {
		if (not recuerdo.estaAsentado()) {
			recuerdo.asentar()
		}
	}
	
	method recuerdosRecientes() {
		return self.recuerdosDelDia().drop(recuerdos.size() - 5)
	}
	
	method recuerdosDelDia() {
		return recuerdos.filter({
			recuerdo => recuerdo.esDelDia()
		})
	}
	
	method pensamientosCentrales() {
		return recuerdos.filter({
			recuerdo => recuerdo.esCentral()
		}).asSet()
	}
	
	method pensamientosCentralesDificilesDeExplicar() {
		return self.pensamientosCentrales().filter({
			recuerdo => recuerdo.esDificilDeExplicar() 
		})
	}
	
	method asentamiento() {
		recuerdos.forEach({
			recuerdo => self.asentarRecuerdo(recuerdo)
		})
	}
	
	method asentamientoSelectivoCon(palabra) {
		self.recuerdosConPalabra(palabra).forEach({
			recuerdo => self.asentarRecuerdo(recuerdo)
		})
	}
	
	method recuerdosConPalabra(palabra) {
		return recuerdos.filter({
			recuerdo => recuerdo.descripcionIncluye(palabra)
		})
	}
	
	method profundizacion() {
		self.recuerdosNoNegadosYNoCentralesDelDia().forEach({
			recuerdo => memoria.add(recuerdo)
		})
	}
	
	method recuerdosNoNegadosYNoCentralesDelDia() {
		return self.recuerdosNoCentralesDelDia().filter({
			recuerdo => not self.niegaA(recuerdo)
		})
	}
	
	method recuerdosNoCentralesDelDia() {
		return self.recuerdosDelDia().filter({
			recuerdo => not recuerdo.esCentral()
		})
	}
	
	method controlHormonal() {
		if (self.hayDesequilibrioHormonal()) {
			self.felicidad(felicidad * 0.85)
			self.perder3PensamientosCentralesMasAntiguos()
		}
	}
	
	method hayDesequilibrioHormonal() {
		return self.algunPensamientoCentralEstaTambienEnMemoria()
			or self.todosLosRecuerdosDelDiaTienenLaMismaEmocionDominante()
	}
	
	method algunPensamientoCentralEstaTambienEnMemoria() {
		return self.recuerdosCentrales().any({
			recuerdo => memoria.contains(recuerdo)
		})
	}
	
	method recuerdosCentrales() {
		return recuerdos.filter({
			recuerdo => recuerdo.esCentral()
		})
	}
	
	method todosLosRecuerdosDelDiaTienenLaMismaEmocionDominante() {
		return self.emocionesPosibles().any({
			emocion => self.todosLosRecuerdosDelDiaTienenLaEmocion(emocion)
		})
	}
	
	method emocionesPosibles() {
		return #{alegria, tristeza, disgusto, furia, temor}
	}
	
	method todosLosRecuerdosDelDiaTienenLaEmocion(emocion) {
		return self.recuerdosDelDia().all({
			recuerdo => recuerdo.emocion() == emocion
		})
	}
	
	method perder3PensamientosCentralesMasAntiguos() {
		self.los3PensamientosCentralesMasAntiguos().forEach({
			recuerdo => recuerdos.remove(recuerdo)
		})
	}
	
	method los3PensamientosCentralesMasAntiguos() {
		return self.recuerdosCentralesOrdenadosPorFecha().take(3)
	}
	
	method recuerdosCentralesOrdenadosPorFecha() {
		return self.recuerdosCentrales().sortBy({
			rec1, rec2 => rec1.fecha() < rec2.fecha()
		})
	}
	
	method restauracionCognitiva() {
		self.felicidad((felicidad + 100).min(1000))
	}
	
	method liberarRecuerdosDelDia() {
		self.recuerdosDelDia().forEach({
			recuerdo => recuerdo.liberar()
		})
	}
	
	method niegaA(recuerdo) {
		return emDominante.niegaA(recuerdo)
	}
	
	method dormir() {
		self.asentamiento()
		self.asentamientoSelectivoCon("pesadilla")
		self.profundizacion()
		self.controlHormonal()
		self.restauracionCognitiva()
		self.liberarRecuerdosDelDia()
	}
	
	method rememorar() {
		pensamientoActual = self.recuerdosMasAntiguosQueMitadDeEdad().anyOne()
	}
	
	method recuerdosMasAntiguosQueMitadDeEdad() {
		return memoria.filter({
			recuerdo => recuerdo.edad() > edad / 2
		})
	}
	
	method repeticionesEnMemoriaDe(recuerdo) {
		return memoria.ocurrencesOf(recuerdo)
	}
	
	method estaTeniendoUnDejaVu() {
		return self.recuerdosRepetidosEnLaMemoria().contains(pensamientoActual)
	}
	
	method recuerdosRepetidosEnLaMemoria() {
		return memoria.filter({
			recuerdo => self.repeticionesEnMemoriaDe(recuerdo) > 1
		})
	}
	
}

// recuerdos

class Recuerdo {
	
	const descripcion
	const emocion
	const fecha
	var delDia = true
	var asentado = false
	var central = false
	
	constructor(_descripcion, _emocion, _fecha) {
		descripcion = _descripcion
		emocion = _emocion
		fecha = _fecha
	}
	
	method emocion() {
		return emocion
	}
	
	method fecha() {
		return fecha
	}
	
	method estaAsentado() {
		return asentado
	}
	
	method asentar() {
		asentado = true
		emocion.asentar(self)
	}
	
	method esDelDia() {
		return delDia
	}
	
	method centralizar() {
		central = true
	}
	
	method esCentral() {
		return central
	}
	
	method esDificilDeExplicar() {
		return descripcion.words().size() > 10
	}
	
	method descripcionIncluye(_palabra) {
		return descripcion.words().any({
			palabra => palabra == _palabra
		})
	}
	
	method liberar() {
		delDia = false
	}
	
	method edad() {
		return (new Date()).year() - fecha.year()
	}
	
}

// emociones = objetos bien conocidos

class Emocion {
	// abstracta
	
	method crearRecuerdo(descripcion, fecha) {
		return new Recuerdo(descripcion, self, fecha)
	}
	
	method esAlegre() {
		return false
	}
	
	method asentar(recuerdo) {
		// nada
	}
	
	method niegaA(recuerdo) {
		return false
	}
	
}

class EmocionCompuesta inherits Emocion {
	
	const emociones
	
	constructor(_emociones) {
		emociones = _emociones
	}
	
	override method esAlegre() {
		return emociones.any({
			emocion => emocion.esAlegre()
		})
	}
	
	override method niegaA(recuerdo) {
		return emociones.all({
			emocion => emocion.niegaA(recuerdo)
		})
	}
	
	override method asentar(recuerdo) {
		emociones.forEach({
			emocion => emocion.asentar(recuerdo)
		})
	}
	
}

object alegria inherits Emocion {
	
	override method esAlegre() {
		return true
	}
	
	override method asentar(recuerdo) {
		if (riley.felicidad() > 500) {
			recuerdo.centralizar()
		}
	}
	
	override method niegaA(recuerdo) {
		return not recuerdo.emocion().esAlegre()
	}
	
}

object tristeza inherits Emocion {
	
	override method niegaA(recuerdo) {
		return recuerdo.emocion().esAlegre()
	}
	
	override method asentar(recuerdo) {
		recuerdo.centralizar()
		riley.felicidad(riley.felicidad() * 0.9)
	}
	
}

object disgusto inherits Emocion {
	
}

object furia inherits Emocion {
	
}

object temor inherits Emocion {
	
}

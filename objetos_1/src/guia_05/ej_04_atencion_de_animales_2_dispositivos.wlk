
class ComederoNormal {
	
	const pesoRacion			// gramos
	var cantRaciones = 0
	const pesoMaximoPorAnimal	// kg
	
	
	constructor(_pesoRacion, _pesoMaximoPorAnimal) {
		pesoRacion = _pesoRacion
		pesoMaximoPorAnimal = _pesoMaximoPorAnimal
	}
	
	method puedeAtenderA(animal) {
		return animal.tieneHambre()
			and animal.peso() <= pesoMaximoPorAnimal
	}
	
	method estaVacio() {
		return cantRaciones == 0
	}
	
	method atender(animal) {
		if (self.estaVacio()) {
			error.throwWithMessage(self.className() + ": no queda alimento")
		}
		animal.comer(pesoRacion)
		cantRaciones -= 1
	}
	
	method necesitaRecarga() {
		return cantRaciones < 10
	}
	
	method recargar() {
		cantRaciones += 30
	}
	
}

class ComederoInteligente {
	
	var kgAlimento = 0		// kg
	const capacidadMaxima	// kg
	
	
	constructor(_capacidadMaxima) {
		capacidadMaxima = _capacidadMaxima
	}
	
	method puedeAtenderA(animal) {
		return animal.tieneHambre()
	}
	
	method estaVacio() {
		return kgAlimento == 0
	}
	
	method atender(animal) {
		if (self.estaVacio()) {
			error.throwWithMessage(self.className() + ": no queda alimento")
		}
		animal.comer(kgAlimento.min(1000 * animal.peso() / 100))
		kgAlimento -= kgAlimento.min(animal.peso() / 100)
	}
	
	method necesitaRecarga() {
		return kgAlimento < 15
	}
	
	method recargar() {
		kgAlimento = capacidadMaxima 
	}
	
}

class Bebedero {
	
	var animalesAtendidos = 0
	
	
	method puedeAtenderA(animal) {
		return animal.tieneSed()
	}
	
	method estaVacio() {
		return animalesAtendidos == 20
	}
	
	method atender(animal) {
		if (self.estaVacio()) {
			error.throwWithMessage(self.className() + ": no queda bebida")
		}
		animal.beber()
		animalesAtendidos += 1
	}
	
	method necesitaRecarga() {
		return self.estaVacio()
	}
	
	method recargar() {
		animalesAtendidos = 0 
	}
	
}

class Vacunatorio {
	
	var vacunasDisponibles = 0
	
	
	method puedeAtenderA(animal) {
		return animal.convieneVacunar()
	}
	
	method estaVacio() {
		return vacunasDisponibles == 0
	}
	
	method atender(animal) {
		if (self.estaVacio()) {
			error.throwWithMessage(self.className() + ": no quedan vacunas")
		}
		animal.recibirVacuna()
		vacunasDisponibles -= 1
	}
	
	method necesitaRecarga() {
		return vacunasDisponibles == 0
	}
	
	method recargar() {
		vacunasDisponibles = 50 
	}
	
}

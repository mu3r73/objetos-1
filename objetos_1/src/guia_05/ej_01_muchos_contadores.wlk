
class Contador {
	
	var valor = 0
	var ultCmd = ""
	
	
	method reset() {
		valor = 0
		ultCmd = "reset"
	}
	
	method inc() {
		valor += 1
		ultCmd = "incremento"
	}
	
	method dec() {
		valor -= 1
		ultCmd = "decremento"
	}
	
	method valorActual() {
		return valor
	}
	
	method ultimoComando() {
		return ultCmd
	}

}

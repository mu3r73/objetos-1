
import ej_06_infraestructura_ferroviaria_1_unidades.*

class Formacion {
	
	var locomotoras = #{}
	var vagones = #{}
	
	
	method agregarLocomotora(locomotora) {
		locomotoras.add(locomotora)
	}
	
	method quitarLocomotora(locomotora) {
		locomotora.remove(locomotora)
	}
	
	method agregarVagon(vagon) {
		vagones.add(vagon)
	}
	
	method quitarVagon(vagon) {
		vagones.remove(vagon)
	}
	
	method totalPasajerosQuePuedeTransportar() {
		return vagones.map({
			vagon => vagon.cantPasajerosQuePuedeTransportar()
		}).sum()
	}
	
	method cantVagonesLivianos() {
		return vagones.filter({
			vagon => vagon.pesoMaximo() < 2500
		}).size()
	}
	
	method vagonQuePuedeTransportarMasPasajeros() {
		return vagones.max({
			vagon => vagon.cantPasajerosQuePuedeTransportar()
		})
	}
	
	method esCarguera() {
		return vagones.all({
			vagon => vagon.cargaMaxima() >= 1500
		})
	}
	
	method esParejita() {
		return (
			vagones.map({
				vagon => vagon.pesoMaximo()
			}).max()
			- vagones.map({
				vagon => vagon.pesoMaximo()
			}).min()
		) <= 200 
	}
	
	method velocidadMaxima() {
		return locomotoras.map({
			locomotora => locomotora.velocidadMaxima()
		}).min()
	}
	
	method esEficiente() {
		return locomotoras.all({
			locomotora => locomotora.pesoMaximoQuePuedeArrastrar() >= locomotora.peso() * 5
		})
	}
	
	method arrastreTotalDeLocomotoras() {
		return locomotoras.map({
			locomotora => locomotora.pesoMaximoQuePuedeArrastrar()
		}).sum()
	}
	
	method pesoMaximoTotal() {
		return (locomotoras.map({
			locomotora => locomotora.peso()
		}) + vagones.map({
			vagon => vagon.pesoMaximo()
		})).sum()
	}
	
	method puedeMover() {
		return self.arrastreTotalDeLocomotoras() >= self.pesoMaximoTotal()
	}
	
	method cuantoEmpujeFaltaParaMover() {
		if (self.puedeMover()) {
			return 0
		} else {
			return self.pesoMaximoTotal() - self.arrastreTotalDeLocomotoras()
		}
	}
	
	method vagonMasPesado() {
		return vagones.max({
			vagon => vagon.pesoMaximo()
		})
	}
	
	method cantUnidades() {
		return locomotoras.size() + vagones.size()
	}
	
	method esCompleja() {
		return self.cantUnidades() > 20
			or self.pesoMaximoTotal() > 10000
	}
	
}

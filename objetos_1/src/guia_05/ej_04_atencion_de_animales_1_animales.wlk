
class Vaca {
	
	var peso 		// en kg
	var tieneSed = false
	var fueVacunada = false
	
	
	constructor(_peso) {
		peso = _peso
	}
	
	method peso() {
		return peso
	}
	
	method comer(gramos) {
		peso += gramos / (3 * 1000)	// gana gramos / 3
		tieneSed = true
	}
	
	method beber() {
		tieneSed = false
		peso -= 500 / 1000	// pierde 500 gramos
	}
	
	method tieneHambre() {
		return peso < 200
	}
	
	method tieneSed() {
		return tieneSed
	}
	
	method recibirVacuna() {
		fueVacunada = true
	}
	
	method convieneVacunar() {
		return not fueVacunada
	}
	
	method caminar() {
		peso -= 3
	}
	
}

class Cerdo {
	
	var peso		// en kg
	var tieneHambre = false
	var tieneSed = false
	var maxComido = 0
	var vecesQueComioSinBeber = 0
	const convieneVacunar = true
	
	
	constructor(_peso) {
		peso = _peso
	}
	
	method peso() {
		return peso
	}
	
	method comer(gramos) {
		if (gramos > 200) {
			peso += (gramos - 200) / 1000
		}
		
		if (gramos > 1000) {
			tieneHambre = false
		}
		
		maxComido = maxComido.max(gramos)
		
		vecesQueComioSinBeber += 1
		if (vecesQueComioSinBeber > 3) {
			tieneSed = true
		}
	}
	
	method beber() {
		tieneSed = false
		tieneHambre = true
		peso -= 1
		vecesQueComioSinBeber = 0
	}
	
	method tieneHambre() {
		return tieneHambre
	}
	
	method tieneSed() {
		return tieneSed
	}
	
	method convieneVacunar() {
		return convieneVacunar
	}
	
	method recibirVacuna() {
		
	}
	
	method maxComido() {
		return maxComido
	}
	
	method vecesQueComioSinBeber() {
		return vecesQueComioSinBeber
	}
	
}

class Gallina {
	
	const peso = 4
	const tieneHambre = true
	const tieneSed = false
	const convieneVacunar = false
	var vecesQueComio = 0
	
	
	method peso() {
		return peso
	}
	
	method comer(gramos) {
		vecesQueComio += 1
	}
	
	method tieneHambre() {
		return tieneHambre
	}
	
	method tieneSed() {
		return tieneSed
	}
	
	method convieneVacunar() {
		return convieneVacunar
	}
	
	method recibirVacuna() {
		
	}
	
	method vecesQueComio() {
		return vecesQueComio
	}
	
}

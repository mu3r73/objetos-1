
class Locomotora {
	
	const peso							// kg
	const pesoMaximoQuePuedeArrastrar	// kg
	const velocidadMaxima				// km/h
	
	constructor(_peso, _pesoMaximoQuePuedeArrastrar, _velocidadMaxima) {
		peso = _peso
		pesoMaximoQuePuedeArrastrar = _pesoMaximoQuePuedeArrastrar
		velocidadMaxima = _velocidadMaxima
	}
	
	method peso() {
		return peso
	}
	
	method pesoMaximoQuePuedeArrastrar() {
		return pesoMaximoQuePuedeArrastrar
	}
	
	method velocidadMaxima() {
		return velocidadMaxima
	}
	
}

class VagonDePasajeros {
	
	const largo		// en m
	const ancho		// en m
	const cargaMaxima = 100					// en kg
	const pesoVacioPorMetroDeLargo = 200	// kg
	
	
	constructor(_largo, _ancho) {
		largo = _largo
		ancho = _ancho
	}
	
	method cantPasajerosQuePuedeTransportar() {
		if (ancho <= 2.5) {
			return largo * 8
		} else {
			return largo * 10
		}
	}
	
	method cargaMaxima() {
		return cargaMaxima
	}
	
	method pesoVacio() {
		return pesoVacioPorMetroDeLargo * largo
	}
	
	method pesoMaximo() {
		return self.cantPasajerosQuePuedeTransportar() * 80 + cargaMaxima + self.pesoVacio()
	}
	
}

class VagonDeCarga {
	
	const cantPasajerosQuePuedeTransportar = 0
	const cargaMaxima			// en kg
	const peso2Guardas = 160	// en kg
	const pesoVacio = 1500		// en kg
	
	
	constructor(_cargaMaxima) {
		cargaMaxima = _cargaMaxima
	}
	
	method cantPasajerosQuePuedeTransportar() {
		return cantPasajerosQuePuedeTransportar
	}
	
	method cargaMaxima() {
		return cargaMaxima
	}
	
	method pesoMaximo() {
		return cargaMaxima + peso2Guardas + pesoVacio
	}
	
}

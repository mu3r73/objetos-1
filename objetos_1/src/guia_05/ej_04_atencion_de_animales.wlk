
import ej_04_atencion_de_animales_1_animales.*
import ej_04_atencion_de_animales_2_dispositivos.*

class EstacionDeServicio {
	
	var dispositivos = #{}
	var animalesAtendidos = #{}
	
	
	method instalarDispositivo(dispositivo) {
		dispositivos.add(dispositivo)
	}
	
	method desinstalarDispositivo(dispositivo) {
		dispositivos.remove(dispositivo)
	}
	
	method puedeAtender(animal) {
		dispositivos.any({
			dispositivo => dispositivo.puedeAtenderA(animal)
		})
	}
	
	method atender(animal) {
		animalesAtendidos.add(animal)
		dispositivos.find({
			dispositivo => dispositivo.puedeAtender(animal)
		}).atender(animal)
	}
	
	method recargarDispositivos() {
		dispositivos.filter({
			dispositivo => dispositivo.necesitaRecarga()
		}).forEach({
			dispositivo => dispositivo.recargar()
		})
	}
	
	method fueAtendido(animal) {
		return animalesAtendidos.contains(animal)
	}
	
	method atendidosQueConvieneVacunar() {
		return animalesAtendidos.filter({
			animal => animal.convieneVacunar()
		})
	}
	
	method atendidoMasPesado() {
		return animalesAtendidos.max({
			animal => animal.peso()
		})
	}
	
	method pesoTotalDeAtendidos() {
		return animalesAtendidos.map({
			animal => animal.peso()
		}).sum()
	}
	
}

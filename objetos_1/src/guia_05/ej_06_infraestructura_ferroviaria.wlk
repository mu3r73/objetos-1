
import ej_06_infraestructura_ferroviaria_1_unidades.*
import ej_06_infraestructura_ferroviaria_2_formaciones.*

class Deposito {
	
	var formaciones = #{}
	var locomotorasSueltas = #{}
	
	
	method agregarFormacion(formacion) {
		formaciones.add(formacion)
	}
	
	method quitarFormacion(formacion) {
		formaciones.remove(formacion)
	}
	
	method agregarLocomotoraSuelta(locomotora) {
		locomotorasSueltas.add(locomotora)
	}
	
	method quitarLocomotoraSuelta(locomotora) {
		locomotorasSueltas.remove(locomotora)
	}
	
	method vagonesMasPesados() {
		return formaciones.map({
			formacion => formacion.vagonMasPesado()
		})
	}
	
	method necesitaConductorExperimentado() {
		return formaciones.any({
			formacion => formacion.esCompleja()
		})
	}
	
	method locomotoraQuePuedeMover(formacion) {
		return locomotorasSueltas.find({
			locomotora => locomotora.pesoMaximoQuePuedeArrastrar() - locomotora.peso() > formacion.cuantoEmpujeFaltaParaMover()
		})
	}
	
	method hayLocomotoraQuePuedaMover(formacion) {
		return self.locomotoraQuePuedeMover(formacion) != null
	}
	
	method agregarLocomotoraParaPoderMover(formacion) {
		if (not formacion.puedeMoverse() and self.hayLocomotoraQuePuedaMover(formacion)) {
			formacion.agregarLocomotora(self.locomotoraQuePuedeMover(formacion))
		}
	}
	
}

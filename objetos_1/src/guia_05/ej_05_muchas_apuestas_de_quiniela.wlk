
class Sorteo {
	
	var numerosSorteados
	var apuestas = #{}
	
	
	method numerosSorteados(lista) {
		numerosSorteados = lista
	}
	
	method resultado(posicion) {
		if (posicion > numerosSorteados.size()) {
			error.throwWithMessage(self.className() + ": sólo posee " + numerosSorteados.size() + " números sorteados")
		}
		return numerosSorteados.get(posicion - 1)
	}
	
	method numerosSorteados() {
		return numerosSorteados
	}
	
	method agregarApuesta(apuesta) {
		apuestas.add(apuesta)
	}
	
	method apuestasGanadoras() {
		return apuestas.filter({
			apuesta => apuesta.esGanadora()
		})
	}
	
	method apuestasPerdedoras() {
		return apuestas.difference(self.apuestasGanadoras())
	}
	
	method totalRecaudado() {
		return apuestas.sum({
			apuesta => apuesta.valorApostado()
		})
	}
	
	method totalEnPremios() {
		return self.apuestasGanadoras().map({
			apuesta => apuesta.montoPremio()
		}).sum()
	}
	
	method ganadores() {
		return self.apuestasGanadoras().map({
			apuesta => apuesta.nombreJugador()
		}).asSet()
	}
	
	method beneficio() {
		return self.totalRecaudado() - self.totalEnPremios()
	}
	
}

class ApuestaAproximacionALaCabeza {
	
	const sorteo
	const numeroApostado
	const nombreJugador
	const valorApostado
	
	
	constructor(_sorteo, _numeroApostado, _nombreJugador, _valorApostado) {
		sorteo = _sorteo
		numeroApostado = _numeroApostado
		nombreJugador = _nombreJugador
		valorApostado = _valorApostado
	}
	
	method numeroApostado() {
		return numeroApostado
	}
	
	method nombreJugador() {
		return nombreJugador
	}
	
	method valorApostado() {
		return valorApostado
	}
	
	method esGanadora() {
		return sorteo.numerosSorteados().first().between(numeroApostado - 4, numeroApostado + 4)
	}
	
	method montoPremio() {
		return ((50 - (sorteo.numerosSorteados().first() - numeroApostado).abs() * 10) * valorApostado).min(0.06 * sorteo.totalRecaudado()).max(0)
	}
	
}

class ApuestaEnOrden {
	
	const sorteo
	const numerosApostados
	const nombreJugador
	const valorApostado = 500
	
	
	constructor(_sorteo, _numerosApostados, _nombreJugador) {
		sorteo = _sorteo
		numerosApostados = _numerosApostados
		nombreJugador = _nombreJugador
	}
	
	method numerosApostados() {
		return numerosApostados
	}
	
	method nombreJugador() {
		return nombreJugador
	}
	
	method valorApostado() {
		return valorApostado
	}
	
	method esGanadora() {
		return numerosApostados == sorteo.numerosSorteados()
	}
	
	method montoPremio() {
		return sorteo.totalRecaudado() * 0.4
	}
	
}

class ApuestaUnRangoALaCabeza {
	
	const sorteo
	const minRangoApostado
	const maxRangoApostado
	const nombreJugador
	const valorApostado
	
	
	constructor(_sorteo, _minRangoApostado, _maxRangoApostado, _nombreJugador, _valorApostado) {
		sorteo = _sorteo
		minRangoApostado = _minRangoApostado
		maxRangoApostado = _maxRangoApostado
		nombreJugador = _nombreJugador
		valorApostado = _valorApostado
	}
	
	method minRangoApostado() {
		return minRangoApostado
	}
	
	method maxRangoApostado() {
		return maxRangoApostado
	}
	
	method nombreJugador() {
		return nombreJugador
	}
	
	method valorApostado() {
		return valorApostado
	}
	
	method esGanadora() {
		return sorteo.numerosSorteados().first().between(minRangoApostado, maxRangoApostado)
	}
	
	method montoPremio() {
		return (valorApostado * 55 / (maxRangoApostado - minRangoApostado)).min(sorteo.totalRecaudado() * 0.1)
	}
	
}

class ApuestaACualquiera {
	
	const sorteo
	const numeroApostado
	const nombreJugador
	const valorApostado
	
	
	constructor(_sorteo, _numeroApostado, _nombreJugador, _valorApostado) {
		sorteo = _sorteo
		numeroApostado = _numeroApostado
		nombreJugador = _nombreJugador
		valorApostado = _valorApostado
	}
	
	method numeroApostado() {
		return numeroApostado
	}
	
	method nombreJugador() {
		return nombreJugador
	}
	
	method valorApostado() {
		return valorApostado
	}
	
	method esGanadora() {
		return sorteo.numerosSorteados().contains(self.numeroApostado())
	}
	
	method montoPremio() {
		return (valorApostado * 2).min(sorteo.totalRecaudado() * 0.01)
	}
	
}

class ApuestaALosPrimeros2 {
	
	const sorteo
	const numeroApostado
	const nombreJugador
	const valorApostado
	
	
	constructor(_sorteo, _numeroApostado, _nombreJugador, _valorApostado) {
		sorteo = _sorteo
		numeroApostado = _numeroApostado
		nombreJugador = _nombreJugador
		valorApostado = _valorApostado
	}
	
	method numeroApostado() {
		return numeroApostado
	}
	
	method nombreJugador() {
		return nombreJugador
	}
	
	method valorApostado() {
		return valorApostado
	}
	
	method esGanadora() {
		return numeroApostado == sorteo.numerosSorteados().first()
			or numeroApostado == sorteo.numerosSorteados().get(1)
	}
	
	method montoPremio() {
		return (valorApostado * 30).min(sorteo.totalRecaudado() * 0.05)
	}
	
}

class ApuestaALaCabeza {
	
	const sorteo
	const numeroApostado
	const nombreJugador
	const valorApostado
	
	
	constructor(_sorteo, _numeroApostado, _nombreJugador, _valorApostado) {
		sorteo = _sorteo
		numeroApostado = _numeroApostado
		nombreJugador = _nombreJugador
		valorApostado = _valorApostado
	}
	
	method numeroApostado() {
		return numeroApostado
	}
	
	method nombreJugador() {
		return nombreJugador
	}
	
	method valorApostado() {
		return valorApostado
	}
	
	method esGanadora() {
		return numeroApostado == sorteo.numerosSorteados().first()
	}
	
	method montoPremio() {
		return (valorApostado * 70).min(sorteo.totalRecaudado() * 0.1)
	}
	
}

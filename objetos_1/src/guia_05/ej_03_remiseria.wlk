
class Remiseria {
	
	var flota = #{}
	var viajes = #{}
	const valorPorKm
	const minimoPorViaje
	
	
	constructor(_valorPorKm, _minimoPorViaje) {
		valorPorKm = _valorPorKm
		minimoPorViaje = _minimoPorViaje
	}
	
	method agregarAFlota(vehiculo) {
		flota.add(vehiculo)
	}
	
	method quitarDeFlota(vehiculo) {
		flota.remove(vehiculo)
	}
	
	method registrarViaje(viaje, vehiculo) {
		viaje.vehiculo(vehiculo)
		viajes.add(viaje)
	}
	
	method pesoTotal() {
		return flota.map({
			vehiculo => vehiculo.peso()
		}).sum()
	}
	
	method esRecomendable() {
		return flota.size() >= 3
			and flota.all({
				vehiculo => vehiculo.velocidadMaxima() >= 100
			})
	}
	
	method capacidadTotalYendoA(velocidad) {
		return flota.filter({
			vehiculo => vehiculo.velocidadMaxima() >= velocidad
		}).map({
			vehiculo => vehiculo.capacidad()
		}).sum()
	}
	
	method colorDelAutoMasRapido() {
		return flota.max({
			vehiculo => vehiculo.velocidadMaxima()
		}).color()
	}
	
	method queVehiculosPuedenHacer(viaje) {
		return flota.filter({
			vehiculo => viaje.puedeSerHechoPor(vehiculo)
		})
	}
	
	method cuantosViajesHizo(vehiculo) {
		return viajes.count({
			viaje => viaje.vehiculo() == vehiculo
		})
	}
	
	method viajesRealizadosDistanciaMayorA(km) {
		return viajes.count({
			viaje => viaje.distancia() > km
		})
	}
	
	method totalLugaresLibresEnViajesRealizados() {
		return viajes.map({
			viaje => viaje.vehiculo().capacidad() - viaje.cantPasajeros()
		}).sum()
	}
	
	method costo(viaje) {
		return viaje.distancia() * valorPorKm
	}
	
	method cuantoPagarPor(viaje) {
		if (self.costo(viaje) > minimoPorViaje) {
			return self.costo(viaje)
		} else {
			return minimoPorViaje
		}
	}
	
	method cuantoPagarleA(vehiculo) {
		return viajes.filter({
			viaje => viaje.vehiculo() == vehiculo
		}).map({
			viaje => self.cuantoPagarPor(viaje)
		}).sum()
	}
	
}

class Viaje {
	
	const distancia // en km
	const tiempoMaximo // en horas
	const cantPasajeros
	const coloresIncompatibles 	// conj de colores
	var vehiculo
	
	
	constructor(_distancia, _tiempoMaximo, _cantPasajeros, _coloresIncompatibles) {
		distancia = _distancia
		tiempoMaximo = _tiempoMaximo
		cantPasajeros = _cantPasajeros
		coloresIncompatibles = _coloresIncompatibles
	}
	
	method distancia() {
		return distancia
	}
	
	method cantPasajeros() {
		return cantPasajeros
	}
	
	method vehiculo(_vehiculo) {
		vehiculo = _vehiculo
	}
	
	method vehiculo() {
		if (vehiculo == null) {
			error.throwWithMessage(self.className() + ": vehiculo no asignado")
		}
		return vehiculo
	}
	
	method puedeSerHechoPor(_vehiculo) {
		return _vehiculo.velocidadMaxima() >= 10 + (distancia / tiempoMaximo)
			and _vehiculo.capacidad() >= cantPasajeros
			and not coloresIncompatibles.contains(_vehiculo.color())
	}
	
}

class ChevroletCorsa {
	
	const capacidad = 4
	const velocidadMaxima = 150
	const peso = 1300
	const color
	
	
	constructor(_color) {
		color = _color
	}
	
	method capacidad() {
		return capacidad
	}
	
	method velocidadMaxima() {
		return velocidadMaxima
	}
	
	method peso() {
		return peso
	}
	
	method color() {
		return color
	}
	
}

class AutoStandardAGas {
	
	const tieneTanqueAdicional
	const capacidadSinTanqueAdicional = 4
	const capacidadConTanqueAdicional = 3
	const velocidadMaximaConTanqueAdicional = 120 
	const velocidadMaximaSinTanqueAdicional = 110
	const pesoSinTanqueAdicional = 1200
	const pesoTanqueAdicional = 150
	const color = "azul"
	
	
	constructor(_tieneTanqueAdicional) {
		tieneTanqueAdicional = _tieneTanqueAdicional
	}
	
	method capacidad() {
		if (tieneTanqueAdicional) {
			return capacidadConTanqueAdicional
		} else {
			return capacidadSinTanqueAdicional
		}
	}
	
	method velocidadMaxima() {
		if (tieneTanqueAdicional) {
			return velocidadMaximaConTanqueAdicional
		} else {
			return velocidadMaximaSinTanqueAdicional
		}
	}
	
	method color() {
		return color
	}
	
	method peso() {
		if (tieneTanqueAdicional) {
			return pesoSinTanqueAdicional + pesoTanqueAdicional
		} else {
			return pesoSinTanqueAdicional
		}
	}
	
}


class AutoDistinto {
	
	const capacidad
	const velocidadMaxima
	const peso
	const color
	
	
	constructor(_capacidad, _velocidadMaxima, _peso, _color) {
		capacidad = _capacidad
		velocidadMaxima = _velocidadMaxima
		peso = _peso
		color = _color
	}
	
	method capacidad() {
		return capacidad
	}
	
	method velocidadMaxima() {
		return velocidadMaxima
	}
	
	method peso() {
		return peso
	}
	
	method color() {
		return color
	}
	
}

object trafic {
	
	const pesoBase = 4000
	var interior
	var motor
	const color = "blanco"
	
	
	method interior(_interior) {
		interior = _interior
	}
	
	method motor(_motor) {
		motor = _motor
	}
	
	method capacidad() {
		return interior.capacidad()
	}
	
	method velocidadMaxima() {
		return motor.velocidadMaxima()
	}
	
	method peso() {
		return pesoBase + interior.peso() + motor.peso()
	}
	
	method color() {
		return color
	}
	
}

object interiorComodo {
	
	const capacidad = 5
	const peso = 700
	
	
	method capacidad() {
		return capacidad
	}
	
	method peso() {
		return peso
	}
	
}

object interiorPopular {
	
	const capacidad = 12
	const peso = 1000
	
	
	method capacidad() {
		return capacidad
	}
	
	method peso() {
		return peso
	}
	
}

object motorPulenta {
	
	const velocidadMaxima = 130
	const peso = 800
	
	
	method velocidadMaxima() {
		return velocidadMaxima
	}
	
	method peso() {
		return peso
	}
	
}

object motorBataton {
	
	const velocidadMaxima = 80
	const peso = 500
	
	
	method velocidadMaxima() {
		return velocidadMaxima
	}
	
	method peso() {
		return peso
	}
	
}


class GobernacionEstelar {
	
	var unidades = #{}
	
	method agregarUnidad(unidad) {
		unidades.add(unidad)
	}
	
	method quitarUnidad(unidad) {
		unidades.remove(unidad)
	}
	
	method recibirAmenaza() {
		unidades.forEach({
			unidad => unidad.recibirAmenaza()
		})
	}
	
}

class DepositoEstelar {
	
	var cantContainers = 0
	
	method recibirContainers(cuantos) {
		cantContainers += cuantos
	}
	
	method recibirAmenaza() {
		cantContainers /= 2
	}
	
}

class NaveEspacial {
	
	var velocidad = 0	// entre 0 y 100000 km/s
	var direccion = 0	// entre -10 y 10
	// -10: opuesta al sol
	// -9..-1: orbita espiral, alejandose del sol
	// 0: orbita circular alrededor del sol
	// 1..9: orbita espiral, acercandose al sol
	// 10: directa al sol
	
	method acelerar(cuanto) {
		// cuanto: en km/s, numero positivo
		velocidad = 100000.min(velocidad + cuanto)
	}
	
	method desacelerar(cuanto) {
		// cuanto: en km/s, numero positivo
		velocidad = 0.max(velocidad - cuanto)
	}
	
	method irHaciaElSol() {
		direccion = 10
	}
	
	method escaparDelSol() {
		direccion = -10
	}
	
	method alejarseUnPocoDelSol() {
		direccion = (-10).max(direccion - 1)
	}
	
	method acercarseUnPocoAlSol() {
		direccion = 10.min(direccion + 1)
	}
	
	method velocidad() {
		return velocidad
	}
	
	method direccion() {
		return direccion
	}
	
	method recibirAmenaza() {
		self.escapar()
		self.avisar()
	}
	
	method escapar()
	
	method avisar()
	
}

class NaveBaliza inherits NaveEspacial {
	
	var color = "azul"
	
	override method escapar() {
		self.irHaciaElSol()
	}
	
	override method avisar() {
		color = "rojo"
	}
	
	method volverALaNormalidad() {
		color = "azul"
	}
	
}

class NaveDePasajeros inherits NaveEspacial {
	
	var cantPasajeros
	var stockRacionesComida
	var stockRacionesBebida
	
	constructor(pasajeros, racComida, racBebida) {
		cantPasajeros = pasajeros
		stockRacionesComida = racComida
		stockRacionesBebida = racBebida
	}
	
	override method escapar() {
		self.acelerar(velocidad)
	}
	
	override method avisar() {
		stockRacionesComida = 0.max(stockRacionesComida - cantPasajeros)
		stockRacionesBebida = 0.max(stockRacionesBebida - (2 * cantPasajeros))
	}
	
}

class NaveDeCombate inherits NaveEspacial {
	
	var invisible = false
	var misilesDesplegados = false
	var mensajesEmitidos = []
	
	method estaInvisible() {
		return invisible
	}
	
	method tieneMisilesDesplegados() {
		return misilesDesplegados
	}
	
	method necesitaCambiarTripulacion() {
		
	}
	
	method ponerseInvisible() {
		invisible = true
	}
	
	method ponerseVisible() {
		invisible = false
	}
	
	method desplegarMisiles() {
		misilesDesplegados = true
	}
	
	method replegarMisiles() {
		misilesDesplegados = false
	}
	
	method emitirMensaje(sms) {
		mensajesEmitidos.add(sms)
	}
	
	method cantMensajesEmitidos() {
		return mensajesEmitidos.size()
	}
	
	method primerMensajeEmitido() {
		return mensajesEmitidos.first()
	}
	
	method ultimoMensajeEmitido() {
		return mensajesEmitidos.last()
	}
	
	method esEscueta() {
		return mensajesEmitidos.all({
			sms => sms.size() < 30
		})
	}
	
	method algunaVezEmitio(sms) {
		return mensajesEmitidos.contains(sms)
	}
	
	override method escapar() {
		2.times({
			self.acercarseUnPocoAlSol()
		})
		self.ponerseInvisible()
		self.desplegarMisiles()
	}
	
	override method avisar() {
		self.emitirMensaje("Amenaza recibida")
	}
	
}

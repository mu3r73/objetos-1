
// partes 2 y 3

class Empresa {
	
	var vehiculos = #{}
	
	method agregarVehiculo(vehiculo) {
		vehiculos.add(vehiculo)
	}
	
	method quitarVehiculo(vehiculo) {
		vehiculos.remove(vehiculo)
	}
	
	method vehiculosQuePuedenRealizar(viaje) {
		return vehiculos.filter({
			vehiculo => viaje.puedeSerRealizadoPor(vehiculo)
		})
	}
	
	method tieneCoberturaTotalPara(provincia) {
		return vehiculos.all({
			vehiculo => vehiculo.estaHabilitadoPara(provincia)
		})
	}
	
}

class Viaje {
	
	const ruta
	const carga	// en kg
	const tiempoMax			// en h
	
	constructor(_ruta, kg, horas) {
		ruta = _ruta
		carga = kg
		tiempoMax = horas
	}
	
	method ruta() {
		return ruta
	}
	
	method carga() {
		return carga
	}
	
	method tiempoMax() {
		return tiempoMax
	}
	
	method esRiesgoso() {
		return (carga >= 5000) and ruta.estaDaniada()
	}
	
	method puedeSerRealizadoPor(vehiculo) {
		return ruta.puedeSerRecorridaPor(vehiculo)
			and vehiculo.cargaMaxima() >= carga
			and vehiculo.tiempoParaRealizar(self) <= tiempoMax
	}
	
}

class Vehiculo {
	
	const tara
	var provinciasHabilitadas = #{}
	
	constructor(_tara) {
		tara = _tara
	}
	
	method habilitar(provincia) {
		provinciasHabilitadas.add(provincia)
	}
	
	method inhabilitar(provincia) {
		provinciasHabilitadas.remove(provincia)
	}
	
	method estaHabilitadoPara(provincia) {
		return provinciasHabilitadas.contains(provincia)
	}
	
	method cargaMaxima()
	
	method tiempoParaRealizar(viaje)
	
	method pesoMaximoTotal() {
		return tara + self.cargaMaxima()
	}
	
}

class Camioneta inherits Vehiculo {
	
	const cargaMaxima = 1200
	
	constructor(_tara) = super(_tara)
	
	override method cargaMaxima() {
		return cargaMaxima
	}
	
	override method tiempoParaRealizar(viaje) {
		return viaje.ruta().cantKmEnBuenEstado() / 120.0
			+ viaje.ruta().cantKmEnMalEstado() / 100.0
	}
	
}

class Combi inherits Vehiculo {
	
	var tieneCapota
	const cargaMaximaSinCapota = 2500
	const pesoCapota = 1000
	
	constructor(_tara, _tieneCapota) = super(_tara) {
		tieneCapota = _tieneCapota 
	}
	
	method agregarCapota() {
		tieneCapota = true
	}
	
	method quitarCapota() {
		tieneCapota = false
	}
	
	override method cargaMaxima() {
		if (tieneCapota) {
			return cargaMaximaSinCapota + pesoCapota
		} else {
			return cargaMaximaSinCapota
		}
	}
	
	override method tiempoParaRealizar(viaje) {
		if (viaje.carga() <= 1800) {
			return viaje.ruta().cantTotalKm() / 100.0
		} else {
			return viaje.ruta().cantTotalKm() / 90.0
		} 
	}
	
}

class Camion inherits Vehiculo {
	
	const nroEjes
	const cargaMaxPorEje = 6000
	
	constructor(_tara, _nroEjes) = super(_tara) {
		nroEjes = _nroEjes
	}
	
	override method cargaMaxima() {
		return nroEjes * cargaMaxPorEje
	}
		
	override method tiempoParaRealizar(viaje) {
		return 15
			+ viaje.ruta().cantKmEnBuenEstado() / 100.0
		 	+ viaje.ruta().cantKmEnMalEstado() / self.velocEnKmMalEstado(viaje.carga())
		 	+ self.demoraPorEjes()
	}
	
	method velocEnKmMalEstado(carga) {
		if (carga <= 10000) {
			return 70
		} else {
			return 60
		}
	}
	
	method demoraPorEjes() {
		if (nroEjes > 2) {
		 	return 1
		} else {
			return 0
		}
	}
	
}

class Ruta {
	
	const cantTotalKm
	var cantKmEnMalEstado = 0.0
	var provinciasPorLasQuePasa = #{}
	
	constructor(kmTot) {
		cantTotalKm = kmTot
	}
	
	method cantTotalKm() {
		return cantTotalKm
	}
	
	method cantKmEnBuenEstado() {
		return (cantTotalKm - cantKmEnMalEstado)
	}
	
	method cantKmEnMalEstado(km) {
		// no puede ser mayor a cantTotalKm
		cantKmEnMalEstado = km.min(cantTotalKm)
	}
	
	method cantKmEnMalEstado() {
		return cantKmEnMalEstado
	}
	
	method agregarProvincia(provincia) {
		provinciasPorLasQuePasa.add(provincia)
	}
	
	method estaDaniada() {
		return (cantKmEnMalEstado > self.cantKmEnBuenEstado())
	}
	
	method sePuedeHacerConGNC() {
		return provinciasPorLasQuePasa.all({
			provincia => provincia.vendeGNC()
		})
	}
	
	method puedeSerRecorridaPor(vehiculo) {
		return provinciasPorLasQuePasa.all({
			provincia => vehiculo.estaHabilitadoPara(provincia)
		})
	}
	
}

class Provincia {
	
	const nombre
	var vendeGNC
	
	constructor(_nombre, _vendeGNC) {
		nombre = _nombre
		vendeGNC = _vendeGNC
	}
	
	method nombre() {
		return nombre
	}
	
	method vendeGNC() {
		return vendeGNC
	}
	
}

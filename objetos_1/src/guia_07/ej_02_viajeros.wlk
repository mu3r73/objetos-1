
class Persona {
	
	var viajes = #{}
	
	method agregarViaje(viaje) {
		viajes.add(viaje)
	}
	
	method enQuePaisesEstuvo(anio) {
		return viajes.filter({
			viaje => viaje.anio() == anio
		}).map({
			viaje => viaje.pais()
		}).asSet()
	}
	
	method coincidioCon(pers, anio) {
		return not self.enQuePaisesEstuvo(anio).intersection(pers.enQuePaisesEstuvo(anio)).isEmpty()
	}
	
}

class Establecido inherits Persona {
	
	const paisResidencia
	
	constructor(paisRes) {
		paisResidencia = paisRes
	}
	
	method residencia(anio) {
		return #{paisResidencia}
	}
	
	override method enQuePaisesEstuvo(anio) {
		return self.residencia(anio) + super(anio)
	}
	
}

class Migrante inherits Persona {
	
	const paisNacimiento
	const paisResidencia
	const anioMigracion
	
	constructor(paisNac, paisRes, anioMig) {
		paisNacimiento = paisNac
		paisResidencia = paisRes
		anioMigracion = anioMig
	}
	
	method residencia(anio) {
		if (anio < anioMigracion) {
			return #{paisNacimiento}
		} else if (anio > anioMigracion) {
			return #{paisResidencia}
		} else {
			return #{paisNacimiento, paisResidencia}
		}
	}
	
	override method enQuePaisesEstuvo(anio) {
		return self.residencia(anio) + super(anio)
	}
	
}

class Doctor inherits Persona {
	
	const paisResidencia
	const paisDoctorado
	const anioIniDoctorado
	const anioFinDoctorado
	
	constructor(paisRes, paisDoc, anioIniDoc, anioFinDoc) {
		paisResidencia = paisRes
		paisDoctorado = paisDoc
		anioIniDoctorado = anioIniDoc
		anioFinDoctorado = anioFinDoc
	}
	
	method residencia(anio) {
		if (anio.between(anioIniDoctorado, anioFinDoctorado)) {
			return paisDoctorado
		} else {
			return paisResidencia
		}
	}
	
	override method enQuePaisesEstuvo(anio) {
		return self.residencia(anio) + super(anio)
	}
	
}

class Menor inherits Persona {
	
	const madre
	
	constructor(_madre) {
		madre = _madre
	}
	
	method residencia(anio) {
		return madre.residencia(anio)
	}
	
	override method enQuePaisesEstuvo(anio) {
		return self.residencia(anio) + super(anio)
	}
	
}

class Viaje {
	
	var pais
	var anio
	
	constructor(_pais, _anio) {
		pais = _pais
		anio = _anio
	}
	
	method pais() {
		return pais
	}
	
	method anio() {
		return anio
	}
	
}

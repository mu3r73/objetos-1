
class Animal {
	
	method estaFeliz() {
		return self.convieneVacunar()
			and (self.peso() < 10 or self.peso() > 150)
	}
	
	method convieneVacunar()
	
	method peso()
	
}

class Vaca inherits Animal {
	
	var peso 		// en kg
	var tieneSed = false
	var fueVacunada = false
	
	
	constructor(_peso) {
		peso = _peso
	}
	
	override method peso() {
		return peso
	}
	
	method comer(gramos) {
		peso += gramos / (3 * 1000)	// gana gramos / 3
		tieneSed = true
	}
	
	method beber() {
		tieneSed = false
		peso -= 500 / 1000	// pierde 500 gramos
	}
	
	method tieneHambre() {
		return peso < 200
	}
	
	method tieneSed() {
		return tieneSed
	}
	
	method recibirVacuna() {
		fueVacunada = true
	}
	
	override method convieneVacunar() {
		return not fueVacunada
	}
	
	method caminar() {
		peso -= 3
	}
	
}

class VacaZen inherits Vaca {
	
	constructor(_peso) = super(_peso)
	
	override method tieneSed() {
		return fueVacunada or super()
	}
	
}

class VacaReflexiva inherits Vaca {
	
	var vecesQueCamino = 0
	
	constructor(_peso) = super(_peso)
	
	override method caminar() {
		vecesQueCamino += 1
	}
	
	override method estaFeliz() {
		return super() and (vecesQueCamino >= 2)
	}
	
}

class VacaFilosofa inherits VacaReflexiva {
	
	constructor(_peso) = super(_peso)
	
	override method estaFeliz() {
		return super() and (not self.tieneSed())
	}
	
}

class Cerdo inherits Animal {
	
	var peso		// en kg
	var tieneHambre = false
	var tieneSed = false
	var maxComido = 0
	var vecesQueComioSinBeber = 0
	var convieneVacunar = true
	
	
	constructor(_peso) {
		peso = _peso
	}
	
	override method peso() {
		return peso
	}
	
	method comer(gramos) {
		if (gramos > 200) {
			peso += (gramos - 200) / 1000
		}
		
		if (gramos > 1000) {
			tieneHambre = false
		}
		
		maxComido = maxComido.max(gramos)
		
		vecesQueComioSinBeber += 1
		if (vecesQueComioSinBeber > 3) {
			tieneSed = true
		}
	}
	
	method beber() {
		tieneSed = false
		tieneHambre = true
		peso -= 1
		vecesQueComioSinBeber = 0
	}
	
	method tieneHambre() {
		return tieneHambre
	}
	
	method tieneSed() {
		return tieneSed
	}
	
	override method convieneVacunar() {
		return convieneVacunar
	}
	
	method recibirVacuna() {
		
	}
	
	method maxComido() {
		return maxComido
	}
	
	method vecesQueComioSinBeber() {
		return vecesQueComioSinBeber
	}
	
}

class CerdoResistente inherits Cerdo {
	
	constructor(_peso) = super(_peso)
	
	
	override method recibirVacuna() {
		convieneVacunar = false
	}
	
	override method comer(gramos) {
		super(gramos)
		if (gramos > 5000) {
			convieneVacunar = true
		}
	}
	
}

class CerditoAlegre inherits Cerdo {
	
	constructor(_peso) = super(_peso)
	
	override method estaFeliz() {
		return true
	}
	
}

class Gallina inherits Animal {
	
	const peso = 4
	const tieneHambre = true
	const tieneSed = false
	const convieneVacunar = false
	var vecesQueComio = 0
	
	
	override method peso() {
		return peso
	}
	
	method comer(gramos) {
		vecesQueComio += 1
	}
	
	method tieneHambre() {
		return tieneHambre
	}
	
	method tieneSed() {
		return tieneSed
	}
	
	override method convieneVacunar() {
		return convieneVacunar
	}
	
	method recibirVacuna() {
		
	}
	
	method vecesQueComio() {
		return vecesQueComio
	}
	
}

class GallinaTuruleca inherits Gallina {
	
	var huevosPuestos = 0
	
	
	method haPuestoUnHuevo() {
		huevosPuestos += 1
	}
	
	method haPuestoDosHuevos() {
		huevosPuestos += 2
	}
	
	method haPuestoTresHuevos() {
		huevosPuestos += 3
	}
	
	override method tieneSed() {
		return (huevosPuestos.odd())
	}
	
}

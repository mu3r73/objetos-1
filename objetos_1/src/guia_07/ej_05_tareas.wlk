
class Supervisor {
	
	var tareas = #{}
	var presupuesto
	
	method asignarTarea(tarea) {
		tareas.add(tarea)
	}
	
	method presupuesto(pesos) {
		presupuesto = pesos
	}
	
	method tareasHechas() {
		return tareas.filter({
			tarea => tarea.estaHecha()
		})
	}
	
	method costoTotalTareas(t) {
		return t.map({
			tarea => tarea.costoTotal()
		}).sum()
	}
	
	method costoTotalTareasHechas() {
		return self.costoTotalTareas(self.tareasHechas())
	}
	
	method necesitaFondosAdicionales() {
		return self.costoTotalTareas(tareas.difference(self.tareasHechas())) > presupuesto
	}
	
	method especializadoEnGrandesObras() {
		return tareas.all({
			tarea => tarea.cantHoras() > 40
		})
	}
	
	method terminarConTodo() {
		tareas.forEach({
			tarea => tarea.registrarQueSeHizo()
		})
	}
	
	method tareasAsignadasConCostoHorarioMayorA(pesos) {
		tareas.filter({
			tarea => tarea.costoHorario() > pesos
		})
	}
	
}

class Tarea {
	
	const gastosPapeleo
	var estaHecha = false
	
	constructor(pesos) {
		gastosPapeleo = pesos
	}
	
	method costoTotal() {
		return self.costoHorario() * self.cantHoras()
			+ self.costoInicial()
			+ gastosPapeleo
	}
	
	method costoInicial()
	
	method costoHorario()
	
	method cantHoras()
	
	method registrarQueSeHizo() {
		estaHecha = true
	}
	
	method estaHecha() {
		return estaHecha
	}
	
}

class TareaRehecha inherits Tarea {
	
	var tarea
	
	constructor(pesos, _tarea) = super(pesos) {
		tarea = _tarea
	}
	
	override method costoInicial() {
		return 1500 + tarea.costoInicial()
	}
	
	override method costoHorario() {
		return tarea.costoHorario()
	}
	
	override method cantHoras() {
		return 5 + tarea.cantHoras()
	}
	
}

class TareaEncuesta inherits Tarea {
	
	const cantPersonas
	const distancia
	
	constructor(pesos, _cantPersonas, _distancia) = super(pesos) {
		cantPersonas = _cantPersonas
		distancia = _distancia
	}
	
	override method costoInicial() {
		return 12 * distancia
	}
	
	override method costoHorario() {
		return 180
	}
	
	override method cantHoras() {
		return cantPersonas / 3.0
	}
	
}

class TareaConstruccion inherits Tarea {
	
	const duracion			// dias
	const cantPersonasReq
	const pagoPorHora		// por persona
	const costoInicial
	
	constructor(pesos, _costoInicial, _duracion, _cantPersonasReq, _pagoPorHora) = super(pesos) {
		costoInicial = _costoInicial
		duracion = _duracion
		cantPersonasReq = _cantPersonasReq
		pagoPorHora = _pagoPorHora
	}
	
	override method costoInicial() {
		return costoInicial
	}
	
	override method costoHorario() {
		return cantPersonasReq * pagoPorHora
	}
	
	override method cantHoras() {
		return 8 * duracion
	} 
	
}

class TareaMantenimientoDeEquipos inherits Tarea {
	
	var tecnico
	const tipoDeEquipo
	const cantDeEquipos
	
	constructor(pesos, _tipoDeEquipo, _cantDeEquipos) = super(pesos) {
		tipoDeEquipo = _tipoDeEquipo
		cantDeEquipos = _cantDeEquipos
	}
	
	override method costoInicial() {
		return 0
	}
	
	override method costoHorario() {
		return tecnico.pagoPorHora()
	}
	
	override method cantHoras() {
		return cantDeEquipos * tecnico.tiempoPorEquipo()
	}
	
	method tecnico(_tecnico) {
		tecnico = _tecnico
	}
	
	method tecnico() {
		return tecnico
	}
	
	method tipoDeEquipo() {
		return tipoDeEquipo
	}
	
}

class Tecnico {
	
	const especializadoEn
	const tiempoPorEquipo
	const pagoPorHora
	
	constructor(equipo, horas, pesos) {
		especializadoEn = equipo
		tiempoPorEquipo = horas
		pagoPorHora = pesos
	}
	
	method especializadoEn() {
		return especializadoEn
	}
	
	method tiempoPorEquipo() {
		return tiempoPorEquipo
	}
	
	method pagoPorHora() {
		return pagoPorHora
	}
	
}

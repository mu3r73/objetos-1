
// parte 1

class Empresa {
	
	var vehiculos = #{}
	
	method agregarVehiculo(vehiculo) {
		vehiculos.add(vehiculo)
	}
	
	method quitarVehiculo(vehiculo) {
		vehiculos.remove(vehiculo)
	}
	
	method vehiculosQuePuedenRealizar(viaje) {
		return vehiculos.filter({
			vehiculo => viaje.puedeSerRealizadoPor(vehiculo)
		})
	}
	
	method tieneCoberturaTotalPara(provincia) {
		if (vehiculos.isEmpty()) {
			return false
		} else {
			return vehiculos.all({
				vehiculo => vehiculo.estaHabilitadoPara(provincia)
			})
		}
	}
	
}

class Viaje {
	
	const ruta
	const carga	// en kg
	
	constructor(_ruta, kg) {
		ruta = _ruta
		carga = kg
	}
	
	method ruta() {
		return ruta
	}
	
	method carga() {
		return carga
	}
	
	method esRiesgoso() {
		return (carga >= 5000) and ruta.estaDaniada()
	}
	
	method puedeSerRealizadoPor(vehiculo) {
		return ruta.puedeSerRecorridaPor(vehiculo)
	}
	
}

class Vehiculo {
	
	var provinciasHabilitadas = #{}
	
	method habilitar(provincia) {
		provinciasHabilitadas.add(provincia)
	}
	
	method inhabilitar(provincia) {
		provinciasHabilitadas.remove(provincia)
	}
	
	method estaHabilitadoPara(provincia) {
		return provinciasHabilitadas.contains(provincia)
	}
	
}

class Ruta {
	
	const cantTotalKm
	var cantKmEnMalEstado = 0
	var provinciasPorLasQuePasa = #{}
	
	constructor(kmTot) {
		cantTotalKm = kmTot
	}
	
	method cantTotalKm() {
		return cantTotalKm
	}
	
	method cantKmEnBuenEstado() {
		return cantTotalKm - cantKmEnMalEstado
	}
	
	method cantKmEnMalEstado(km) {
		// no puede ser mayor a cantTotalKm
		cantKmEnMalEstado = km.min(cantTotalKm)
	}
	
	method cantKmEnMalEstado() {
		return cantKmEnMalEstado
	}
	
	method agregarProvincia(provincia) {
		provinciasPorLasQuePasa.add(provincia)
	}
	
	method estaDaniada() {
		return (cantKmEnMalEstado > self.cantKmEnBuenEstado())
	}
	
	method sePuedeHacerConGNC() {
		return provinciasPorLasQuePasa.all({
			provincia => provincia.vendeGNC()
		})
	}
	
	method puedeSerRecorridaPor(vehiculo) {
		return provinciasPorLasQuePasa.all({
			provincia => vehiculo.estaHabilitadoPara(provincia)
		})
	}
	
}

class Provincia {
	
	const nombre
	var vendeGNC
	
	constructor(_nombre, _vendeGNC) {
		nombre = _nombre
		vendeGNC = _vendeGNC
	}
	
	method nombre() {
		return nombre
	}
	
	method vendeGNC() {
		return vendeGNC
	}
	
}

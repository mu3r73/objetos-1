
import ej_04_empresa_de_transporte_1.*

test "Empresa: tiene cobertura total para provincia" {
	
	const plp = new Provincia("La Pampa", true)
	
	var veh1 = new Vehiculo()
	veh1.habilitar(plp)
	
	var veh2 = new Vehiculo()
	veh2.habilitar(plp)
	
	var veh3 = new Vehiculo()
	
	var e = new Empresa()

	assert.notThat(e.tieneCoberturaTotalPara(plp))

	e.agregarVehiculo(veh1)
	e.agregarVehiculo(veh2)
	assert.that(e.tieneCoberturaTotalPara(plp))
	
	e.agregarVehiculo(veh3)
	assert.notThat(e.tieneCoberturaTotalPara(plp))
	
}

test "Empresa: vehiculos que pueden realizar viaje" {
	
	const pba = new Provincia("Buenos Aires", true)
	const plp = new Provincia("La Pampa", true)
	const psl = new Provincia("San Luis", true)
	const pme = new Provincia("Mendoza", false)

	var r1 = new Ruta(750)
	r1.agregarProvincia(pba)
	r1.agregarProvincia(plp)
	r1.agregarProvincia(psl)
	
	var r2 = new Ruta(1000)
	r2.agregarProvincia(pba)
	r2.agregarProvincia(plp)
	r2.agregarProvincia(psl)
	r2.agregarProvincia(pme)
	
	var v1 = new Viaje(r1, 1000)
	var v2 = new Viaje(r2, 1500)
	
	var veh1 = new Vehiculo()
	veh1.habilitar(pba)
	veh1.habilitar(plp)
	veh1.habilitar(psl)
	veh1.habilitar(pme)
	
	var veh2 = new Vehiculo()
	veh2.habilitar(pba)
	veh2.habilitar(plp)
	veh2.habilitar(psl)
	
	var veh3 = new Vehiculo()
	
	var e = new Empresa()
	e.agregarVehiculo(veh1)
	e.agregarVehiculo(veh2)
	e.agregarVehiculo(veh3)
	
	assert.equals(#{veh1, veh2}, e.vehiculosQuePuedenRealizar(v1))
	assert.equals(#{veh1}, e.vehiculosQuePuedenRealizar(v2))
	
}

test "Viaje: puede ser realizado por vehiculo" {
	
	const pba = new Provincia("Buenos Aires", true)
	const plp = new Provincia("La Pampa", true)
	const psl = new Provincia("San Luis", true)
	const pme = new Provincia("Mendoza", false)

	var r1 = new Ruta(750)
	r1.agregarProvincia(pba)
	r1.agregarProvincia(plp)
	r1.agregarProvincia(psl)
	
	var r2 = new Ruta(1000)
	r2.agregarProvincia(pba)
	r2.agregarProvincia(plp)
	r2.agregarProvincia(psl)
	r2.agregarProvincia(pme)
	
	var v1 = new Viaje(r1, 1000)
	var v2 = new Viaje(r2, 1500)
	
	var veh1 = new Vehiculo()
	veh1.habilitar(pba)
	veh1.habilitar(plp)
	veh1.habilitar(psl)
	veh1.habilitar(pme)
	
	var veh2 = new Vehiculo()
	veh2.habilitar(pba)
	veh2.habilitar(plp)
	veh2.habilitar(psl)
	
	var veh3 = new Vehiculo()
	
	assert.that(v1.puedeSerRealizadoPor(veh1))
	assert.that(v1.puedeSerRealizadoPor(veh2))
	assert.notThat(v1.puedeSerRealizadoPor(veh3))
	
	assert.that(v2.puedeSerRealizadoPor(veh1))
	assert.notThat(v2.puedeSerRealizadoPor(veh2))
	assert.notThat(v1.puedeSerRealizadoPor(veh3))
	
}

test "Viaje: es riesgoso" {
	
	var r = new Ruta(1000)
	
	const v1 = new Viaje(r, 4999)
	assert.notThat(v1.esRiesgoso())
	
	const v2 = new Viaje(r, 5000)
	assert.notThat(v2.esRiesgoso())
	
	r.cantKmEnMalEstado(600)
	assert.notThat(v1.esRiesgoso())
	assert.that(v2.esRiesgoso())
	
}

test "Viaje" {
	
	const r1 = new Ruta(1000)
	const v1 = new Viaje(r1, 500)
	assert.equals(r1, v1.ruta())
	assert.equals(500, v1.carga())
	
	const r2 = new Ruta(500)
	const v2 = new Viaje(r2, 1000)
	assert.equals(r2, v2.ruta())
	assert.equals(1000, v2.carga())
	
}

test "Vehiculo: habilitar, inhabilitar provincia" {
	
	const pba = new Provincia("Buenos Aires", true)
	const plp = new Provincia("La Pampa", true)
	const psl = new Provincia("San Luis", true)

	var v = new Vehiculo()
	assert.notThat(v.estaHabilitadoPara(pba))
	assert.notThat(v.estaHabilitadoPara(plp))
	assert.notThat(v.estaHabilitadoPara(psl))
	
	v.habilitar(pba)
	v.habilitar(psl)
	assert.that(v.estaHabilitadoPara(pba))
	assert.notThat(v.estaHabilitadoPara(plp))
	assert.that(v.estaHabilitadoPara(psl))
	
	v.habilitar(plp)
	v.inhabilitar(psl)
	assert.that(v.estaHabilitadoPara(pba))
	assert.that(v.estaHabilitadoPara(plp))
	assert.notThat(v.estaHabilitadoPara(psl))
	
}

test "Ruta: puede ser recorrida por vehiculo" {
	
	const pba = new Provincia("Buenos Aires", true)
	const plp = new Provincia("La Pampa", true)
	const psl = new Provincia("San Luis", true)
	const pme = new Provincia("Mendoza", false)

	var r1 = new Ruta(750)
	r1.agregarProvincia(pba)
	r1.agregarProvincia(plp)
	r1.agregarProvincia(psl)
	
	var r2 = new Ruta(1000)
	r2.agregarProvincia(pba)
	r2.agregarProvincia(plp)
	r2.agregarProvincia(psl)
	r2.agregarProvincia(pme)
	
	var veh1 = new Vehiculo()
	veh1.habilitar(pba)
	veh1.habilitar(plp)
	veh1.habilitar(psl)
	veh1.habilitar(pme)
	
	var veh2 = new Vehiculo()
	veh2.habilitar(pba)
	veh2.habilitar(plp)
	veh2.habilitar(psl)
	
	var veh3 = new Vehiculo()
	
	assert.that(r1.puedeSerRecorridaPor(veh1))
	assert.that(r1.puedeSerRecorridaPor(veh2))
	assert.notThat(r1.puedeSerRecorridaPor(veh3))
	
	assert.that(r2.puedeSerRecorridaPor(veh1))
	assert.notThat(r2.puedeSerRecorridaPor(veh2))
	assert.notThat(r1.puedeSerRecorridaPor(veh3))
	
}

test "Ruta: se puede hacer con gnc" {
	
	const pba = new Provincia("Buenos Aires", true)
	const plp = new Provincia("La Pampa", true)
	const psl = new Provincia("San Luis", true)
	const pme = new Provincia("Mendoza", false)

	var r = new Ruta(1000)
	assert.that(r.sePuedeHacerConGNC())
	
	r.agregarProvincia(pba)
	r.agregarProvincia(plp)
	r.agregarProvincia(psl)
	assert.that(r.sePuedeHacerConGNC())
	
	r.agregarProvincia(pme)
	assert.notThat(r.sePuedeHacerConGNC())
	
}

test "Ruta" {
	
	var r = new Ruta(1000)
	assert.equals(1000, r.cantTotalKm())
	assert.equals(0, r.cantKmEnMalEstado())
	assert.equals(1000, r.cantKmEnBuenEstado())
	assert.notThat(r.estaDaniada())
	
	r.cantKmEnMalEstado(501)
	assert.equals(501, r.cantKmEnMalEstado())
	assert.equals(1000 - 501, r.cantKmEnBuenEstado())
	assert.that(r.estaDaniada())
	
	r.cantKmEnMalEstado(2000)
	assert.equals(1000, r.cantKmEnMalEstado())
	assert.equals(0, r.cantKmEnBuenEstado())
	assert.that(r.estaDaniada())
	
}

test "Provincia" {
	
	const pba = new Provincia("Buenos Aires", true)
	assert.equals("Buenos Aires", pba.nombre())
	assert.that(pba.vendeGNC())
	
	const pmi = new Provincia("Misiones", false)
	assert.equals("Misiones", pmi.nombre())
	assert.notThat(pmi.vendeGNC()) 
	
}

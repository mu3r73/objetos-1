
object pepita {
	
	var energia = 0
	
	
	method comer(que) {
		energia += que.energia()
	}
	
	method volar(kilometros) {
		energia -= 10 + kilometros
	}
	
	method energia() {
		return energia
	}
	
	method estaDebil() {
		return (energia < 50)
	}
	
	method estaFeliz() {
		return ((energia >= 500) and (energia <= 1000))
	}
	
	method cuantoQuiereVolar() {
		var cuanto = energia / 5
		if ((energia >= 300) and (energia <= 400)) {
			cuanto += 10
		}
		if (energia % 20 == 0) {
			cuanto += 15
		}
		return cuanto
	}

	method haceLoQueQuieras() {
		if (self.estaDebil()) {
			alpiste.peso(20)
			self.comer(alpiste) 
		}
		if (self.estaFeliz()) {
			self.volar(self.cuantoQuiereVolar())
		}
	}
	
}

object alpiste {
	
	var gramos = 50
	
	method peso(unosGramos) {
		gramos = unosGramos 
	}
	
	method energia() {
		return gramos * 4
	}
	
}

object nemo {
	
	var edad = 2	// en anios
	
	method edad(nuevaEdad) {
		edad = nuevaEdad	// en anios
	}
	
	method energia() {
		return 2 * edad
	}
	
}

object postreLight {
	
	method energia() {
		return 0
	}
	
}

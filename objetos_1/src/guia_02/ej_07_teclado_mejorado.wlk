
object telefono {

	var numero = ""
	var llamadasFijos = 0
	var llamadasCelulares = 0
	var tiempoFijos = 0
	var tiempoCelulares = 0
	var compania = companiaRRHH

	
	method agregarDigito(digito) {
		numero = numero + digito
	}
	
	method llamar(segundos) {
		if (self.esNumeroFijo()) {
			llamadasFijos++
			tiempoFijos += segundos
		} else if (self.esNumeroCelular()) {
			llamadasCelulares++
			tiempoCelulares += segundos
		}
		numero = ""
	}
	
	method numeroIngresado() {
		return numero
	}
	
	method esNumeroValido() {
		return (self.esNumeroFijo())
			or (self.esNumeroCelular()) 
	}
	
	method esNumeroFijo() {
		return (numero.length() == 5)
	}
	
	method esNumeroCelular() {
		return (numero.length() == 7) and numero.startsWith("15")
	}
 
 	method borrarUltimoDigito() {
 		numero = numero.substring(0, numero.size() - 1)
 	}
 	
 	method cantLlamadasFijos() {
 		return llamadasFijos
 	}
 	
 	method cantLlamadasCelular() {
 		return llamadasCelulares
 	}
 	
 	method tiempoTotalFijo() {
 		return tiempoFijos
 	}
 	
 	method tiempoTotalCelulares() {
 		return tiempoCelulares
 	}
 	
 	method compania(nuevaCompania) {
 		compania = nuevaCompania
 	}
 	
 	method totalFacturacion() {
 		return tiempoFijos * compania.precioFijo()
			+ tiempoCelulares * compania.precioCelular()
 	}
 	
 	method totalFacturacionConPoliticas() {
 		return compania.precioFijoConPolitica(tiempoFijos)
 			+ compania.precioCelularConPolitica(tiempoCelulares, llamadasCelulares)
 	}
 	
}

object companiaRRHH {
	
	method precioFijo() {
		return 0.45
	}
	
	method precioCelular() {
		return 0.70
	}
	
	method precioFijoConPolitica(segundos) {
		var precio = 0
		if (segundos > 1000) {
			precio = (segundos - 1000) * self.precioFijo()
		}
		return precio
	}
	
	method precioCelularConPolitica(segundos, llamadas) {
		return segundos * self.precioCelular()
	}
	
}

object companiaEstrellaFugaz {
	
	method precioFijo() {
		return 0.50
	}
	
	method precioCelular() {
		return 0.60
	}
	
	method precioFijoConPolitica(segundos) {
		var precio = segundos * self.precioFijo()
		if (segundos > 1500) {
			precio *= 0.90
		}
		return precio
	}
	
	method precioCelularConPolitica(segundos, llamadas) {
		var precio = segundos * self.precioCelular()
		if (llamadas > 120) {
			precio *= 0.85
		}
		return precio
	}
	
}


object paquete {
	
	var pago = false
	
	
	method estaPago() {
		return pago
	}
	
	method pagar() {
		pago = true
	}
	
	method puedeSerEntregado(mensajero, destino) {
		return pago and destino.puedePasar(mensajero)
	}
	
}

object spielberg {
	
	var ubicacion = puenteDeBrooklyn 
	
	
	method ubicacion() {
		return ubicacion
	}
	
	method asignarUbicacion(donde) {
		ubicacion = donde
	}
	
}

// destinos

object puenteDeBrooklyn {
	
	method puedePasar(mensajero) {
		return (mensajero.peso() <= 1000)
	}
	
}

object laMatrix {
	
	method puedePasar(mensajero) {
		return (mensajero.puedeLlamar())
	}
	
}

// mensajeros

object chuckNorris {
	
	method peso() {
		return 900
	}
	
	method puedeLlamar() {
		return true
	}
	
}

object neo {
	
	var credito = 0
	
	
	method peso() {
		return 0
	}
	
	method puedeLlamar() {
		return credito > 0
	}
	
}

object lincolnHawk {
	
	var peso = 80
	var transporte = bicicleta
	
	
	method peso() {
		return peso + transporte.peso()
	}
	
	method puedeLlamar() {
		return false
	}
	
	method peso(kg) {
		peso = kg
	}
	
	method transporte(vehiculo) {
		transporte = vehiculo
	}
	
}

// vehiculos de L. Hawk

object bicicleta {
	
	var peso = 10
	
	
	method peso() {
		return peso
	}
	
}

object camion {
	
	var peso = 1000
	var acoplados = 0
	
	
	method acoplados(cantidad) {
		acoplados = cantidad
	}
	
	method peso() {
		return peso + 500 * acoplados
	}
	
}

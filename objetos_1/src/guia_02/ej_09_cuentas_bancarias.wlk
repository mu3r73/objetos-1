
// parte 1: cuentas basicas

object cuentaPepe {
	// cuenta normal
	
	var saldo = 0
	
	
	method saldo() {
		return saldo
	}
	
	method depositar(pesos) {
		saldo += pesos
	}
	
	method extraer(pesos) {
		saldo -= pesos
	}
	
}

object cuentaJulian {
	// cuenta embargada
	
	var saldo = 0
	
	
	method saldo() {
		return saldo
	}
	
	method depositar(pesos) {
		saldo += pesos * 0.80
	}
	
	method extraer(pesos) {
		saldo -= pesos
		// gastos administrativos
		if (saldo > 5) {
			saldo -= 5
		}
	}
	
}

object cuentaPapa {
	// cuenta dolarizada
	
	var saldoEnDolares = 0
	var precioDeCompra = 14.70
	var precioDeVenta = 15.10
	
	
	method saldo() {
		return saldoEnDolares * precioDeCompra
	}
	
	method depositar(pesos) {
		saldoEnDolares += pesos / precioDeVenta
	}
	
	method extraer(pesos) {
		saldoEnDolares -= pesos / precioDeCompra
	}
	
	method precioDeCompra(pesos) {
		precioDeCompra = pesos
	}
	
	method precioDeVenta(pesos) {
		precioDeVenta = pesos
	}
	
}

// parte 2: cuenta combinada

object cuentaCombinada {
	
	var cuentaPrimaria = cuentaPepe
	var cuentaSecundaria = cuentaJulian
	
	
	method saldo() {
		return cuentaPrimaria.saldo() + cuentaSecundaria.saldo()
	}
	
	method depositar(pesos) {
		if (cuentaSecundaria.saldo() < 1000) {
			cuentaSecundaria.depositar(pesos)
		} else {
			cuentaPrimaria.depositar(pesos)
		}
	}
	
	method extraer(pesos) {
		if (pesos <= cuentaPrimaria.saldo()) {
			cuentaPrimaria.extraer(pesos)
		} else {
			cuentaSecundaria.extraer(pesos - cuentaPrimaria.saldo())
			cuentaPrimaria.extraer(cuentaPrimaria.saldo())
		}
	}
	
	method cuentaPrimaria(cuenta) {
		cuentaPrimaria = cuenta
	}
	
	method cuentaSecundaria(cuenta) {
		cuentaSecundaria = cuenta
	}
	
}

// parte 3: casa de Julian y Pepe

object casa {
	
	var cuenta = cuentaPepe
	var gastos = 0
	
	
	method cuenta(nuevaCuenta) {
		cuenta = nuevaCuenta
	}
	
	method comprar(pesos) {
		if (cuenta.saldo() < pesos) {
			error.throwWithMessage("saldo insuficiente")
		}
		gastos += pesos
		cuenta.extraer(pesos)
	}
	
	method esDerrochona() {
		return gastos > 5000
	}
	
	method esBacan() {
		return cuenta.saldo() >= 40000
	}
	
}


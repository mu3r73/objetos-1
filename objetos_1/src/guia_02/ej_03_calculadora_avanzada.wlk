
object calculadora {
	
	var res = 0
	var memoria
	var resAnterior
	
	
	method cargar(numero) {
		self.backup()
		res = numero
	}
	
	method sumar(numero) {
		self.backup()
		res += numero	
	}
	
	method restar(numero) {
		self.backup()
		res -= numero
	}
	
	method multiplicar(numero) {
		self.backup()
		res *= numero
	}
	
	method valorActual() {
		return res
	}
	
	// memoria	
		
	method cargarMemoria() {
		memoria = self.valorActual()
	}
	
	method limpiarMemoria() {
		memoria = 0
	}
	
	method memoria() {
		return memoria
	}
	
	method sumarMemoria() {
		self.sumar(memoria)
	}
	
	method restarMemoria() {
		self.restar(memoria)
	}
	
	method multiplicarMemoria() {
		self.multiplicar(memoria)
	}	
	
	// deshacer
	
	method backup() {
		resAnterior = self.valorActual()
	}
	
	method deshacer() {
		self.cargar(resAnterior)
	}
	
}

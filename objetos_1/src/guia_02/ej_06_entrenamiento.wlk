
object roque {
	
	method entrenar(ave) {
		ave.volar(10)
		alpiste.peso(300)
		ave.comer(alpiste)
		ave.volar(10)
		ave.haceLoQueQuieras()
	}
	
}

object pepita {
	
	var energia = 0
	
	
	method comer(que) {
		energia += que.energia()
	}
	
	method volar(kilometros) {
		energia -= 10 + kilometros
	}
	
	method energia() {
		return energia
	}
	
	method estaDebil() {
		return (energia < 50)
	}
	
	method estaFeliz() {
		return ((energia >= 500) and (energia <= 1000))
	}
	
	method cuantoQuiereVolar() {
		var cuanto = energia / 5
		if ((energia >= 300) and (energia <= 400)) {
			cuanto += 10
		}
		if (energia % 20 == 0) {
			cuanto += 15
		}
		return cuanto
	}

	method haceLoQueQuieras() {
		if (self.estaDebil()) {
			alpiste.peso(20)
			self.comer(alpiste)
		}
		if (self.estaFeliz()) {
			self.volar(self.cuantoQuiereVolar())
		}
	}
	
}

object pepon {
	
	var energia = 0
	
	
	method comer(que) {
		energia += que.energia() / 2.0
	}
	
	method volar(kilometros) {
		energia -= 1 + 0.5 * kilometros
	}
	
	method energia() {
		return energia
	}
	
	method estaDebil() {
		return (energia < 50)
	}
	
	method estaFeliz() {
		return ((energia >= 500) and (energia <= 1000))
	}
	
	method cuantoQuiereVolar() {
		return 1
	}

	method haceLoQueQuieras() {
		self.volar(self.cuantoQuiereVolar())
	}
	
}

object pipa {
	
	var calorias = 0
	var kilometros = 0
	
	
	method comer(que) {
		// 1 kcaloria = 4184 joules
		// 1000 calorias = 4184 joules
		calorias += 4184 * que.energia() / 1000
	}
	
	method volar(km) {
		kilometros += km
	}
	
	method kmsRecorridos() {
		return kilometros
	}
	
	method caloriasIngeridas() {
		return calorias
	}
	
	method haceLoQueQuieras() {
		// no hace nada
	}
	
}

object alpiste {
	
	var gramos = 50
	
	method peso(unosGramos) {
		gramos = unosGramos 
	}
	
	method energia() {
		return gramos * 4
	}
	
}

object nemo {
	
	var edad = 2	// en anios
	
	method edad(nuevaEdad) {
		edad = nuevaEdad	// en anios
	}
	
	method energia() {
		return 2 * edad
	}
	
}

object postreLight {
	
	method energia() {
		return 0
	}
	
}

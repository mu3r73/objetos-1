
object silvestre {
	
	var energia = 100
	
	
	method velocidad() {
		return 5 + (energia / 10.0)
	}
	
	method correr(distancia) {
		energia -= self.energiaConsumida(distancia)
		if (energia < 0) {
			energia = 0
		}
	}
	
	method energiaConsumida(distancia) {
		return 0.5 * distancia * self.velocidad()
	}
	
	method comer(personaje) {
		energia += personaje.energiaProvista()
	}
	
	method energia() {
		return energia
	}
	
	method convieneComerA(personaje, distancia) {
		return personaje.energiaProvista() > self.energiaConsumida(distancia)
	}
	
}

object tweety {
	
	var peso = 50
	
	
	method peso(gramos) {
		peso = gramos
	}
	
	method energiaProvista() {
		return 12 + peso
	}
	
}

object speedyGonzalez {
	
	method energiaProvista() {
		return 100000
	}
}

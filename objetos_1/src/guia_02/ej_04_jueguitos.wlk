
object pepe {
	
	var record = 0
	
	method jueguito() {
		contador.inc()
	}
	
	method pique() {
		if (contador.valorActual() > record) {
			record = contador.valorActual()
		}
		contador.reset()
	}
	
	method acumulado() {
		return contador.valorActual()
	}
	
	method record() {
		return record
	}
	
}

object pepeAlt {
	
	method jueguito() {
		contador.inc()
	}
	
	method pique() {
		recordDePepe.nuevoRecord(contador.valorActual())
		contador.reset()
	}
	
	method acumulado() {
		return contador.valorActual()
	}
	
	method record() {
		return recordDePepe.recordActual()
	}
	
}

object recordDePepe {
	
	var record = 0
	
	method nuevoRecord(valor) {
		if (valor > record) {
			record = valor
		}
	}
	
	method recordActual() {
		return record
	}
	
}

object contador {
	
	var valor = 0
	var ultCmd = ""


	method reset() {
		valor = 0
		ultCmd = "reset"
	}
	
	method inc() {
		valor += 1
		ultCmd = "incremento"
	}
	
	method dec() {
		valor -= 1
		ultCmd = "decremento"
	}
	
	method valorActual() {
		return valor
	}
	
	method ultimoComando() {
		return ultCmd
	}

}

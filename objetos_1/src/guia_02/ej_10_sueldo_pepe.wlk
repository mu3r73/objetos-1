
object pepe {
	
	var faltas = 0
	var categoria = catCadete
	var descSindical = descSinSindicato
	var bonoPresentismo
	
	
	method faltas(dias) {
		faltas = dias
	}
	
	method categoria(nuevaCategoria) {
		categoria = nuevaCategoria
	}
	
	method descSindical(nuevoDescuento) {
		descSindical = nuevoDescuento
	}
	
	method bonoPresentismo(nuevoBonoPresentismo) {
		bonoPresentismo = nuevoBonoPresentismo
	}
	
	method sueldo() {
		return categoria.neto()
			- descSindical.desc(categoria.neto())
			+ bonoPresentismo.bono(faltas)
	}
	
}

object catGerente {
	
	method neto() {
		return 15000
	}
	
}

object catCadete {
	
	method neto() {
		return 20000
	}
	
}

object descPorcentual {
	
	method desc(neto) {
		return neto * 3 / 100
	}
	
} 

object descComprometido {
	
	method desc(neto) {
		return 1500 + neto / 100
	}
	
}

object descSinSindicato {
	
	method desc(neto) {
		return 0
	}
	
}

object bonoPresNormal {
	
	method bono(faltas) {
		if (faltas == 0) {
			return 2000
		} else if (faltas == 1) {
			return 1000
		} else {
			return 0
		}
	}
	
}

object bonoPresAjuste {
	
	method bono(faltas) {
		if (faltas == 0) {
			return 10
		} else {
			return 0
		}
	}
	
}

object bonoPresDemagogico {
	
	method bono(faltas) {
		return 500
	}
	
}


object pelota {
	
	method caer() {
		linterna.encender()
	}
	
}

object linterna {
	
	method encender() {
		lupa.iluminar()
	}
	
}

object lupa {
	
	method iluminar() {
		vela.encender()
	}
	
}

object vela {
	
	var encendida = false
	
	method encender() {
		encendida = true
	}
	
	method encendida() {
		return encendida
	}
	
}

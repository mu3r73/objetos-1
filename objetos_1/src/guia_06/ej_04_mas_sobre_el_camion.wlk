
// parte 1: camión

object camion {
	
	var cosas = #{}
	const tara = 1000
	const pesoMax = 2500
	
	
	method cargar(cosa) {
		cosas.add(cosa)
	}
	
	method descargar(cosa) {
		cosas.remove(cosa)
	}
	
	method pesoTotal() {
		return tara + cosas.sum({
			cosa => cosa.peso()
		}) 
	}
	
	method excedidoDePeso() {
		return self.pesoTotal() > pesoMax
	}
	
	method objetosPeligrosos(n) {
		return cosas.filter({
			obj => obj.peligrosidad() > n
		})
	}
	
	method objetosMasPeligrososQue(cosa) {
		return self.objetosPeligrosos(cosa.peligrosidad())
	}
	
	method puedeCircularEnRuta(nivelMaxPeligrosidad) {
		return self.objetosPeligrosos(nivelMaxPeligrosidad) == #{}
	}
	
}

// parte 2: cosas

object knightRider {
	
	const peso = 500
	const peligrosidad = 10
	

	method peso() {
		return peso
	}
	
	method peligrosidad() {
		return peligrosidad
	}
	
}

object bumblebee {
	
	const peso = 800
	const peligrosidadAuto = 15
	const peligrosidadRobot = 30
	var estadoRobot = true
	
	
	method peso() {
		return peso
	}
	
	method transformar() {
		estadoRobot = not estadoRobot
	}
	
	method peligrosidad() {
		if (estadoRobot) {
			return peligrosidadRobot
		} else { // estado auto
			return peligrosidadAuto
		}
	}
	
}

object paqueteDeLadrillos {
	
	const pesoLadrillo = 2
	var cantLadrillos
	const peligrosidad = 2
	
	
	method cantLadrillos(cant) {
		cantLadrillos = cant
	}
	
	method peso() {
		return cantLadrillos * pesoLadrillo
	}
	
	method peligrosidad() {
		return peligrosidad
	}
	
}

object arenaAGranel {
	
	var peso
	const peligrosidad = 1
	
	
	method peso(kg) {
		peso = kg
	}
	
	method peso() {
		if (peso == null) {
			error.throwWithMessage(self.kindName() + ": no tiene peso")
		}
		return peso
	}
	
	method peligrosidad() {
		return peligrosidad
	}
	
}

object bateriaAntiAerea {
	
	var tieneMisiles = false
	const pesoConMisiles = 300
	const pesoSinMisiles = 200
	const peligrosidadConMisiles = 100
	const peligrosidadSinMisiles = 0
	
	
	method cargarMisiles() {
		tieneMisiles = true
	}
	
	method descargarMisiles() {
		tieneMisiles = false
	}
	
	method peso() {
		if (tieneMisiles) {
			return pesoConMisiles
		} else {
			return pesoSinMisiles
		}
	}
	
	method peligrosidad() {
		if (tieneMisiles) {
			return peligrosidadConMisiles
		} else {
			return peligrosidadSinMisiles
		}
	}
	
}

object contenedorPortuario {
	
	const tara = 100
	var cosas = #{}
	
	
	method cargar(cosa) {
		cosas.add(cosa)
	}
	
	method descargar(cosa) {
		cosas.remove(cosa)
	}
	
	method peso() {
		return tara + cosas.sum({
			cosa => cosa.peso()
		})
	}
	
	method peligrosidad() {
		if (cosas == #{}) {
			return 0
		} else {
			return cosas.max({
				cosa => cosa.peligrosidad()
			}).peligrosidad()
		}
	}
	
}

object residuosRadioactivos {
	
	var peso
	const peligrosidad = 200
	
	
	method peso(kg) {
		peso = kg
	}
	
	method peso() {
		if (peso == null) {
			error.throwWithMessage(self.kindName() + ": no tiene peso")
		}
		return peso
	}
	
	method peligrosidad() {
		return peligrosidad
	}
	
}

class EmbalajeStandard {
	
	var cosas = #{}
	
	
	method agregarCosa(cosa) {
		if (cosas.size() < 2) {
			cosas.add(cosa)
		} else {
			error.throwWithMessage(self.className() + ": ya contiene 2 cosas")
		}
	}
	
	method peso() {
		return cosas.map({
			cosa => cosa.peso()
		}).sum()
	}
	
	method peligrosidad() {
		return cosas.map({
			cosa => cosa.peligrosidad()
		}).max()
	}
	
}

class EmbalajeLiviano inherits EmbalajeStandard {
	
	override method peligrosidad() {
		if (self.peso() < 200) {
			return 0
		} else {
			return super()
		}
	}
	
}

class EmbalajeBolsaDeAserrin inherits EmbalajeStandard {
	
	var kgDeAserrin = 0
	
	method agregarAserrin(kg) {
		kgDeAserrin += kg
	}
	
	method quitarAserrin(kg) {
		kgDeAserrin -= kg
	}
	
	override method peso() {
		return super() + kgDeAserrin
	}
	
	override method peligrosidad() {
		return super() - (10 * kgDeAserrin)
	}
	
}

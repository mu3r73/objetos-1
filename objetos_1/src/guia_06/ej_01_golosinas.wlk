
object mariano {
	
	var golosinas = #{}
	
	
	method comprar(golosina) {
		golosinas.add(golosina)
	}
	
	method desechar(golosina) {
		golosinas.remove(golosina)
	}
	
	method hayGolosinasSinTACC() {
		return golosinas.any({
			golosina => golosina.esLibreDeGluten()
		})
	}
	
	method golosinaMasCara() {
		return golosinas.max({
			golosina => golosina.precio()
		})
	}
	
	method golosinaCualquiera() {
		return golosinas.anyOne()
	}
	
	method golosinaDeSabor(sabor) {
		return golosinas.find({
			golosina => golosina.sabor() == sabor
		})
	}
	
	method golosinasDeSabor(sabor) {
		return golosinas.filter({
			golosina => (golosina.sabor() == sabor)
		})
	}
	
	method preciosCuidados() {
		return golosinas.all({
			golosina => golosina.precio() <= 10
		})
	}
	
	method sabores() {
		return golosinas.map({
			golosina => golosina.sabor()
		}).asSet()
	}
	
	method probarGolosinas() {
		golosinas.forEach({
			golosina => golosina.recibirMordisco()
		})
	}
	
	method golosinasFaltantes(deseadas) {
		return deseadas.difference(golosinas)
	}
	
	method baniar(golosina) {
		golosinas.remove(golosina)
		golosinas.add(new GolosinaBaniada(golosina))
	}
	
}

object heladera {
	
	var coefHumedad = 0.0	// entre 0 y 1
	
	method coefHumedad(nro) {
		coefHumedad = nro
	}
	
	method coefHumedad() {
		return coefHumedad
	}
	
}

class PastillaTuttiFrutti {
	
	var peso = 5
	var libreDeGluten
	var sabor = "frutilla"
	
	
	constructor(_libreDeGluten) {
		libreDeGluten = _libreDeGluten
	}
	
	method precio() {
		if (libreDeGluten) {
			return 7
		} else {
			return 10
		}
	}
	
	method sabor() {
		return sabor
	}
	
	method peso() {
		return peso
	}
	
	method esLibreDeGluten() {
		return libreDeGluten
	}
	
	method recibirMordisco() {
		if (peso == 0) {
			error.throwWithMessage(self.className() + ": se termino")
		}
		peso -= 1
		if (sabor == "frutilla") {
			sabor = "chocolate"
		} else if (sabor == "chocolate") {
			sabor = "naranja"
		} else {
			sabor = "frutilla"
		}
	}
	
}

class GolosinaBaniada {
	
	const golosinaBase
	var pesoBaniado = 4
	
	
	constructor(golosina) {
		golosinaBase = golosina
	}
	
	method precio() {
		return 2 + golosinaBase.precio()
	}
	
	method sabor() {
		return golosinaBase.sabor()
	}
	
	method peso() {
		return pesoBaniado + golosinaBase.peso()
	}
	
	method esLibreDeGluten() {
		return golosinaBase.esLibreDeGluten()
	}
	
	method recibirMordisco() {
		pesoBaniado = (pesoBaniado - 2).max(0)
		golosinaBase.recibirMordisco()
	}
	
}

class Chocolatin {
	
	const pesoInicial
	var peso
	
	
	constructor(gramos) {
		pesoInicial = gramos
		peso = pesoInicial
	}
	
	method precio() {
		return pesoInicial * 0.5
	}
	
	method sabor() {
		return "chocolate"
	}
	
	method peso() {
		return peso
	}
	
	method esLibreDeGluten() {
		return false
	}
	
	method recibirMordisco() {
		if (self.peso() == 0) {
			error.throwWithMessage(self.className() + ": se termino")
		}
		peso = (peso - 2).max(0)
	}
	
}

class ChocolatinVIP inherits Chocolatin {
	
	constructor(gramos) = super(gramos)
	
	method humedad() {
		return heladera.coefHumedad()
	}
	
	override method peso() {
		return super() * (1 + self.humedad())
	}
	
	override method recibirMordisco() {
		peso -= peso * 0.03
	}
	
}

class ChocolatinPremium inherits ChocolatinVIP {
	
	constructor(gramos) = super(gramos)
	
	override method humedad() {
		return heladera.coefHumedad() / 2
	}
	
}

class Oblea {
	
	var peso = 250
	
	
	method precio() {
		return 5
	}
	
	method sabor() {
		return "vainilla"
	}
	
	method peso() {
		return peso
	}
	
	method esLibreDeGluten() {
		return false
	}
	
	method recibirMordisco() {
		if (peso > 70) {
			peso -= peso * 0.5
		} else {
			peso -= peso * 0.25
		}
	}
	
}

class ObleaCrujiente inherits Oblea {
	
	var mordiscos = 0
	
	override method recibirMordisco() {
		if (peso == 0) {
			error.throwWithMessage(self.className() + ": se termino")
		}
		super()
		if (mordiscos < 3) {
			peso = (peso - 3).max(0)
			mordiscos += 1
		}
	}
	
}

class Chupetin {
	
	var peso = 7
	
	
	method precio() {
		return 2
	}
	
	method sabor() {
		return "naranja"
	}
	
	method peso() {
		return peso
	}
	
	method esLibreDeGluten() {
		return true
	}
	
	method recibirMordisco() {
		if (peso >= 2) {
			peso -= peso * 0.1
		}
	}
	
}

class Caramelo {
	
	var peso = 5
	var sabor	// var y no const por CarameloRelleno
	
	
	constructor() = self("frutilla")
	// mantiene compatibilidad con modelo anterior
	// sabor predeterminado = "frutilla"
	
	constructor(_sabor) {
		if (not #{"frutilla", "naranja", "chocolate"}.contains(_sabor)) {
			error.throwWithMessage(self.className() + ": sabor " + _sabor + " no permitido")
		}
		sabor = _sabor		
	}
	
	method precio() {
		return 1
	}
	
	method sabor() {
		return sabor
	}
	
	method peso() {
		return peso
	}
	
	method esLibreDeGluten() {
		return true
	}
	
	method recibirMordisco() {
		if (peso == 0) {
			error.throwWithMessage(self.className() + ": se termino")
		}
		peso -= 1
	}
	
}

class CarameloRelleno inherits Caramelo {
	
	constructor(_sabor) = super(_sabor)
		
	override method recibirMordisco() {
		super()
		sabor = "chocolate"
	}
	
}

class Alfajor {
	
	var peso = 300
	
	
	method precio() {
		return 12
	}
	
	method sabor() {
		return "chocolate"
	}
	
	method peso() {
		return peso
	}
	
	method esLibreDeGluten() {
		return false
	}
	
	method recibirMordisco() {
		peso -= peso * 0.2
	}
	
}

class Bombon {
	
	var peso = 15
	
	
	method precio() {
		return 5
	}
	
	method sabor() {
		return "frutilla"
	}
	
	method peso() {
		return peso
	}
	
	method esLibreDeGluten() {
		return true
	}
	
	method recibirMordisco() {
		if (peso == 0) {
			error.throwWithMessage(self.className() + ": se termino")
		}
		peso = ((peso * 0.8) - 1).max(0)
	}
	
}

class BombonDuro inherits Bombon {
	
	override method recibirMordisco() {
		peso -= peso * 0.01
	}
	
}

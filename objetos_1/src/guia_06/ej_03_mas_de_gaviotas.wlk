
object roque {
	
	var aves = #{}
	
	
	method agregarParaEntrenar(ave) {
		aves.add(ave)
	}
	
	method dejarDeEntrenar(ave) {
		aves.remove(ave)
	}
	
	method entrenar(ave) {
		ave.volar(10)
		ave.comer(300)
		ave.volar(10)
		ave.haceLoQueQuieras()
	}
	
	method entrenar() {
		aves.forEach({
			ave => self.entrenar(ave)
		})
	}
	
	method avesCapacesDeVolar(km) {
		aves.filter({
			ave => ave.puedeVolar(km)
		})
	}
	
	method aQuienesEntrenas() {
		return aves
	}
	
}

class Golondrina {
	
	var energia = 0
	
	
	method comer(gramos) {
		energia += gramos * 4
	}
	
	method energiaParaVolar(km) {
		return 10 + km
	}
	
	method volar(km) {
		if (energia < self.energiaParaVolar(km)) {
			error.throwWithMessage(self.className() + ": energia insuficiente para volar " + km + " km")
		}
		energia -= self.energiaParaVolar(km)
	}
	
	method energia() {
		return energia
	}
	
	method estaDebil() {
		return (energia < 50)
	}
	
	method estaFeliz() {
		return ((energia >= 500) and (energia <= 1000))
	}
	
	method cuantoQuiereVolar() {
		var cuanto = energia / 5
		if ((energia >= 300) and (energia <= 400)) {
			cuanto += 10
		}
		if (energia % 20 == 0) {
			cuanto += 15
		}
		return cuanto
	}
	
	method puedeVolar(km) {
		return energia >= self.energiaParaVolar(km)
	}
	
	method haceLoQueQuieras() {
		if (self.estaDebil()) {
			self.comer(20)
		}
		if (self.estaFeliz()) {
			self.volar(self.cuantoQuiereVolar())
		}
	}
	
}

class GolondrinaAndina inherits Golondrina {
	
	override method comer(gramos) {
		super(gramos)
		self.volar(1)
	}
	
}

class GolondrinaParda inherits Golondrina {
	
	var ultimaAccion
	
	constructor() {
		ultimaAccion = "volar"
	}
	
	override method comer(gramos) {
		super(gramos)
		ultimaAccion = "comer"
	}
	
	override method volar(km) {
		super(km)
		ultimaAccion = "volar"
	}
	
	override method haceLoQueQuieras() {
		if (ultimaAccion == "comer") {
			self.volar(5)
		} else { // es "volar"
			self.comer(10)
		}
	}
	
}

class Gorrion {
	
	var energia = 0
	
	
	method comer(gramos) {
		energia += gramos * 2
	}
	
	method energiaParaVolar(km) {
		return 1 + 0.5 * km
	}
	
	method volar(km) {
		if (energia < self.energiaParaVolar(km)) {
			error.throwWithMessage(self.className() + ": energia insuficiente para volar " + km + " km")
		}
		energia -= self.energiaParaVolar(km)
	}
	
	method energia() {
		return energia
	}
	
	method estaDebil() {
		return (energia < 50)
	}
	
	method estaFeliz() {
		return ((energia >= 500) and (energia <= 1000))
	}
	
	method cuantoQuiereVolar() {
		return 1
	}

	method puedeVolar(km) {
		return energia >= self.energiaParaVolar(km)
	}
	
	method haceLoQueQuieras() {
		self.volar(self.cuantoQuiereVolar())
	}
	
}

class Paloma {
	
	var calorias = 0
	var kilometros = 0
	
	
	method comer(gramos) {
		// 1 kcaloria = 4184 joules
		// 1000 calorias = 4184 joules
		calorias += 4184 * gramos * 4 / 1000
	}
	
	method volar(km) {
		kilometros += km
	}
	
	method kmsRecorridos() {
		return kilometros
	}
	
	method caloriasIngeridas() {
		return calorias
	}
	
	method puedeVolar(km) {
		return true
	}
	
	method haceLoQueQuieras() {
		// no hace nada
	}
	
}

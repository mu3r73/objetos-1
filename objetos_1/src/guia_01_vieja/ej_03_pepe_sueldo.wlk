
object pepe {
	
	var faltas = 0
	var categoria = cadete
	var bonoPresentismo = bonoPresentismo1
	var bonoResultados = bonoResultados1
	
	
	method faltas(nuevasFaltas) {
		faltas = nuevasFaltas	
	}
	
	method categoria(nuevaCategoria) {
		categoria = nuevaCategoria
	}
	
	method bonoPresentismo(nuevoBonoPresentismo) {
		bonoPresentismo = nuevoBonoPresentismo
	}
	
	method bonoResultados(nuevoBonoResultados) {
		bonoResultados = nuevoBonoResultados
	}
	
	method sueldo() {
		return categoria.neto() + bonoPresentismo.bono(faltas) + bonoResultados.bono(categoria.neto())
	}
	
}

object gerente {
	
	method neto() {
		return 1000
	}
	
}

object cadete {
	
	method neto() {
		return 1500
	}
	
}

object bonoPresentismo1 {
	
	method bono(faltas) {
		if (faltas == 0) {
			return 100
		} else if (faltas == 1) {
			return 50
		} else {
			return 0
		}
	}
	
}

object bonoPresentismo2 {
	
	method bono(faltas) {
		return 0
	}
	
}


object bonoResultados1 {
	
	method bono(neto) {
		return neto * 10 / 100
	}
	
} 

object bonoResultados2 {
	
	method bono(neto) {
		return 80
	}
	
}

object bonoResultados3 {
	
	method bono(neto) {
		return 0
	}
	
}

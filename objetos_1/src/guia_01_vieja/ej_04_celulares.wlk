
object juliana {
	
	var celular = motorolaU9
	var servicio = servicioPersonal
	
	
	method llamar(persona, minutos) {
		if ((not celular.estaApagado()) and (not persona.celular().estaApagado())) {
			persona.recibirLlamada(minutos)
			celular.usar(minutos)
			servicio.llamar(minutos)
		}
	}
	
	method recibirLlamada(minutos) {
		if (not celular.estaApagado()) {
			celular.usar(minutos)
		}
	}
	
	
  	method deudaPorCelular() {
  		servicio.deuda()
  	}
	
	method celular() {
		return celular
	}
	
	method celular(nuevoCelular) {
		celular = nuevoCelular
	}
	
	method servicio() {
		return servicio
	}
	
	method servicio(nuevoServicio) {
		servicio = nuevoServicio
	}
	
}

object catalina {
	
	var celular = iPhone
	var servicio = servicioMovistar
	
	
	method llamar(persona, minutos) {
		if ((not celular.estaApagado()) and (not persona.celular().estaApagado())) {
			persona.recibirLlamada(minutos)
			celular.usar(minutos)
			servicio.llamar(minutos)
		}
	}
	
	method recibirLlamada(minutos) {
		if (not celular.estaApagado()) {
			celular.usar(minutos)
		}
	}
	
  	method deudaPorCelular() {
  		servicio.deuda()
  	}
	
	method celular() {
		return celular
	}	
	
	method celular(nuevoCelular) {
		celular = nuevoCelular
	}
	
	method servicio() {
		return servicio
	}
	
	method servicio(nuevoServicio) {
		servicio = nuevoServicio
	}
	
}

object motorolaU9 {
	
	var bateria = bateriaMotorolaU9
	
	
	method usar(minutos) {
		bateria.descargar(self.usoBateriaPorLlamada(minutos))
	}
	
	method usoBateriaPorLlamada(minutos) {
		return 0.25
	}

	method bateria() {
		return bateria
	}
	
	method estaApagado() {
		return bateria.carga() == 0
	}
	
}

object iPhone {
	
	var bateria = bateriaIPhone
	
	
	method usar(minutos) {
		bateria.descargar(self.usoBateriaPorLlamada(minutos))
	}
	
	method usoBateriaPorLlamada(minutos) {
		return minutos * 0.1 / 100
	}

	method bateria() {
		return bateria
	}
	
	method estaApagado() {
		return bateria.carga() == 0
	}
	
}

object bateriaMotorolaU9 {

	var carga = 5
	
	
	method descargar(cuanto) {
		carga -= cuanto
		if (carga < 0) {
			carga = 0
		}
	}
	
	method carga() {
		return carga
	}
	
	method recargar() {
		carga = 5
	}
	
}

object bateriaIPhone {

	var carga = 5
	
	
	method descargar(cuanto) {
		carga -= cuanto
		if (carga < 0) {
			carga = 0
		}
	}
	
	method carga() {
		return carga
	}
	
	method recargar() {
		carga = 5
	}
	
}

object servicioMovistar {
	
	var deuda = 0
	
	
	method llamar(minutos) {
		deuda += self.costoLlamada(minutos)	
	}
	
	method deuda() {
		return deuda
	}
	
	method costoLlamada(minutos) {
		return minutos * 0.60
	}
	
}

object servicioClaro {
	
	var deuda = 0
	
	
	method llamar(minutos) {
		deuda += self.costoLlamada(minutos)	
	}
	
	method deuda() {
		return deuda
	}	
	
	method costoLlamada(minutos) {
		return 0.5 * minutos * 1.21
	}
	
}

object servicioPersonal {
	
	var deuda = 0
	
	
	method llamar(minutos) {
		deuda += self.costoLlamada(minutos)	
	}
	
	method deuda() {
		return deuda
	}
	
	method costoLlamada(minutos) {
		var costo = 0.70
		if (minutos > 10) {
			costo += (minutos - 10) * 0.40
		}
		return costo
	}
	
}

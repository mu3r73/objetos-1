object persona {
	
	var ratonesComidos = 0
	var tiempoCorrido = 0
	
	
	method tomComioRaton() {
		ratonesComidos += 1
	}
	
	method tomCorrio(segundos) {
		tiempoCorrido += segundos
	}
	
	method ratonesComidos() {
		return ratonesComidos
	}
	
	method tiempoCorrido() {
		return tiempoCorrido
	}
	
}

object tom {
	
	var energia = 100
	
	
	method comer(raton) {
		energia += self.energiaPorComer(raton.peso())
		persona.tomComioRaton()
	}
	
	method energiaPorComer(gramos) {
		return 12 + gramos
	}
	
	method energia() {
		return energia
	}
	
	method velocidad() {
		return 5 + (energia / 10.0)
	}
	
	method energiaParaCorrer(distancia) {
		return 0.5 * distancia
	}
	
	method correr(segundos) {
		energia -= self.energiaParaCorrer(self.velocidad() * segundos)
		if (energia < 0) {
			energia = 0
		}
		persona.tomCorrio(segundos)
	}
	
	method meConvieneComerA(raton, distancia) {
		return self.energiaPorComer(raton.peso()) > self.energiaParaCorrer(distancia)
	}
	
}

object raton {
	
	var peso = 250
	
	
	method peso(gramos) {
		peso = gramos
	}
	
	method peso() {
		return peso
	}
	
}


// paises

class Pais {
	
	const nombre
	var moneda	// string
	var poblacion
	var superficie	// en km^2
	var capital	// ciudad
	
	constructor(_nombre, _moneda, _poblacion, _superficie, _capital) {
		nombre = _nombre
		self.moneda(_moneda)
		self.poblacion(_poblacion)
		self.superficie(_superficie)
		self.capital(_capital)
	}
	
	method moneda(string) {
		moneda = string
	}
	
	method moneda() {
		return moneda
	}
	
	method poblacion(cantHab) {
		poblacion = cantHab
	}
	
	method poblacion() {
		return poblacion
	}
	
	method superficie(cantKm2) {
		superficie = cantKm2
	}
	
	method superficie() {
		return superficie
	}
	
	method capital(ciudad) {
		capital = ciudad
	}
	
	method capital() {
		return capital
	}
	
	method densidad() {
		return poblacion / superficie
	}
		
}

// ciudades

class Ciudad {
	
	var pais
	var diasDeSolPorAnio
	var autosPorDia
	var barrios = #{}
	
	constructor(_pais, _diasDeSolPorAnio) {
		self.pais(_pais)
		self.diasDeSolPorAnio(_diasDeSolPorAnio)
	}
	
	method pais(_pais) {
		pais = _pais
	}
	
	method pais() {
		return pais
	}
	
	method diasDeSolPorAnio(cant) {
		diasDeSolPorAnio = cant
	}
	
	method diasDeSolPorAnio() {
		return diasDeSolPorAnio
	}
	
	method autosPorDia(cant) {
		autosPorDia = cant
	}
	
	method autosPorDia() {
		return autosPorDia
	}
	
	method agregarBarrio(barrio) {
		barrios.add(barrio)
	}
	
	method moneda() {
		return pais.moneda()
	}
	
	method esEnorme() {
		return self.poblacion() > 2000000
	}
	
	method autosPorSemana() {
		return autosPorDia / 7
	}
	
	method esAlegre() {
		return self.poblacion().between(200000, 1000000)
			and diasDeSolPorAnio >= 200
			and not self.tieneProblemasDeTransito()
	}
	
	method tieneProblemasDeTransito() {
		return self.autosPorDiaPorKm2() > indicadores.umbralAutosPorDiaPorKm2()
	}
	
	method autosPorDiaPorKm2() {
		return autosPorDia / self.superficie()
	}
	
	method horasProductivasPorDiaLaboral() {
		return self.poblacionActiva() * 7
	}
	
	method poblacionActiva() {
		return self.poblacion() / 3
	}
	
	method poblacion() {
		return barrios.map({
			barrio => barrio.poblacion()
		}).sum()
	}
	
	method superficie() {
		return barrios.map({
			barrio => barrio.superficie()
		}).sum()
	}
	
	method estaBienOrganizada() {
		return barrios.all({
			barrio => barrio.poblacion() <= self.poblacionPromedioPorBarrio()
		})
		
	}
	
	method poblacionPromedioPorBarrio() {
		return self.poblacion() / barrios.size() 
	}
	
	method conoceA(empresa) {
		return empresa.estaEstablecidaEn(self)
			or empresa.estaEstablecidaEn(pais.capital())
	}
	
}

class CiudadUltraconectada inherits Ciudad {
	
	constructor(_pais, _diasDeSolPorAnio) = super(_pais, _diasDeSolPorAnio)
	
	override method tieneProblemasDeTransito() {
		return false
	}
	
	override method poblacionActiva() {
		return super() * 1.2
	}
	
}

// barrios

class Barrio {
	
	var poblacion
	var superficie
	
	constructor(_poblacion, _superficie) {
		self.poblacion(_poblacion)
		self.superficie(_superficie)
	}
	
	method poblacion(cantHab) {
		poblacion = cantHab
	}
	
	method poblacion() {
		return poblacion
	}
	
	method superficie(cantKm2) {
		superficie = cantKm2
	}
	
	method superficie() {
		return superficie
	}
	
}

// empresa

class Empresa {
	
	var ciudades = #{}
	
	method establecerseEn(ciudad) {
		ciudades.add(ciudad)
	}
	
	method estaEstablecidaEn(ciudad) {
		return ciudades.contains(ciudad)
	}
	
}

// objetos bien conocidos

object indicadores {
	
	var umbralAutosPorDiaPorKm2 = 400
	
	method umbralAutosPorDiaPorKm2(cant) {
		umbralAutosPorDiaPorKm2 = cant
	}
	
	method umbralAutosPorDiaPorKm2() {
		return umbralAutosPorDiaPorKm2
	}
	
}


// red

class Red {
	
	var administradores = #{}
	
	method agregarAdministrador(admin) {
		administradores.add(admin)
		admin.red(self)
	}
	
	method quitarAdministrador(admin) {
		administradores.remove(admin)
		admin.red(null)
	}
	
	method puedeAceptar(documento) {
		return administradores.any({
			admin => admin.puedeAceptar(documento)
		})
	}
	
	method aceptar(documento) {
		if (not self.puedeAceptar(documento)) {
			throw new ImpresionException("el documento no se puede aceptar para imprimir en esta red")
		}
		self.quienPuedeImprimir(documento).aceptar(documento)
	}
	
	method quienPuedeImprimir(documento) {
		return administradores.anyOne({
			admin => admin.puedeAceptar(documento)
		})
	}
	
	method pesoTotalPendientes() {
		return administradores.map({
			admin => admin.pesoTotalPendientes()
		}).sum()
	}
	
	method dispositivosDisponibles() {
		return administradores.map({
			admin => admin.dispositivosDisponibles()
		}).flatten()
	}
	
	method docQueSeEstanImprimiendo() {
		return administradores.map({
			admin => admin.docQueSeEstanImprimiendo()
		}).flatten()
	}
	
	method terminoDocumento() {
		// w/e
	}
	
}


// administradores de impresión

class Administrador {
	// abstracta
	
	var red
	
	method red(_red) {
		red = _red
	}
	
	method puedeAceptar(documento)
	// abstracto
	
	method aceptar(documento)
	// abstracto
	
	method pesoTotalPendiente()
	// abstracto
	
	method dispositivosDisponibles()
	// abstracto
	
	method docQueSeEstanImprimiendo()
	// abstracto
	
	method terminoDocumento()
	// abstracto
	
}

class AdminInterno inherits Administrador {
	
	var impresoras = #{}
	
	method agregarImpresora(impresora) {
		impresoras.add(impresora)
		impresora.admin(self)
	}
	
	method quitarImpresora(impresora) {
		impresoras.remove(impresora)
		impresora.admin(null)
	}
	
	override method puedeAceptar(documento) {
		return impresoras.any({
			impresora => impresora.puedeAceptar(documento)
		})
	}
	
	override method aceptar(documento) {
		self.quienPuedeAceptar(documento).aceptar(documento)
	}
	
	method quienPuedeAceptar(documento) {
		return impresoras.anyOne({
			impresora => impresora.puedeAceptar(documento)
		})
	}
	
	override method pesoTotalPendiente() {
		return impresoras.map({
			impresora => impresora.pesoTotalCola()
		}).sum()
	}
	
	override method dispositivosDisponibles() {
		return impresoras.filter({
			impresora => impresora.estaDisponible()
		})
	}
	
	override method docQueSeEstanImprimiendo() {
		return self.impresorasConDocsEnCola().map({
			impresora => impresora.queEstasImprimiendo()
		})
	}
	
	method impresorasConDocsEnCola() {
		return impresoras.difference(self.dispositivosDisponibles())
	}
	
	override method terminoDocumento() {
		red.terminoDocumento()
	}
	
}

class AdminExterno inherits Administrador {
	
	var docPendientes = #{}
	
	override method puedeAceptar(documento) {
		return documento.cantPaginas() >= indicadores.minPagAdminExt()
	}
	
	override method aceptar(documento) {
		docPendientes.add(documento)
	}
	
	override method pesoTotalPendiente() {
		return docPendientes.map({
			documento => documento.peso()
		}).sum()
	}
	
	override method dispositivosDisponibles() {
		if (self.estaDisponible()) {
			return [self]
		} else {
			return []
		}
	}
	
	method estaDisponible() {
		return docPendientes.isEmpty()
	}
	
	override method docQueSeEstanImprimiendo() {
		if (not self.estaDisponible()) {
			return [self.docPendienteMasPesado()]
		} else {
			return []
		}
	}
	
	method docPendienteMasPesado() {
		return docPendientes.max({
			documento => documento.peso()
		})
	}
	
	override method terminoDocumento() {
		docPendientes.remove(self.docPendienteMasPesado())
		self.notificarPorEmail()
	}
	
	method notificarPorEmail() {
		red.terminoDocumento()
	}
	
}

// impresoras

class Impresora {
	
	var capacidad	// en kb
	var admin
	var cola = []
	
	constructor(_capacidad) {
		capacidad = _capacidad
	}
	
	method admin(_admin) {
		admin = _admin
	}
	
	method agregarACola(documento) {
		cola.add(documento)
	}
	
	method puedeAceptar(documento) {
		return self.pesoTotalCola() + documento.peso() <= capacidad
	}
	
	method pesoTotalCola() {
		return cola.map({
			documento => documento.peso()
		}).sum()
	}
	
	method aceptar(documento) {
		cola.add(documento)
	}
	
	method estaDisponible() {
		return cola.isEmpty()
	}
	
	method queEstasImprimiendo() {
		return cola.first()
	}
	
	method terminoDocumento() {
		cola.remove(cola.first())
		admin.terminoDocumento()
	}
	
}

class ImpChorroDeTinta inherits Impresora {
	
	const maxPaginas
	
	constructor(_capacidad, _maxPaginas) = super(_capacidad) {
		maxPaginas = _maxPaginas
	}
	
	override method puedeAceptar(documento) {
		return super(documento)
			and documento.cantPaginas() <= maxPaginas
	}
	
}

class ImpAltaCarga inherits Impresora {
	
	constructor(_capacidad) = super(_capacidad)
	
	override method puedeAceptar(documento) {
		return super(documento)
			and documento.peso() >= 300
	}
	
}

// documentos

class Documento {
	// abstracta
	
	method peso()
	// abstracto
	
	method cantPaginas()
	// abstracto
	
}

class DocTexto inherits Documento {
	
	const cantPaginas
	const pesoPagina = 50	// en kb
	
	constructor(_cantPaginas) {
		cantPaginas = _cantPaginas
	}
	
	override method peso() {
		return cantPaginas * pesoPagina 
	}
	
	override method cantPaginas() {
		return cantPaginas
	}
	
}

class DocPaqueteDeImagenes inherits Documento {
	
	const imagenes
	
	constructor(_imagenes) {
		imagenes = _imagenes
	}
	
	override method peso() {
		return imagenes.map({
			imagen => imagen.peso()
		}).sum()
	}
	
	override method cantPaginas() {
		return imagenes.map({
			imagen => imagen.cantPaginas()
		}).sum()
	}
	
}

class Imagen {
	
	const peso	// en kb
	
	constructor(_peso) {
		peso = _peso
	}
	
	method peso() {
		return peso
	}
	
	method cantPaginas() {
		if (peso >= 100) {
			return 1
		} else {
			return 0.5
		}
	}
	
}

class DocPlanillaDeCalculo inherits Documento {
	
	const filas
	const columnas
	const pesoCelda = 0.25	// en kb
	
	constructor(_filas, _columnas) {
		filas = _filas
		columnas = _columnas
	}
	
	method cantCeldas() {
		return filas * columnas
	}
	
	override method peso() {
		return self.cantCeldas() * pesoCelda
	}
	
	override method cantPaginas() {
		return filas / self.filasPorPagina()
	}
	
	method filasPorPagina() {
		if (columnas > 15) {
			return 20
		} else {
			return 40
		}
	}
	
}

// objetos bien conocidos

object indicadores {
	
	var minPagAdminExt = 50
	
	method minPagAdminExt(paginas) {
		minPagAdminExt = paginas
	}
	
	method minPagAdminExt() {
		return minPagAdminExt
	}
	
}

// excepciones

class ImpresionException inherits Exception {
	
	constructor(_message) = super(_message)
	
}

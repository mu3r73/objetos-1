
// ligas

class Liga {
	
	var equipos = #{}
	var representantes = #{}
	
	method agregarEquipo(equipo) {
		equipos.add(equipo)
		equipo.liga(self)
	}
	
	method quitarEquipo(equipo) {
		equipos.remove(equipo)
		equipo.liga(null)
	}
	
	method agregarRepresentante(representante) {
		representantes.add(representante)
		representante.liga(self)
	}
	
	method quitarRepresentante(representante) {
		representantes.remove(representante)
		representante.liga(null)
	}
	
	method equipoConMasVision() {
		return equipos.max({
			equipo => equipo.vision()
		})
	}
	
	method equiposInteresadosEn(jugador) {
		return equipos.filter({
			equipo => equipo.estaInteresadoEn(jugador)
		})
	}
	
}

// entidades
// (pueden ser equipos o representantes)

class Entidad {
	// clase abstracta
	
	var liga
	var jugadores = #{}
	
	method agregarJugador(jugador) {
		jugadores.add(jugador)
		jugador.duenioPase(self)
	}
	
	method quitarJugador(jugador) {
		jugadores.remove(jugador)
		jugador.duenioPase(null)
	}
	
	method jugadores() {
		return jugadores
	}
	
	method liga(_liga) {
		liga = _liga
	}
	
	method liga() {
		return liga
	}
	
	method estaInteresadoEn(jugador)
	// abstracto
	
	method prefiereDescartarA(jugador)
	// abstracto
	
	method estaInteresadoEn2oMasJugadoresDe(entidad)
	// abstracto
	
	method jugadoresQueLeInteresanDe(ljugadores) {
		return ljugadores.filter({
			jugador => self.estaInteresadoEn(jugador)
		})
	}
	
	method jugadoresQuePrefiereDescartarDe(ljugadores) {
		return ljugadores.filter({
			jugador => self.prefiereDescartarA(jugador)
		})
	}
	
}

// equipos

class Equipo inherits Entidad {
	// abstracta
	
	method potencia() {
		return self.potenciaMaximaEntre(jugadores)
	}
	
	method potenciaSumaDe2MasPotentes() {
		return self.potenciaMaximaEntre(jugadores)
			+ self.potenciaMaximaEntre(jugadores.difference(#{self.jugadorMasPotente()}))
	}
	
	method potenciaMaximaEntre(_jugadores) {
		return jugadores.map({
			jugador => jugador.potencia()
		}).max()
	}
	
	method jugadorMasPotente() {
		return jugadores.max({
			jugador => jugador.potencia()
		})
	}
	
	method tieneMasDe3JugadoresMasPotentesQue(jugador) {
		return self.jugadoresMasPotentesQue(jugador).size() > 3
	}
	
	method jugadoresMasPotentesQue(_jugador) {
		return jugadores.filter({
			jugador => jugador.potencia() > _jugador.potencia()
		})
	}
	
	method precision() {
		return jugadores.map({
			jugador => jugador.precision()
		}).sum()
	}
	
	method precisionPromedio() {
		return self.precision() / jugadores.size()
	}
	
	method vision() {
		return jugadores.map({
			jugador => jugador.vision()
		}).sum()
	}
	
	method visionSin(_jugador) {
		return jugadores.difference({_jugador}).map({
			jugador => jugador.vision()
		}).sum()
	}
	
	method puedeHacerTratosCon(representante) {
		return representante.estaInteresadoEn2oMasJugadoresDe(self)
			and self.estaInteresadoEn2oMasJugadoresDe(representante)
	}
	
	override method estaInteresadoEn2oMasJugadoresDe(representante) {
		return self.jugadoresQueLeInteresanDe(representante.jugadores()).size() >= 2
	}
	
	method estaEquilibrado() {
		return self.estaInteresadoEnAlMenos5Jugadores()
			and self.prefiereDescartarNoMasDe2Jugadores()
	}
	
	method estaInteresadoEnAlMenos5Jugadores() {
		return self.jugadoresQueLeInteresanDe(jugadores).size() >= 5
	}
	
	method prefiereDescartarNoMasDe2Jugadores() {
		return self.jugadoresQuePrefiereDescartarDe(jugadores).size() <= 2
	}
	
}

class EquipoLirico inherits Equipo {
	
	override method estaInteresadoEn(jugador) {
		return jugador.precision() >= 2 + self.precisionPromedio()
	}
	
	override method prefiereDescartarA(jugador) {
		return jugador.precision() + jugador.vision() <= 5 
	}
	
}

class EquipoRustico inherits Equipo {
	
	override method estaInteresadoEn(jugador) {
		return not self.tieneMasDe3JugadoresMasPotentesQue(jugador)
	}
	
	override method prefiereDescartarA(jugador) {
		return jugador.habilidadPases() > jugador.potencia()
	}
	
}

class EquipoOrganizado inherits Equipo {
	
	override method estaInteresadoEn(jugador) {
		return jugador.cantAtributosQueCumplen({
			valor => valor >= 8
		}) >= 2
	}
	
	override method prefiereDescartarA(jugador) {
		return jugador.cantAtributosQueCumplen({
			valor => valor <= 5
		}) >= 2
	}
	
}

// jugadores

class Jugador {
	// abstracta
	
	var visionJuego
	var visionCompanieros
	var potencia
	var habilidadPases
	var equipo
	var duenioPase
	
	constructor(_visionJuego, _visionCompanieros, _potencia, _habilidadPases) {
		self.visionJuego(_visionJuego)
		self.visionCompanieros(_visionCompanieros)
		self.potencia(_potencia)
	}
	
	method visionJuego(_visionJuego) {
		visionJuego = _visionJuego
	}
	
	method visionCompanieros(_visionCompanieros) {
		visionCompanieros = _visionCompanieros
	}
	
	method potencia(_potencia) {
		potencia = _potencia
	}
	
	method potencia() {
		return potencia
	}
	
	method habilidadPases(_habilidadPases) {
		habilidadPases = _habilidadPases
	}
	
	method habilidadPases() {
		return habilidadPases
	}
	
	method equipo(_equipo) {
		equipo = _equipo
	}
	
	method duenioPase(_duenioPase) {
		duenioPase = _duenioPase
	}
	
	method precision() {
		return 3 * self.valorIntrinseco() + habilidadPases
	}
	
	method valorIntrinseco()
	// abstracto
	
	method vision()
	// abstracto
	
	method cantAtributosQueCumplen(closure) {
		return self.atributosQueCumplen(closure).size()
	}
	
	method atributosQueCumplen(closure) {
		return self.atributos().filter(closure)
	}
	
	method atributos() {
		return #{self.valorIntrinseco(), self.habilidadPases(), self.vision()}
	}
	
	method estaEnRiesgoLaboral() {
		return duenioPase.prefiereDescartarA(self)
	}
	
	method destinosPosiblesEnLiga() {
		return duenioPase.liga().equiposInteresadosEn(self)
	}
	
}

class Defensor inherits Jugador {
	
	var quite
	
	constructor(_visionJuego, _visionCompanieros, _potencia, _habilidadPases, _quite) = super(_visionJuego, _visionCompanieros, _potencia, _habilidadPases) {
		self.quite(_quite)
	}
	
	method quite(_quite) {
		quite = _quite
	}
	
	override method valorIntrinseco() {
		return quite
	}
	
	override method vision() {
		return self.visionCompanieros() + visionJuego
	}
	
	method visionCompanieros() {
		return equipo.visionSinJugador(self)
	}
	
}

class Delantero inherits Jugador {
	
	var anotacion
	
	constructor(_visionJuego, _visionCompanieros, _potencia, _habilidadPases, _anotacion) = super(_visionJuego, _visionCompanieros, _potencia, _habilidadPases) {
		self.anotacion(_anotacion)
	}
	
	method anotacion(_anotacion) {
		anotacion = _anotacion
	}
	
	override method valorIntrinseco() {
		return anotacion
	}
	
	override method vision() {
		return visionJuego + habilidadPases
	}
	
}

// representantes

class Representante inherits Entidad {
	
	var pedidos = #{}
	
	method agregarPedido(pedido) {
		pedidos.add(pedido)
	}
	
	method quitarPedido(pedido) {
		pedidos.remove(pedido)
	}
	
	override method estaInteresadoEn(jugador) {
		return pedidos.any({
			pedido => pedido.esSatisfechoPor(jugador)
		})
	}
	
	override method prefiereDescartarA(jugador) {
		return not self.estaInteresadoEn(jugador)
	}
	
	override method estaInteresadoEn2oMasJugadoresDe(equipo) {
		return equipo.jugadores().filter({
			jugador => self.estaInteresadoEn(jugador)
		}).size() >= 2
	}
	
}

// pedidos

class Pedido {
	// abstracta
	
	method esSatisfechoPor(jugador)
	// abstracto
	
}

class PedidoPorPotencia inherits Pedido {
	
	const nivelMinimo
	
	constructor(_nivelMinimo) {
		nivelMinimo = _nivelMinimo
	}
	
	override method esSatisfechoPor(jugador) {
		return jugador.potencia() >= nivelMinimo
	}
	
}

class PedidoPorVision inherits Pedido {
	
	const nivelMinimo
	
	constructor(_nivelMinimo) {
		nivelMinimo = _nivelMinimo
	}
	
	override method esSatisfechoPor(jugador) {
		return jugador.vision() >= nivelMinimo
	}
	
}

class PedidoCombinado inherits Pedido {
	
	const valorMin
	const valorMax
	
	constructor(_valorMin, _valorMax) {
		valorMin = _valorMin
		valorMax = _valorMax
	}
	
	override method esSatisfechoPor(jugador) {
		return (jugador.habilidadPases() + jugador.precision() + jugador.vision()).between(valorMin, valorMax)
	}
	
}


object mariano {
	
	var golosinas = #{}
	
	
	method comprar(golosina) {
		golosinas.add(golosina)
	}
	
	method desechar(golosina) {
		golosinas.remove(golosina)
	}
	
	method hayGolosinasSinTACC() {
		return golosinas.any({
			golosina => golosina.esLibreDeGluten()
		})
	}
	
	method golosinaCualquiera() {
		if (golosinas.isEmpty()) {
			error.throwWithMessage("no hay golosinas")
		}
		return golosinas.anyOne()
	}
	
	method golosinaDeSabor(sabor) {
		return golosinas.find({
			golosina => golosina.sabor() == sabor
		})
	}
	
}

object caramelo {
	
	var peso = 5
	
	
	method precio() {
		return 1
	}
	
	method sabor() {
		return "frutilla"
	}
	
	method peso() {
		return peso
	}
	
	method esLibreDeGluten() {
		return true
	}
	
	method recibirMordisco() {
		if (peso == 0) {
			error.throwWithMessage("se termino el caramelo")
		}
		peso -= 1
	}
	
}

object alfajor {
	
	var peso = 300
	
	
	method precio() {
		return 12
	}
	
	method sabor() {
		return "chocolate"
	}
	
	method peso() {
		return peso
	}
	
	method esLibreDeGluten() {
		return false
	}
	
	method recibirMordisco() {
		// como siempre se consume un porcentaje del peso que queda, nunca llega a 0
		// duda: deberia definir un margen proximo a 0 como fin?
		peso -= peso * 0.2
	}
	
}

object bombon {
	
	var peso = 15
	
	
	method precio() {
		return 5
	}
	
	method sabor() {
		return "frutilla"
	}
	
	method peso() {
		return peso
	}
	
	method esLibreDeGluten() {
		return true
	}
	
	method recibirMordisco() {
		if (peso == 0) {
			error.throwWithMessage("se termino el bombon")
		}
		peso = ((peso * 0.8) - 1).max(0)
	}
	
}

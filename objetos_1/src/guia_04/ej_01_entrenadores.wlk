
object instituto {
	
	var entrenadores = #{}
	
	method contratar(entrenador) {
		entrenadores.add(entrenador)
	}
	
	method prescindir(entrenador) {
		entrenadores.remove(entrenador)
	}
	
	method entrenamientoGeneral() {
		entrenadores.forEach({
			entrenador => entrenador.entrenar()
		})
	}
	
	method buenAmbiente() {
		return entrenadores.all({
			entrenador => entrenador.estaContentx()
		})
	}
	
	method mejoresEstudiantes() {
		var res = #{}
		entrenadores.forEach({
			entrenador => res.add(entrenador.mejorEstudiante())			
		})
		return res
	}
	
}


object susana {
	
	var ave
	
	method entrenar() {
		alpiste.peso(100)
		ave.comer(alpiste)
		ave.volar(20)
	}
	
	method aQuienEntrenas() {
		return ave
	}
	
	method entrenarA(nuevaAve) {
		ave = nuevaAve
	}
	
	method estaContentx() {
		return (ave.puedeVolar(5))
	}
	
	method mejorEstudiante() {
		if (ave == null) {
			error.throwWithMessage("Susana no tiene ave para entrenar")
		}
		return ave
	}
	
}

object roque {
	
	var aves = #{}
	
	method agregarParaEntrenar(ave) {
		aves.add(ave)
	}
	
	method dejarDeEntrenar(ave) {
		aves.remove(ave)
	}
	
	method entrenar(ave) {
		ave.volar(10)
		alpiste.peso(300)
		ave.comer(alpiste)
		ave.volar(10)
		ave.haceLoQueQuieras()
	}
	
	method entrenar() {
		aves.forEach({
			ave => self.entrenar(ave)
		})
	}
	
	method avesCapacesDeVolar(km) {
		aves.filter({
			ave => ave.puedeVolar(km)
		})
	}
	
	method aQuienesEntrenas() {
		return aves
	}
	
	method estaContentx() {
		return (aves.size() > 1)
			and (aves.size() < 8)
	}
	
	method mejorEstudiante() {
		return aves.find({
			ave => ave.puedeVolar(10)
		})
	}
	
}

object pepita {
	
	var energia = 0
	
	
	method comer(que) {
		energia += que.energia()
	}
	
	method energiaParaVolar(km) {
		return 10 + km
	}
	
	method volar(km) {
		if (energia < self.energiaParaVolar(km)) {
			error.throwWithMessage("a Pepita no le alcanza la energia para volar " + km + " km")
		}
		energia -= self.energiaParaVolar(km)
	}
	
	method energia() {
		return energia
	}
	
	method estaDebil() {
		return (energia < 50)
	}
	
	method estaFeliz() {
		return ((energia >= 500) and (energia <= 1000))
	}
	
	method cuantoQuiereVolar() {
		var cuanto = energia / 5
		if ((energia >= 300) and (energia <= 400)) {
			cuanto += 10
		}
		if (energia % 20 == 0) {
			cuanto += 15
		}
		return cuanto
	}
	
	method puedeVolar(km) {
		return energia >= self.energiaParaVolar(km)
	}
	
	method haceLoQueQuieras() {
		if (self.estaDebil()) {
			alpiste.peso(20)
			self.comer(alpiste)
		}
		if (self.estaFeliz()) {
			self.volar(self.cuantoQuiereVolar())
		}
	}
	
}

object pepon {
	
	var energia = 0
	
	
	method comer(que) {
		energia += que.energia() / 2.0
	}
	
	method energiaParaVolar(km) {
		return 1 + 0.5 * km
	}
	
	method volar(km) {
		if (energia < self.energiaParaVolar(km)) {
			error.throwWithMessage("a Pepon no le alcanza la energia para volar " + km + " km")
		}
		energia -= self.energiaParaVolar(km)
	}
	
	method energia() {
		return energia
	}
	
	method estaDebil() {
		return (energia < 50)
	}
	
	method estaFeliz() {
		return ((energia >= 500) and (energia <= 1000))
	}
	
	method cuantoQuiereVolar() {
		return 1
	}

	method puedeVolar(km) {
		return energia >= self.energiaParaVolar(km)
	}
	
	method haceLoQueQuieras() {
		self.volar(self.cuantoQuiereVolar())
	}
	
}

object pipa {
	
	var calorias = 0
	var kilometros = 0
	
	
	method comer(que) {
		// 1 kcaloria = 4184 joules
		// 1000 calorias = 4184 joules
		calorias += 4184 * que.energia() / 1000
	}
	
	method volar(km) {
		kilometros += km
	}
	
	method kmsRecorridos() {
		return kilometros
	}
	
	method caloriasIngeridas() {
		return calorias
	}
	
	method puedeVolar(km) {
		return true
	}
	
	method haceLoQueQuieras() {
		// no hace nada
	}
	
}

object alpiste {
	
	var gramos = 50
	
	method peso(unosGramos) {
		gramos = unosGramos 
	}
	
	method energia() {
		return gramos * 4
	}
	
}

object nemo {
	
	var edad = 2	// en anios
	
	method edad(nuevaEdad) {
		edad = nuevaEdad	// en anios
	}
	
	method energia() {
		return 2 * edad
	}
	
}

object postreLight {
	
	method energia() {
		return 0
	}
	
}

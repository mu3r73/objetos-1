
import ej_05_quiniela_reducida.*

test "sorteo: apuestas ganadoras y perdedoras, ganadores, total premios, beneficios 1" {
	
	const apuestas = #{apuestaDePrueba1, apuestaDePrueba2, apuestaDePrueba3}
	apuestas.forEach({
		apuesta => sorteo.agregarApuesta(apuesta)
	})
	
	const ganadores = #{111, 222, 333, 444, 555}
	ganadores.forEach({
		numero => sorteo.ingresarGanador(numero)
	})
	
	assert.equals(#{apuestaDePrueba1, apuestaDePrueba2}, sorteo.apuestasGanadoras())
	assert.equals(#{apuestaDePrueba3}, sorteo.apuestasPerdedoras())
	
	assert.equals(#{"Jugador 1", "Jugador 2"}, sorteo.ganadores())
	
	assert.equals(4444 + 55555 + 666666, sorteo.totalRecaudado())
	assert.equals((4444 + 55555) * 2, sorteo.totalEnPremios())
	assert.equals(4444 + 55555 + 666666 - ((4444 + 55555) * 2), sorteo.beneficio())
	
}

test "sorteo: apuestas ganadoras y perdedoras, ganadores, total premios, beneficios 2" {
	
	const apuestas = #{apuestaDePrueba1, apuestaDePrueba2, apuestaDePrueba3}
	apuestas.forEach({
		apuesta => sorteo.agregarApuesta(apuesta)
	})
	
	const ganadores = #{666, 777, 888, 999, 000}
	ganadores.forEach({
		numero => sorteo.ingresarGanador(numero)
	})
	
	assert.equals(#{apuestaDePrueba3}, sorteo.apuestasGanadoras())
	assert.equals(#{apuestaDePrueba1, apuestaDePrueba2}, sorteo.apuestasPerdedoras())
	
	assert.equals(#{"Jugador 3"}, sorteo.ganadores())
	
	assert.equals(4444 + 55555 + 666666, sorteo.totalRecaudado())
	assert.equals(666666 * 2, sorteo.totalEnPremios())
	assert.equals(4444 + 55555 + 666666 - (666666 * 2), sorteo.beneficio())
	
}

test "sorteo: numeros ganadores" {
	
	// antes de ingresar numeros ganadores
	assert.throwsException({
		=> sorteo.resultado(1)
	})
	
	// numeros ganadores: posicion 1 = 1, posicion 2 = 2, etc.
	(1..4).forEach({
		i => sorteo.ingresarGanador(i)
	})
	
	assert.equals(1, sorteo.resultado(1))
	assert.equals(3, sorteo.resultado(3))
	
	// resultado aun no cargado
	assert.throwsException({
		=> sorteo.resultado(5)
	})
	
	sorteo.ingresarGanador(5)
	
	// no se pueden ingresar mas numeros ganadores, porque ya hay 5
	assert.throwsException({
		=> sorteo.ingresarGanador(6)
	})
	
}

test "apuesta aproximacion a la cabeza" {
	
	apuestaAproximacionALaCabeza.numeroApostado(6)
	apuestaAproximacionALaCabeza.nombreJugador("Jugador 1")
	apuestaAproximacionALaCabeza.valorApostado(100)
	
	assert.that(apuestaAproximacionALaCabeza.esGanadora([6, 7, 8, 9, 0]))
	assert.that(apuestaAproximacionALaCabeza.esGanadora([2, 12, 22, 32, 42]))
	assert.that(apuestaAproximacionALaCabeza.esGanadora([10, 20, 30, 40, 50]))
	assert.notThat(apuestaAproximacionALaCabeza.esGanadora([1, 11, 21, 31, 41]))
	assert.notThat(apuestaAproximacionALaCabeza.esGanadora([1, 6, 11, 16, 21]))
	
	assert.equals(5000, apuestaAproximacionALaCabeza.montoPremio(100000, [6, 7, 8, 9, 0]))
	assert.equals(1000, apuestaAproximacionALaCabeza.montoPremio(100000, [10, 20, 30, 40, 50]))
	assert.equals(600, apuestaAproximacionALaCabeza.montoPremio(10000, [6, 7, 8, 9, 0]))
	assert.equals(600, apuestaAproximacionALaCabeza.montoPremio(10000, [10, 20, 30, 40, 50]))
	
}

test "apuesta en orden" {
	
	apuestaEnOrden.numerosApostados([1, 3, 5, 7, 9])
	apuestaEnOrden.nombreJugador("Jugador 1")
	
	assert.that(apuestaEnOrden.esGanadora([1, 3, 5, 7, 9]))
	assert.notThat(apuestaEnOrden.esGanadora([3, 5, 7, 9, 0]))
	assert.notThat(apuestaEnOrden.esGanadora([9, 7, 5, 3, 1]))
	assert.notThat(apuestaEnOrden.esGanadora(#{1, 3, 5, 7, 9}))
	
	assert.equals(4000, apuestaEnOrden.montoPremio(10000, []))
	
}

test "apuesta un rango a la cabeza" {
	
	apuestaUnRangoALaCabeza.minRangoApostado(1)
	apuestaUnRangoALaCabeza.maxRangoApostado(5)
	apuestaUnRangoALaCabeza.nombreJugador("Jugador 1")
	apuestaUnRangoALaCabeza.valorApostado(100)
	
	assert.that(apuestaUnRangoALaCabeza.esGanadora([3, 6, 9, 12, 15]))
	assert.that(apuestaUnRangoALaCabeza.esGanadora([1, 5, 10, 15, 20]))
	assert.notThat(apuestaUnRangoALaCabeza.esGanadora([10, 20, 30, 40, 50]))
	
	assert.equals(1375, apuestaUnRangoALaCabeza.montoPremio(100000, []))
	assert.equals(1000, apuestaUnRangoALaCabeza.montoPremio(10000, []))
	
}

test "apuesta a cualquiera" {
	
	apuestaACualquiera.numeroApostado(3)
	apuestaACualquiera.nombreJugador("Jugador 1")
	apuestaACualquiera.valorApostado(100)
	
	assert.that(apuestaACualquiera.esGanadora([3, 4, 5, 1, 2]))
	assert.that(apuestaACualquiera.esGanadora([1, 2, 3, 4, 5]))
	assert.that(apuestaACualquiera.esGanadora([4, 5, 1, 2, 3]))
	assert.notThat(apuestaACualquiera.esGanadora([4, 5, 6, 7, 8]))
	
	assert.equals(200, apuestaACualquiera.montoPremio(100000, []))
	assert.equals(100, apuestaACualquiera.montoPremio(10000, []))
	
}

test "apuesta a los primeros 2" {
	
	apuestaALosPrimeros2.numeroApostado(3)
	apuestaALosPrimeros2.nombreJugador("Jugador 1")
	apuestaALosPrimeros2.valorApostado(100)
	
	assert.that(apuestaALosPrimeros2.esGanadora([3, 4, 5, 1, 2]))
	assert.that(apuestaALosPrimeros2.esGanadora([2, 3, 4, 5, 1]))
	assert.notThat(apuestaALosPrimeros2.esGanadora([1, 2, 3, 4, 5]))
	
	assert.equals(3000, apuestaALosPrimeros2.montoPremio(100000, []))
	assert.equals(500, apuestaALosPrimeros2.montoPremio(10000, []))
	
}

test "apuesta a la cabeza" {
	
	apuestaALaCabeza.numeroApostado(3)
	apuestaALaCabeza.nombreJugador("Jugador 1")
	apuestaALaCabeza.valorApostado(100)
	
	assert.that(apuestaALaCabeza.esGanadora([3, 4, 5, 1, 2]))
	assert.notThat(apuestaALaCabeza.esGanadora([1, 2, 3, 4, 5]))
	
	assert.equals(7000, apuestaALaCabeza.montoPremio(100000, []))
	assert.equals(1000, apuestaALaCabeza.montoPremio(10000, []))
	
}


object romina {
	
	var deseos = #{}
	
	
	method desear(cosa) {
		deseos.add(cosa)
	}
	
	method olvidar(cosa) {
		deseos.remove(cosa)
	}
	
	method pedir(cosa) {
		if (not casa.puedeComprar(cosa)) {
			error.throwWithMessage("casa no puede comprar " + cosa.kindName())
		}
		if (not casa.compras().contains(cosa)) {
			casa.comprar(cosa)
		}
	}
	
	method deseosNoSatisfechos() {
		return deseos.difference(self.deseosSatisfechos())
	}
	
	method deseosSatisfechos() {
		return deseos.filter({
			cosa => casa.compras().contains(cosa)
		})
	}
	
	method cosasParaDonar() {
		return casa.compras().filter({
			cosa => not deseos.contains(cosa)
		})
	}
	
}

object casa {
	
	var cuenta
	var compras = []
	
	
	method cuenta(nuevaCuenta) {
		cuenta = nuevaCuenta
	}
	
	method verificarCuenta() {
		if (cuenta == null) {
			error.throwWithMessage("la casa no tiene cuenta bancaria")
		}
	}
	
	method comprar(cosa) {
		self.verificarCuenta()
		cuenta.extraer(cosa.precio())
		compras.add(cosa)
	}
	
	method tieneComida() {
		return compras.any({
			cosa => cosa.esComida()
		})
	}
	
	method vieneDeEquiparse() {
		return compras.last().esElectrodomestico()
			or compras.last().precio() > 5000
	}
	
	method puedeComprar(cosa) {
		self.verificarCuenta()
		return cosa.precio() <= cuenta.saldo()
	}
	
	method cuentaParaGastos() {
		self.verificarCuenta()
		return cuenta
	}
	
	method esDerrochona() {
		return self.gastos() > 5000
	}
	
	method esBacan() {
		self.verificarCuenta()
		return cuenta.saldo() >= 40000
	}
	
	method gastos() {
		return compras.sum({
			cosa => cosa.precio()
		})
	}
	
	method compras() {
		return compras
	}
	
	method compraMasCara() {
		return compras.max({
			cosa => cosa.precio()
		})
	}
	
	method electrodomesticosComprados() {
		return compras.filter({
			cosa => cosa.esElectrodomestico()
		})
	}
	
	method malaEpoca() {
		// si compras == [], devuelve true
		return compras.all({
			cosa => cosa.esComida()
		})
	}
	
	method queFaltaComprar(lista) {
		return lista.filter({
			cosa => not compras.contains(cosa)
		})
	}
	
	method faltaComida() {
		return compras.count({
			cosa => cosa.esComida()
		}) <= 2
	}
	
}

object heladera {
	
	var precio = 20000
	
	
	method precio() {
		return precio
	}
	
	method esComida() {
		return false
	}
	
	method esElectrodomestico() {
		return true
	}
	
}


object cama {
	
	var precio = 8000
	
	
	method precio() {
		return precio
	}
	
	method esComida() {
		return false
	}
	
	method esElectrodomestico() {
		return false
	}
	
}


object asado {
	
	var precio = 350
	
	
	method precio() {
		return precio
	}
	
	method esComida() {
		return true
	}
	
	method esElectrodomestico() {
		return false
	}
	
}

object fideos {
	
	var precio = 50
	
	
	method precio() {
		return precio
	}
	
	method esComida() {
		return true
	}
	
	method esElectrodomestico() {
		return false
	}
	
}

object plancha {
	
	var precio = 1200
	
	
	method precio() {
		return precio
	}
	
	method esComida() {
		return false
	}
	
	method esElectrodomestico() {
		return true
	}
	
}

object cuentaCombinada {
	
	var cuentaPrimaria
	var cuentaSecundaria
	
	
	method verificarCuentas() {
		if (cuentaPrimaria == null) {
			error.throwWithMessage("cuentaCombinada no tiene cuenta primeria")
		}
		if (cuentaSecundaria == null) {
			error.throwWithMessage("cuentaCombinada no tiene cuenta secundaria")
		}
	}
	
	method saldo() {
		self.verificarCuentas()
		return cuentaPrimaria.saldo() + cuentaSecundaria.saldo()
	}
	
	method depositar(pesos) {
		self.verificarCuentas()
		if (cuentaSecundaria.saldo() < 1000) {
			cuentaSecundaria.depositar(pesos)
		} else {
			cuentaPrimaria.depositar(pesos)
		}
	}
	
	method extraer(pesos) {
		self.verificarCuentas()
		if (pesos <= cuentaPrimaria.saldo()) {
			cuentaPrimaria.extraer(pesos)
		} else {
			cuentaSecundaria.extraer(pesos - cuentaPrimaria.saldo())
			cuentaPrimaria.extraer(cuentaPrimaria.saldo())
		}
	}
	
	method cuentaPrimaria(cuenta) {
		cuentaPrimaria = cuenta
	}
	
	method cuentaSecundaria(cuenta) {
		cuentaSecundaria = cuenta
	}
	
}

object cuentaPepe {
	// cuenta normal
	
	var saldo = 0
	
	
	method saldo() {
		return saldo
	}
	
	method depositar(pesos) {
		saldo += pesos
	}
	
	method extraer(pesos) {
		if (pesos > saldo) {
			error.throwWithMessage("cuentaPepe no tiene saldo suficiente para extraer " + pesos)
		}
		saldo -= pesos
	}
	
}

object cuentaJulian {
	// cuenta embargada
	
	var saldo = 0
	
	
	method saldo() {
		return saldo
	}
	
	method depositar(pesos) {
		saldo += pesos * 0.80
	}
	
	method extraer(pesos) {
		if (pesos > saldo) {
			error.throwWithMessage("cuentaJulian no tiene saldo suficiente para extraer " + pesos)
		}
		saldo -= pesos
		// gastos administrativos
		if (saldo > 5) {
			saldo -= 5
		}
	}
	
}

object cuentaPapa {
	// cuenta dolarizada
	
	var saldoEnDolares = 0
	var precioDeCompra = 14.70
	var precioDeVenta = 15.10
	
	
	method saldo() {
		return saldoEnDolares * precioDeCompra
	}
	
	method depositar(pesos) {
		saldoEnDolares += pesos / precioDeVenta
	}
	
	method extraer(pesos) {
		if (self.dolaresParaComprar(pesos) > saldoEnDolares) {
			error.throwWithMessage("cuentaPapa no tiene saldo suficiente para extraer " + pesos)
		}
		saldoEnDolares -= self.dolaresParaComprar(pesos)
	}
	
	method dolaresParaComprar(pesos) {
		return pesos / precioDeCompra
	}
	
	method precioDeCompra(pesos) {
		precioDeCompra = pesos
	}
	
	method precioDeVenta(pesos) {
		precioDeVenta = pesos
	}
	
}

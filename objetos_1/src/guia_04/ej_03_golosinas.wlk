
object mariano {
	
	var golosinas = #{}
	
	
	method comprar(golosina) {
		golosinas.add(golosina)
	}
	
	method desechar(golosina) {
		golosinas.remove(golosina)
	}
	
	method hayGolosinasSinTACC() {
		return golosinas.any({
			golosina => golosina.esLibreDeGluten()
		})
	}
	
	method golosinaMasCara() {
		return golosinas.max({
			golosina => golosina.precio()
		})
	}
	
	method golosinaCualquiera() {
		if (golosinas.isEmpty()) {
			error.throwWithMessage(self.kindName() + ": no hay golosinas")
		}
		return golosinas.anyOne()
	}
	
	method golosinaDeSabor(sabor) {
		return golosinas.find({
			golosina => golosina.sabor() == sabor
		})
	}
	
	method golosinasDeSabor(sabor) {
		return golosinas.filter({
			golosina => (golosina.sabor() == sabor)
		})
	}
	
	method preciosCuidados() {
		return golosinas.all({
			golosina => golosina.precio() <= 10
		})
	}
	
	method sabores() {
		return golosinas.map({
			golosina => golosina.sabor()
		}).asSet()
	}
	
	method probarGolosinas() {
		golosinas.forEach({
			golosina => golosina.recibirMordisco()
		})
	}
	
	method golosinasFaltantes(deseadas) {
		return deseadas.difference(golosinas)
	}
	
}

object pastillaTuttiFrutti {
	
	var peso = 5
	var libreDeGluten
	var sabor = "frutilla"
	
	
	method verificarGluten() {
		if (libreDeGluten == null) {
			error.throwWithMessage(self.kindName() + ": no sabe si es libre de gluten")
		}
	}
	
	method precio() {
		self.verificarGluten()
		if (libreDeGluten) {
			return 7
		} else {
			return 10
		}
	}
	
	method sabor() {
		return sabor
	}
	
	method peso() {
		return peso
	}
	
	method esLibreDeGluten(tof) {
		libreDeGluten = tof
	}
	
	method esLibreDeGluten() {
		self.verificarGluten()
		return libreDeGluten
	}
	
	method recibirMordisco() {
		if (peso == 0) {
			error.throwWithMessage(self.kindName() + ": se termino")
		}
		peso -= 1
		if (sabor == "frutilla") {
			sabor = "chocolate"
		} else if (sabor == "chocolate") {
			sabor = "naranja"
		} else {
			sabor = "frutilla"
		}
	}
	
}

object golosinaBaniada {
	
	var golosinaBase
	var pesoBaniado = 4
	
	
	method golosinaBase(golosina) {
		golosinaBase = golosina
	}
	
	method precio() {
		return 2 + golosinaBase.precio()
	}
	
	method sabor() {
		return golosinaBase.sabor()
	}
	
	method peso() {
		return pesoBaniado + golosinaBase.peso()
	}
	
	method esLibreDeGluten() {
		return golosinaBase.esLibreDeGluten()
	}
	
	method recibirMordisco() {
		pesoBaniado = (pesoBaniado - 2).max(0)
		golosinaBase.recibirMordisco()
	}
	
}

object chocolatin {
	
	var pesoInicial
	var mordiscos
	
	
	method peso(gramos) {
		pesoInicial = gramos
		mordiscos = 0
	}
	
	method precio() {
		return pesoInicial * 0.5
	}
	
	method sabor() {
		return "chocolate"
	}
	
	method peso() {
		return (pesoInicial - 2 * mordiscos).max(0)
	}
	
	method esLibreDeGluten() {
		return false
	}
	
	method recibirMordisco() {
		if (self.peso() == 0) {
			error.throwWithMessage(self.kindName() + ": se termino")
		}
		mordiscos += 1
	}
	
}

object oblea {
	
	var peso = 250
	
	
	method precio() {
		return 5
	}
	
	method sabor() {
		return "vainilla"
	}
	
	method peso() {
		return peso
	}
	
	method esLibreDeGluten() {
		return false
	}
	
	method recibirMordisco() {
		if (peso > 70) {
			peso -= peso * 0.5
		} else {
			peso -= peso * 0.25
		}
	}
	
}

object chupetin {
	
	var peso = 7
	
	
	method precio() {
		return 2
	}
	
	method sabor() {
		return "naranja"
	}
	
	method peso() {
		return peso
	}
	
	method esLibreDeGluten() {
		return true
	}
	
	method recibirMordisco() {
		if (peso >= 2) {
			peso -= peso * 0.1
		}
	}
	
}

object caramelo {
	
	var peso = 5
	
	
	method precio() {
		return 1
	}
	
	method sabor() {
		return "frutilla"
	}
	
	method peso() {
		return peso
	}
	
	method esLibreDeGluten() {
		return true
	}
	
	method recibirMordisco() {
		if (peso == 0) {
			error.throwWithMessage(self.kindName() + ": se termino")
		}
		peso -= 1
	}
	
}

object alfajor {
	
	var peso = 300
	
	
	method precio() {
		return 12
	}
	
	method sabor() {
		return "chocolate"
	}
	
	method peso() {
		return peso
	}
	
	method esLibreDeGluten() {
		return false
	}
	
	method recibirMordisco() {
		peso -= peso * 0.2
	}
	
}

object bombon {
	
	var peso = 15
	
	
	method precio() {
		return 5
	}
	
	method sabor() {
		return "frutilla"
	}
	
	method peso() {
		return peso
	}
	
	method esLibreDeGluten() {
		return true
	}
	
	method recibirMordisco() {
		if (peso == 0) {
			error.throwWithMessage(self.kindName() + ": se termino")
		}
		peso = ((peso * 0.8) - 1).max(0)
	}
	
}

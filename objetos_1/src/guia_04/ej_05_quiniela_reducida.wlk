
object sorteo {
	
	var numerosSorteados = []
	var apuestas = #{}
	
	
	method ingresarGanador(numero) {
		if (numerosSorteados.size() == 5) {
			error.throwWithMessage("sorteo: ya se han ingresado los 5 numeros ganadores")
		}
		numerosSorteados.add(numero)
	}
	
	method resultado(posicion) {
		if (posicion > numerosSorteados.size()) {
			error.throwWithMessage("sorteo: resultado #" + posicion + " aun no sorteado")
		}
		return numerosSorteados.get(posicion - 1)
	}
	
	method agregarApuesta(apuesta) {
		apuestas.add(apuesta)
	}
	
	method apuestasGanadoras() {
		return apuestas.filter({
			apuesta => apuesta.esGanadora(numerosSorteados)
		})
	}
	
	method apuestasPerdedoras() {
		return apuestas.difference(self.apuestasGanadoras())
	}
	
	method totalRecaudado() {
		return apuestas.sum({
			apuesta => apuesta.valorApostado()
		})
	}
	
	method totalEnPremios() {
		return self.apuestasGanadoras().sum({
			apuesta => apuesta.montoPremio(self.totalRecaudado(), numerosSorteados)
		})
	}
	
	method ganadores() {
		return self.apuestasGanadoras().map({
			apuesta => apuesta.nombreJugador()
		}).asSet()
	}
	
	method beneficio() {
		return self.totalRecaudado() - self.totalEnPremios()
	}
	
}

object apuestaAproximacionALaCabeza {
	
	var numeroApostado
	var nombreJugador
	var valorApostado
	
	
	method numeroApostado(numero) {
		numeroApostado = numero
	}
	
	method numeroApostado() {
		if (numeroApostado == null) {
			error.throwWithMessage(self.kindName() + ": desconozco numero apostado")
		}
		return numeroApostado
	}
	
	method nombreJugador(nombre) {
		nombreJugador = nombre
	}
	
	method nombreJugador() {
		if (nombreJugador == null) {
			error.throwWithMessage(self.kindName() + ": desconozco nombre del jugador")
		}
		return nombreJugador
	}
	
	method valorApostado(pesos) {
		valorApostado = pesos
	}
	
	method valorApostado() {
		if (valorApostado == null) {
			error.throwWithMessage(self.kindName() + ": desconozco valor apostado")
		}
		return valorApostado
	}
	
	method esGanadora(numerosSorteados) {
		return numerosSorteados.first().between(numeroApostado - 4, numeroApostado + 4)
	}
	
	method montoPremio(totalRecaudado, numerosSorteados) {
		return ((50 - (numerosSorteados.first() - numeroApostado).abs() * 10) * valorApostado).min(0.06 * totalRecaudado).max(0)
	}
	
}

object apuestaEnOrden {
	
	var numerosApostados
	var nombreJugador
	const valorApostado = 500
	
	
	method numerosApostados(lista) {
		if (lista.size() < 5) {
			error.throwWithMessage(self.kindName() + ": tenes que elegir 5 numeros")
		}
		numerosApostados = lista
	}
	
	method numerosApostados() {
		if (numerosApostados == null) {
			error.throwWithMessage(self.kindName() + ": desconozco numeros apostados")
		}
		return numerosApostados
	}
	
	method nombreJugador(nombre) {
		nombreJugador = nombre
	}
	
	method nombreJugador() {
		if (nombreJugador == null) {
			error.throwWithMessage(self.kindName() + ": desconozco nombre del jugador")
		}
		return nombreJugador
	}
	
	method valorApostado() {
		return valorApostado
	}
	
	method esGanadora(numerosSorteados) {
		return numerosApostados == numerosSorteados
	}
	
	method montoPremio(totalRecaudado, numerosSorteados) {
		return totalRecaudado * 0.4
	}
	
}

object apuestaUnRangoALaCabeza {
	
	var minRangoApostado
	var maxRangoApostado
	var nombreJugador
	var valorApostado
	
	
	method minRangoApostado(numero) {
		minRangoApostado = numero
	}
	
	method minRangoApostado() {
		if (minRangoApostado == null) {
			error.throwWithMessage(self.kindName() + ": desconozco minimo del rango apostado")
		}
		return minRangoApostado
	}
	
	method maxRangoApostado(numero) {
		maxRangoApostado = numero
	}
	
	method maxRangoApostado() {
		if (maxRangoApostado == null) {
			error.throwWithMessage(self.kindName() + ": desconozco maximo del rango apostado")
		}
		return maxRangoApostado
	}
	
	method nombreJugador(nombre) {
		nombreJugador = nombre
	}
	
	method nombreJugador() {
		if (nombreJugador == null) {
			error.throwWithMessage(self.kindName() + ": desconozco nombre del jugador")
		}
		return nombreJugador
	}
	
	method valorApostado(pesos) {
		valorApostado = pesos
	}
	
	method valorApostado() {
		if (valorApostado == null) {
			error.throwWithMessage(self.kindName() + ": desconozco valor apostado")
		}
		return valorApostado
	}
	
	method esGanadora(numerosSorteados) {
		return numerosSorteados.first().between(minRangoApostado, maxRangoApostado)
	}
	
	method montoPremio(totalRecaudado, numerosSorteados) {
		return (valorApostado * 55 / (maxRangoApostado - minRangoApostado)).min(totalRecaudado * 0.1)
	}
	
}

object apuestaACualquiera {
	
	var numeroApostado
	var nombreJugador
	var valorApostado
	
	
	method numeroApostado(numero) {
		numeroApostado = numero
	}
	
	method numeroApostado() {
		if (numeroApostado == null) {
			error.throwWithMessage(self.kindName() + ": desconozco numero apostado")
		}
		return numeroApostado
	}
	
	method nombreJugador(nombre) {
		nombreJugador = nombre
	}
	
	method nombreJugador() {
		if (nombreJugador == null) {
			error.throwWithMessage(self.kindName() + ": desconozco nombre del jugador")
		}
		return nombreJugador
	}
	
	method valorApostado(pesos) {
		valorApostado = pesos
	}
	
	method valorApostado() {
		if (valorApostado == null) {
			error.throwWithMessage(self.kindName() + ": desconozco valor apostado")
		}
		return valorApostado
	}
	
	method esGanadora(numerosSorteados) {
		return numerosSorteados.contains(self.numeroApostado())
	}
	
	method montoPremio(totalRecaudado, numerosSorteados) {
		return (valorApostado * 2).min(totalRecaudado * 0.01)
	}
	
}

object apuestaALosPrimeros2 {
	
	var numeroApostado
	var nombreJugador
	var valorApostado
	
	
	method numeroApostado(numero) {
		numeroApostado = numero
	}
	
	method numeroApostado() {
		if (numeroApostado == null) {
			error.throwWithMessage(self.kindName() + ": desconozco numero apostado")
		}
		return numeroApostado
	}
	
	method nombreJugador(nombre) {
		nombreJugador = nombre
	}
	
	method nombreJugador() {
		if (nombreJugador == null) {
			error.throwWithMessage(self.kindName() + ": desconozco nombre del jugador")
		}
		return nombreJugador
	}
	
	method valorApostado(pesos) {
		valorApostado = pesos
	}
	
	method valorApostado() {
		if (valorApostado == null) {
			error.throwWithMessage(self.kindName() + ": desconozco valor apostado")
		}
		return valorApostado
	}
	
	method esGanadora(numerosSorteados) {
		return numeroApostado == numerosSorteados.first()
			or numeroApostado == numerosSorteados.get(1)
	}
	
	method montoPremio(totalRecaudado, numerosSorteados) {
		return (valorApostado * 30).min(totalRecaudado * 0.05)
	}
	
}

object apuestaALaCabeza {
	
	var numeroApostado
	var nombreJugador
	var valorApostado
	
	
	method numeroApostado(numero) {
		numeroApostado = numero
	}
	
	method numeroApostado() {
		if (numeroApostado == null) {
			error.throwWithMessage(self.kindName() + ": desconozco numero apostado")
		}
		return numeroApostado
	}
	
	method nombreJugador(nombre) {
		nombreJugador = nombre
	}
	
	method nombreJugador() {
		if (nombreJugador == null) {
			error.throwWithMessage(self.kindName() + ": desconozco nombre del jugador")
		}
		return nombreJugador
	}
	
	method valorApostado(pesos) {
		valorApostado = pesos
	}
	
	method valorApostado() {
		if (valorApostado == null) {
			error.throwWithMessage(self.kindName() + ": desconozco valor apostado")
		}
		return valorApostado
	}
	
	method esGanadora(numerosSorteados) {
		return numeroApostado == numerosSorteados.first()
	}
	
	method montoPremio(totalRecaudado, numerosSorteados) {
		return (valorApostado * 70).min(totalRecaudado * 0.1)
	}
	
}

object apuestaDePrueba1 {
	
	const numeroApostado = 444
	
	
	method valorApostado() {
		return 4444
	}
	
	method nombreJugador() {
		return "Jugador 1"
	}
	
	method esGanadora(numerosSorteados) {
		return numerosSorteados.contains(numeroApostado)
	}
	
	method montoPremio(totalRecaudado, numerosSorteados) {
		return self.valorApostado() * 2
	}
	
}

object apuestaDePrueba2 {
	
	const numeroApostado = 555
	
	
	method valorApostado() {
		return 55555
	}
	
	method nombreJugador() {
		return "Jugador 2"
	}
	
	method esGanadora(numerosSorteados) {
		return numerosSorteados.contains(numeroApostado)
	}
	
	method montoPremio(totalRecaudado, numerosSorteados) {
		return self.valorApostado() * 2
	}
	
}

object apuestaDePrueba3 {
	
	const numeroApostado = 666
	
	
	method valorApostado() {
		return 666666
	}
	
	method nombreJugador() {
		return "Jugador 3"
	}
	
	method esGanadora(numerosSorteados) {
		return numerosSorteados.contains(numeroApostado)
	}
	
	method montoPremio(totalRecaudado, numerosSorteados) {
		return self.valorApostado() * 2
	}
	
}

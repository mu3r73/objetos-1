
object tramoRectoLargo {
	
	var longitud = 200
	var siguiente
	
	
	method siguiente() {
		if (siguiente == null) {
			error.throwWithMessage("tramoRectoLargo no tiene siguiente")
		}
		return siguiente
	}
	
	method siguiente(nuevoSiguiente) {
		siguiente = nuevoSiguiente
	}

	method longitudMaximaHastaElFinal() {
		return longitud + siguiente.longitudMaximaHastaElFinal()
	}
	
	method velocidadMaximaDeSalida(velocidadEntrada) {
		return siguiente.velocidadMaximaDeSalida(velocidadEntrada)
	}
		
}

object tramoRectoCorto {
	
	var longitud = 40
	var siguiente
	
	
	method siguiente() {
		if (siguiente == null) {
			error.throwWithMessage("tramoRectoCorto no tiene siguiente")
		}
		return siguiente
	}	
	
	method siguiente(nuevoSiguiente) {
		siguiente = nuevoSiguiente
	}
	
	method longitudMaximaHastaElFinal() {
		return longitud + siguiente.longitudMaximaHastaElFinal()
	}
	
	method velocidadMaximaDeSalida(velocidadEntrada) {
		return siguiente.velocidadMaximaDeSalida(velocidadEntrada)
	}
	
}

object codo {
	
	var longitud = 70
	var siguiente
	
	
	method siguiente() {
		if (siguiente == null) {
			error.throwWithMessage("codo no tiene siguiente")
		}
		return siguiente
	}
	
	method siguiente(nuevoSiguiente) {
		siguiente = nuevoSiguiente
	}
	
	method longitudMaximaHastaElFinal() {
		return longitud + siguiente.longitudMaximaHastaElFinal()
	}
	
	method velocidadMaximaDeSalida(velocidadEntrada) {
		return siguiente.velocidadMaximaDeSalida(velocidadEntrada - 20)
	}
	
}

object tramoEnSubida {
	
	var longitud = 100
	var siguiente
	
	
	method siguiente() {
		if (siguiente == null) {
			error.throwWithMessage("tramoEnSubida no tiene siguiente")
		}
		return siguiente
	}
	
	method siguiente(nuevoSiguiente) {
		siguiente = nuevoSiguiente
	}
	
	method longitudMaximaHastaElFinal() {
		return longitud + siguiente.longitudMaximaHastaElFinal()
	}
	
	method velocidadMaximaDeSalida(velocidadEntrada) {
		return siguiente.velocidadMaximaDeSalida(velocidadEntrada / 2.0)
	}
	
}

object tramoEnBajada {
	
	var longitud = 200
	var siguiente
	
	
	method siguiente() {
		if (siguiente == null) {
			error.throwWithMessage("tramoEnBajada no tiene siguiente")
		}
		return siguiente
	}
	
	method siguiente(nuevoSiguiente) {
		siguiente = nuevoSiguiente
	}
	
	method longitudMaximaHastaElFinal() {
		return longitud + siguiente.longitudMaximaHastaElFinal()
	}
	
	method velocidadMaximaDeSalida(velocidadEntrada) {
		return siguiente.velocidadMaximaDeSalida(velocidadEntrada * 2)
	}
	
}

object bifurcacionY {
	
	var longitud = 50
	var siguienteIzq
	var siguienteDer
	
	
	method siguienteIzquierda() {
		if (siguienteIzq == null) {
			error.throwWithMessage("bifurcacionY no tiene siguienteIzq")
		}
		return siguienteIzq
	}

	method siguienteDerecha() {
		if (siguienteDer == null) {
			error.throwWithMessage("bifurcacionY no tiene siguienteDer")
		}
		return siguienteDer
	}
	
	method siguienteIzquierda(nuevoSigIzq) {
		siguienteIzq = nuevoSigIzq
	}
	
	method siguienteDerecha(nuevoSigDer) {
		siguienteDer = nuevoSigDer
	}
	
	method longitudMaximaHastaElFinal() {
		return longitud + siguienteIzq.longitudMaximaHastaElFinal().max(siguienteDer.longitudMaximaHastaElFinal())
	}
	
	method velocidadMaximaDeSalida(velocidadEntrada) {
		return siguienteIzq.velocidadMaximaDeSalida(velocidadEntrada).max(siguienteDer.velocidadMaximaDeSalida(velocidadEntrada))
	}
	
}

object tope {
	
	var longitud = 0
	
	method longitudMaximaHastaElFinal() {
		return longitud
	}
	
	method velocidadMaximaDeSalida(velocidadEntrada) {
		return 0
	}
	
}

object canilla {
	
	method longitudMaximaHastaElFinal() {
		return 0
	}
	
	method velocidadMaximaDeSalida(velocidadEntrada) {
		return velocidadEntrada
	}
	
}


object casa {
	
	var cuenta = cuentaPepe
	var gastos = 0
	var tieneComida
	var vieneDeEquiparse
	
	
	method cuenta(nuevaCuenta) {
		cuenta = nuevaCuenta
	}
	
	method comprar(cosa) {
		self.gastar(cosa.precio())
		if (cosa.esComida()) {
			tieneComida = true
		}
		if (cosa.esElectrodomestico() or cosa.precio() > 5000) {
			vieneDeEquiparse = true
		} else {
			vieneDeEquiparse = false
		}
	}
	
	method gastar(pesos) {
		gastos += pesos
		cuenta.extraer(pesos)
	}
	
	method tieneComida() {
		return tieneComida
	}
	
	method vieneDeEquiparse() {
		return vieneDeEquiparse
	}
	
	method puedeComprar(cosa) {
		return cosa.precio() <= cuenta.saldo()
	}
	
	method cuentaParaGastos() {
		return cuenta
	}
	
	method esDerrochona() {
		return gastos > 5000
	}
	
	method esBacan() {
		return cuenta.saldo() >= 40000
	}
	
}

object heladera {
	
	var precio = 20000
	
	
	method precio() {
		return precio
	}
	
	method esComida() {
		return false
	}
	
	method esElectrodomestico() {
		return true
	}
	
}


object cama {
	
	var precio = 8000
	
	
	method precio() {
		return precio
	}
	
	method esComida() {
		return false
	}
	
	method esElectrodomestico() {
		return false
	}
	
}


object asado {
	
	var precio = 350
	
	
	method precio() {
		return precio
	}
	
	method esComida() {
		return true
	}
	
	method esElectrodomestico() {
		return false
	}
	
}

object fideos {
	
	var precio = 50
	
	
	method precio() {
		return precio
	}
	
	method esComida() {
		return true
	}
	
	method esElectrodomestico() {
		return false
	}
	
}

object plancha {
	
	var precio = 1200
	
	
	method precio() {
		return precio
	}
	
	method esComida() {
		return false
	}
	
	method esElectrodomestico() {
		return true
	}
	
}

object cuentaCombinada {
	
	var cuentaPrimaria = cuentaPepe
	var cuentaSecundaria = cuentaJulian
	
	
	method saldo() {
		return cuentaPrimaria.saldo() + cuentaSecundaria.saldo()
	}
	
	method depositar(pesos) {
		if (cuentaSecundaria.saldo() < 1000) {
			cuentaSecundaria.depositar(pesos)
		} else {
			cuentaPrimaria.depositar(pesos)
		}
	}
	
	method extraer(pesos) {
		if (pesos <= cuentaPrimaria.saldo()) {
			cuentaPrimaria.extraer(pesos)
		} else {
			cuentaSecundaria.extraer(pesos - cuentaPrimaria.saldo())
			cuentaPrimaria.extraer(cuentaPrimaria.saldo())
		}
	}
	
	method cuentaPrimaria(cuenta) {
		cuentaPrimaria = cuenta
	}
	
	method cuentaSecundaria(cuenta) {
		cuentaSecundaria = cuenta
	}
	
}

object cuentaPepe {
	// cuenta normal
	
	var saldo = 0
	
	
	method saldo() {
		return saldo
	}
	
	method depositar(pesos) {
		saldo += pesos
	}
	
	method extraer(pesos) {
		saldo -= pesos
	}
	
}

object cuentaJulian {
	// cuenta embargada
	
	var saldo = 0
	
	
	method saldo() {
		return saldo
	}
	
	method depositar(pesos) {
		saldo += pesos * 0.80
	}
	
	method extraer(pesos) {
		saldo -= pesos
		// gastos administrativos
		if (saldo > 5) {
			saldo -= 5
		}
	}
	
}

object cuentaPapa {
	// cuenta dolarizada
	
	var saldoEnDolares = 0
	var precioDeCompra = 14.70
	var precioDeVenta = 15.10
	
	
	method saldo() {
		return saldoEnDolares * precioDeCompra
	}
	
	method depositar(pesos) {
		saldoEnDolares += pesos / precioDeVenta
	}
	
	method extraer(pesos) {
		saldoEnDolares -= pesos / precioDeCompra
	}
	
	method precioDeCompra(pesos) {
		precioDeCompra = pesos
	}
	
	method precioDeVenta(pesos) {
		precioDeVenta = pesos
	}
	
}

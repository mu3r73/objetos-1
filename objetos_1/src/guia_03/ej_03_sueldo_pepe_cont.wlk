
object ernesto {
	
	var companiero
	var bonoPresentismo
	
	
	method neto() {
		return companiero.neto()
	}
	
	method presentismo() {
		return bonoPresentismo.bono(0)
	}
	
	method sueldo() {
		return self.neto() + self.presentismo()
	}
	
	method companiero(nuevoCompaniero) {
		companiero = nuevoCompaniero
	}
	
	method bonoPresentismo(nuevoBonoPresentismo) {
		bonoPresentismo = nuevoBonoPresentismo
	}
	
}

object roque {
	
	var neto = 28000
	var descSindical
	
	
	method descSindical(nuevoDescuento) {
		descSindical = nuevoDescuento
	}
	
	method neto() {
		return neto
	}
	
	method descSindical() {
		return descSindical.desc(neto)
	}
	
	method sueldo() {
		return neto - self.descSindical() + 9500
	}
	
}

object pepe {
	
	var faltas = 0
	var categoria
	var descSindical
	var bonoPresentismo
	
	
	method faltas(dias) {
		faltas = dias
	}
	
	method categoria(nuevaCategoria) {
		categoria = nuevaCategoria
	}
	
	method descSindical(nuevoDescuento) {
		descSindical = nuevoDescuento
	}
	
	method bonoPresentismo(nuevoBonoPresentismo) {
		bonoPresentismo = nuevoBonoPresentismo
	}
	
	method neto() {
		return categoria.neto()
	}
	
	method descSindical() {
		return descSindical.desc(self.neto())
	}
	
	method presentismo() {
		return bonoPresentismo.bono(faltas)
	}
	
	method sueldo() {
		return self.neto() - self.descSindical() + self.presentismo() 
	}
	
}

object catGerente {
	
	method neto() {
		return 15000
	}
	
}

object catCadete {
	
	method neto() {
		return 20000
	}
	
}

object descPorcentual {
	
	method desc(neto) {
		return neto * 3 / 100
	}
	
} 

object descComprometido {
	
	method desc(neto) {
		return 1500 + neto / 100
	}
	
}

object descSinSindicato {
	
	method desc(neto) {
		return 0
	}
	
}

object bonoPresNormal {
	
	method bono(faltas) {
		if (faltas == 0) {
			return 2000
		} else if (faltas == 1) {
			return 1000
		} else {
			return 0
		}
	}
	
}

object bonoPresAjuste {
	
	method bono(faltas) {
		if (faltas == 0) {
			return 10
		} else {
			return 0
		}
	}
	
}

object bonoPresDemagogico {
	
	method bono(faltas) {
		return 500
	}
	
}
